package com.fed.fedsense.Fragments

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.fed.fedsense.util.Utilities
import com.fed.fedsense.Activities.*
import com.fed.fedsense.Models.BaseResponse
import com.fed.fedsense.Models.Home.HomeData
import com.fed.fedsense.Models.Home.HomeResponse
import com.fed.fedsense.Models.SignUp.SignupReponse
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.R
import com.fed.fedsense.databinding.FragmentHomeBinding
import com.google.gson.Gson
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import kotlinx.android.synthetic.main.fragment_home.*
import org.aviran.cookiebar2.CookieBar
import org.aviran.cookiebar2.CookieBarDismissListener
import org.aviran.cookiebar2.CookieBarDismissListener.DismissType
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private var sendScreen: String = ""
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    var homedata: ArrayList<HomeData> = ArrayList<HomeData>()
    private var video_Url: String = ""
    private var description: String = ""
    private var title: String = ""
    private var isSubscribed = false
    private var notification = ""
    var first : String = ""
    private lateinit var user: User
    var yesicamefirstime = ""

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)

        val sharedPref = context?.getSharedPreferences("notification", Context.MODE_PRIVATE)
        notification = sharedPref?.getString("notification", "").toString()

        initt()
        if (notification.equals("yes")) {
            binding.icdoticon.visibility = View.VISIBLE
        } else {
            binding.icdoticon.visibility = View.GONE
        }
        getHomeData()
        utilities.saveStrings(requireContext(),"login","")







        binding.box11.setOnClickListener {
            if (utilities.isConnectingToInternet(requireContext())) {

                binding.box1.setBackgroundResource(R.drawable.bg_onclick_box)
                binding.box2.setBackgroundResource(R.drawable.bg_card_border)
                binding.box3.setBackgroundResource(R.drawable.bg_card_border)
                binding.box4.setBackgroundResource(R.drawable.bg_card_border)

                binding.cvBox1.outlineSpotShadowColor =
                    ContextCompat.getColor(requireContext(), R.color.border)
                binding.cvBox2.outlineSpotShadowColor =
                    ContextCompat.getColor(requireContext(), R.color.white)
                binding.cvBox3.outlineSpotShadowColor =
                    ContextCompat.getColor(requireContext(), R.color.white)
                binding.cvBox4.outlineSpotShadowColor =
                    ContextCompat.getColor(requireContext(), R.color.white)

                sendScreen = "fedral"

                video_Url = homedata[0].service_video!!
                description = homedata[0].service_description!!
                title = homedata[0].service_title!!
                isSubscribed = homedata[0].is_subscribed!!
            } else {
                Toast.makeText(
                    requireContext(),
                    "You are not connected to Internet",
                    Toast.LENGTH_SHORT
                ).show()
            }


        }

        binding.box21.setOnClickListener {
            if (utilities.isConnectingToInternet(requireContext())) {

                binding.box2.setBackgroundResource(R.drawable.bg_onclick_box)
                binding.box1.setBackgroundResource(R.drawable.bg_card_border)
                binding.box3.setBackgroundResource(R.drawable.bg_card_border)
                binding.box4.setBackgroundResource(R.drawable.bg_card_border)

                binding.cvBox2.outlineSpotShadowColor = resources.getColor(R.color.border)
                binding.cvBox1.outlineSpotShadowColor = resources.getColor(R.color.white)
                binding.cvBox3.outlineSpotShadowColor = resources.getColor(R.color.white)
                binding.cvBox4.outlineSpotShadowColor = resources.getColor(R.color.white)

                sendScreen = "quick"

                video_Url = homedata[1].service_video!!
                description = homedata[1].service_description!!
                isSubscribed = homedata[1].is_subscribed!!
                title = homedata[1].service_title!!
            } else {
                Toast.makeText(
                    requireContext(),
                    "You are not connected to Internet",
                    Toast.LENGTH_SHORT
                ).show()


            }
        }

        binding.box31.setOnClickListener {
            if (utilities.isConnectingToInternet(requireContext())) {

                binding.box3.setBackgroundResource(R.drawable.bg_onclick_box)
                binding.box1.setBackgroundResource(R.drawable.bg_card_border)
                binding.box2.setBackgroundResource(R.drawable.bg_card_border)
                binding.box4.setBackgroundResource(R.drawable.bg_card_border)


                binding.cvBox3.outlineSpotShadowColor = resources.getColor(R.color.border)
                binding.cvBox1.outlineSpotShadowColor = resources.getColor(R.color.white)
                binding.cvBox2.outlineSpotShadowColor = resources.getColor(R.color.white)
                binding.cvBox4.outlineSpotShadowColor = resources.getColor(R.color.white)

                sendScreen = "enroll"

                video_Url = homedata[2].service_video!!
                description = homedata[2].service_description!!
                title = homedata[2].service_title!!
                isSubscribed = homedata[2].is_subscribed!!
            } else {
                Toast.makeText(
                    requireContext(),
                    "You are not connected to Internet",
                    Toast.LENGTH_SHORT
                ).show()


            }


        }


        binding.box41.setOnClickListener {
            if (utilities.isConnectingToInternet(requireContext())) {

                binding.box4.setBackgroundResource(R.drawable.bg_onclick_box)
                binding.box3.setBackgroundResource(R.drawable.bg_card_border)
                binding.box2.setBackgroundResource(R.drawable.bg_card_border)
                binding.box1.setBackgroundResource(R.drawable.bg_card_border)

                binding.cvBox4.outlineSpotShadowColor = resources.getColor(R.color.border)
                binding.cvBox1.outlineSpotShadowColor = resources.getColor(R.color.white)
                binding.cvBox2.outlineSpotShadowColor = resources.getColor(R.color.white)
                binding.cvBox3.outlineSpotShadowColor = resources.getColor(R.color.white)

                sendScreen = "resume"

                video_Url = homedata[3].service_video!!
                description = homedata[3].service_description!!
                title = homedata[3].service_title!!
                isSubscribed = homedata[3].is_subscribed!!
            } else {
                Toast.makeText(
                    requireContext(),
                    "You are not connected to Internet",
                    Toast.LENGTH_SHORT
                ).show()


            }

        }


        binding.icNotification.setOnClickListener {
            val sharedPref: SharedPreferences =
                context?.getSharedPreferences("notification", Context.MODE_PRIVATE)!!
            val editor: SharedPreferences.Editor = sharedPref.edit()
            editor.putString("notification", "")
            editor.apply()
            binding.icdoticon.visibility =View.GONE
            val intent = Intent(context, NotificationActivity::class.java)
            startActivity(intent)


        }

        binding.ivNext.setOnClickListener {
            if (sendScreen.equals("fedral")) {
                val intent = Intent(context, FedralActivity::class.java)
                intent.putExtra("url", video_Url)
                intent.putExtra("description", description)
                intent.putExtra("title", title)
                intent.putExtra("isSubsribed", isSubscribed)
                Log.i("detail", video_Url + description + title + isSubscribed)
                startActivity(intent)

            } else if (sendScreen.equals("quick")) {
                val intent = Intent(context, QuickQualififcationActivity::class.java)
                intent.putExtra("quickurl", video_Url)
                intent.putExtra("quickdescription", description)
                intent.putExtra("quickisSubsribed", isSubscribed)
                intent.putExtra("quicktitle", title)
                Log.i("info", isSubscribed.toString())
                startActivity(intent)
            } else if (sendScreen.equals("enroll")) {

                val intent = Intent(context, EnrollActivity::class.java)
                intent.putExtra("url", video_Url)
                intent.putExtra("description", description)
                intent.putExtra("title", title)
                intent.putExtra("isSubsribed", isSubscribed)
                startActivity(intent)

            } else if (sendScreen.equals("resume")) {
                val intent = Intent(context, ResumeActivity::class.java)
                intent.putExtra("url", video_Url)
                intent.putExtra("description", description)
                intent.putExtra("title", title)
                intent.putExtra("isSubsribed", isSubscribed)
                startActivity(intent)
            }

        }


        binding.imageView5!!.setOnClickListener {

            video_Url = homedata[0].service_video!!
            description = homedata[0].service_description!!
            title = homedata[0].service_title!!
            isSubscribed = homedata[0].is_subscribed!!

            val intent = Intent(context, FedralActivity::class.java)
            intent.putExtra("url", video_Url)
            intent.putExtra("description", description)
            intent.putExtra("title", title)
            intent.putExtra("isSubsribed", isSubscribed)
            Log.i("detail", video_Url + description + title + isSubscribed)
            startActivity(intent)
        }


        binding.imageView6!!.setOnClickListener {

            video_Url = homedata[1].service_video!!
            description = homedata[1].service_description!!
            isSubscribed = homedata[1].is_subscribed!!
            title = homedata[1].service_title!!

            val intent = Intent(context, QuickQualififcationActivity::class.java)
            intent.putExtra("quickurl", video_Url)
            intent.putExtra("quickdescription", description)
            intent.putExtra("quickisSubsribed", isSubscribed)
            intent.putExtra("quicktitle", title)
            Log.i("info", isSubscribed.toString())
            startActivity(intent)
        }

        binding.imageView7!!.setOnClickListener {

            video_Url = homedata[2].service_video!!
            description = homedata[2].service_description!!
            title = homedata[2].service_title!!
            isSubscribed = homedata[2].is_subscribed!!

            val intent = Intent(context, EnrollActivity::class.java)
            intent.putExtra("url", video_Url)
            intent.putExtra("description", description)
            intent.putExtra("title", title)
            intent.putExtra("isSubsribed", isSubscribed)
            startActivity(intent)
        }


        binding.imageView8!!.setOnClickListener {

            video_Url = homedata[3].service_video!!
            description = homedata[3].service_description!!
            title = homedata[3].service_title!!
            isSubscribed = homedata[3].is_subscribed!!

            val intent = Intent(context, ResumeActivity::class.java)
            intent.putExtra("url", video_Url)
            intent.putExtra("description", description)
            intent.putExtra("title", title)
            intent.putExtra("isSubsribed", isSubscribed)
            startActivity(intent)
        }






        return binding.root
    }

    private fun initt() {
        apiClient = ApiClient()
        utilities = Utilities(requireContext())
        if (!::utilities.isInitialized) utilities = Utilities(requireContext())
//        first =  utilities.getStrings(requireContext(),"login")
        yesicamefirstime = utilities.getStrings(requireContext(),"yesicame")
        if (yesicamefirstime.equals(""))
        {
            muteNotification()
        }
    }

    override fun onDetach() {
        super.onDetach()
        utilities = Utilities(requireContext())
        utilities.saveStrings(requireContext(),"login","")
    }
    private fun getHomeData() {


        if (utilities.isConnectingToInternet(requireContext())) {

            val gsonn = Gson()
            val jsonn: String = utilities.getString(requireContext(), "user")
            val obj: User = gsonn.fromJson(jsonn, User::class.java)
            val user_idd: String = java.lang.String.valueOf(obj.id)

            val url = Constants.BASE_URL + "home_screen_data/" + user_idd
            apiClient.getApiService().getHomeData(url)
                .enqueue(object : Callback<HomeResponse> {
                    override fun onResponse(
                        call: Call<HomeResponse>, response: Response<HomeResponse>
                    ) {
                        val signupResponse = response.body()
                        val data = response.body()?.homedata
                        if (signupResponse!!.status == true) {
                            homedata = response.body()!!.homedata
                            binding.indexOneTitle.text = homedata[0].service_title
                            binding.indexTwoTitle.text = homedata[1].service_title
                            binding.indexThreeTitle.text = homedata[2].service_title
                            binding.indexFourTitle.text = homedata[3].service_title
                            Log.d("this", signupResponse.homedata.toString())
                            val tes = response.body()!!.message
                            val gson = Gson()
                            val json = gson.toJson(signupResponse)
                            context?.let { utilities.saveString(it, "homedata", json) }
                        } else {
                            utilities.hideProgressDialog()
                            utilities.customToastFailure(
                                signupResponse.message,
                                requireActivity()
                            )
                        }
                    }
                    override fun onFailure(call: Call<HomeResponse>, t: Throwable) {
                        utilities.customToastFailure(t.toString(), requireActivity())
                    }
                })
        } else {
            Toast.makeText(
                requireContext(),
                "Check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        }

    }

    private fun muteNotification() {
        val dialog = Dialog(requireContext())

        dialog.setContentView(R.layout.mute_notification_popup)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val notificationstatustext = dialog.findViewById<TextView>(R.id.notificationStatus)

        val imgyes = dialog.findViewById<ImageView>(R.id.imgyes)
        val imgNo = dialog.findViewById<ImageView>(R.id.imgNo)
        val ed_suggestions = dialog.findViewById<EditText>(R.id.ed_suggestions)
        utilities.saveStrings(requireContext(),"yesicame","yesicame")
        close.setOnClickListener { dialog.dismiss() }
        imgNo.setOnClickListener {
            utilities.saveString(requireContext(),"notificationOff","off")
            muteNotificationApi()
            dialog.dismiss()
        }
        imgyes.setOnClickListener {
            muteNotificationApi()
            utilities.saveString(requireContext(),"notificationOff","on")
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun muteNotificationApi() {

        val gsonn = Gson()
        val jsonn: String = utilities.getString(requireContext(), "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        val url = Constants.BASE_URL + "mute_notifications/" + user_idd
        apiClient.getApiService().muteNotification(url).enqueue(object :
            Callback<SignupReponse> {

            override fun onResponse(
                call: Call<SignupReponse>, response: Response<SignupReponse>
            ) {
                val signupResponse = response.body()
                if (signupResponse!!.status == true) {
                    val notificationstatus = signupResponse.message
                    utilities.saveString(requireContext(), "mute", notificationstatus)
                    utilities.customToastSuccess(signupResponse.message, requireContext())
                    user = response.body()!!.user
                    val gson = Gson()
                    val json = gson.toJson(user)
                    utilities.saveString(requireContext(), "user", json)

                } else {

                    utilities.customToastFailure(signupResponse.message, requireContext())

                }
            }

            override fun onFailure(call: Call<SignupReponse>, t: Throwable) {

                utilities.customToastFailure(t.toString(), requireContext())
            }


        })

    }


}