package com.fed.fedsense.Fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fed.fedsense.Activities.CardListActivity
import com.fed.fedsense.Activities.VideoDetailActivity
import com.fed.fedsense.util.Utilities
import com.fed.fedsense.Adapters.PaidVideoAdapter
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.Models.VideoDataModel
import com.fed.fedsense.Models.videolesson.VideoLessonPremiumDataModel
import com.fed.fedsense.Models.videolesson.VideoLessonResponseModel
import com.fed.fedsense.R
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PiadVideoFragment : Fragment() {


    lateinit var list: ArrayList<VideoDataModel>
    var adapter: PaidVideoAdapter? = null
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    lateinit var rvDoc: RecyclerView
    var paidVideoData: java.util.ArrayList<VideoLessonPremiumDataModel> = java.util.ArrayList<VideoLessonPremiumDataModel>()
    var isPaid : Boolean = false
    var videoUrl : String = ""
    var videoName : String = ""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view: View = inflater.inflate(R.layout.fragment_piad_video, container, false)


        rvDoc = view.findViewById(R.id.rvFree)
        apiClient = ApiClient()
        utilities = Utilities(requireContext())
        if (!::utilities.isInitialized) utilities = Utilities(requireContext())
        getPaidVideoLesson()


        /*adapter?.setOnItemClickListner(object : PaidVideoAdapter.OnItemClicksListners{
            override fun onClick(position: Int) {
                Toast.makeText(requireContext(),position,Toast.LENGTH_SHORT).show()
                val model = paidVideoData.get(position)
                isPaid = model.is_subscribed
                videoUrl = model.video
                videoName = model.video_title
                val islike = model.is_liked
                val isDislike = model.is_disliked
                val numOfLike  = model.total_likes
                val numOfDislike = model.total_dislikes
                val desc = model.video_description
                val id = model.id
                if (isPaid.equals(true))
                {


                    val intent = Intent(context, VideoDetailActivity::class.java)
                    val bundle = Bundle()
                    bundle.putString("videourl", videoUrl);
                    bundle.putString("videoname", videoName);
                    bundle.putBoolean("islike", islike);
                    bundle.putBoolean("isdislike", isDislike);
                    bundle.putString("totallike", numOfLike);
                    bundle.putString("totaldislike", numOfDislike);
                    bundle.putString("desc", desc);
                    bundle.putString("id",id)
                    intent.putExtras(bundle)
                    startActivity(intent)
                }else{

                    val intent = Intent(context, CardListActivity::class.java)
                    intent.putExtra("amount",model.video_price)
                    intent.putExtra("from","paidvideo")
                    intent.putExtra("videoid",id)
                    startActivity(intent)
                    Toast.makeText(context,"from fragment",Toast.LENGTH_SHORT).show()

                }
            }



        })*/
        utilities.saveStrings(requireContext(),"login","")

        return view
    }




    private fun getPaidVideoLesson() {

        val gsonn = Gson()
        val jsonn: String = utilities.getString(requireContext(), "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        val url = Constants.BASE_URL+"get_video_lessons/"+user_idd
        utilities.showProgressDialog(context, "Loading ...")
        apiClient.getApiService().getvideoLesson(url).enqueue(object :
            Callback<VideoLessonResponseModel> {

            override fun onResponse(call: Call<VideoLessonResponseModel>, response: Response<VideoLessonResponseModel>
            ) {
                val signupResponse = response.body()
                utilities.hideProgressDialog()
                if (signupResponse!!.status == true) {

                    paidVideoData = response.body()!!.data.videopremiumlist
                    rvDoc.setLayoutManager(LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false))
                    adapter = PaidVideoAdapter(requireContext(), paidVideoData)
                    rvDoc.setAdapter(adapter)


                } else {

                   /* Toast.makeText(
                        requireContext(),
                        "" + signupResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()*/
                    utilities.customToastFailure(signupResponse.message,requireContext())


                }
            }

            override fun onFailure(call: Call<VideoLessonResponseModel>, t: Throwable) {

                utilities.hideProgressDialog()
//                Toast.makeText(requireContext(), t.toString(), Toast.LENGTH_SHORT).show()
                utilities.customToastFailure(t.toString(),requireContext())

            }


        })



    }




}