package com.fed.fedsense.Fragments

import android.content.Context
import android.content.Context.WINDOW_SERVICE
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.EditText
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.asad.mykotlinproject.Constants
import com.asad.mykotlinproject.Message
import com.asad.mykotlinproject.UserChat
import com.fed.fedsense.Activities.MainActivity
import com.fed.fedsense.Adapters.FirebaseMessageAdapter
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.Notification.MessageNotificationData
import com.fed.fedsense.Notification.Notifications
import com.fed.fedsense.R
import com.fed.fedsense.util.Database
import com.fed.fedsense.util.Utilities
import com.google.firebase.database.*
import com.google.gson.Gson
import com.mtechsoft.compassapp.services.ApiClient
import java.text.SimpleDateFormat
import java.util.*
import androidx.core.content.ContextCompat.getSystemService

import android.view.Display
import android.webkit.WebSettings
import android.webkit.WebChromeClient
import android.widget.Toast
import com.fed.fedsense.Activities.FAQActivity
import com.fed.fedsense.Fragments.ChatFragment.WebViewController
import com.google.firebase.database.ktx.getValue
import kotlinx.android.synthetic.main.fragment_chat.*


class ChatFragment : Fragment()  {

    lateinit var rv_messages: RecyclerView
    var messageAdapter: FirebaseMessageAdapter? = null
    var messageArrayList: ArrayList<Message> = ArrayList<Message>()
    lateinit var user_id : String
    lateinit var back : ImageView
    lateinit var ivSend : CardView
    lateinit var etChat : EditText
    lateinit var et_message : EditText
    lateinit var mDatabase: FirebaseDatabase
    var database = Database()
    var reciever_user_id = "1"
    var reciever_user_name = ""
    var reciever_user_image = ""
    var currnet_user_id = ""
    var currnet_user_name = ""
    var currnet_user_image = ""
    var messageNode = ""
    var image = ""
    lateinit var constants: Constants
    var unreadMessageCount = 0
    var userName = ""
//    lateinit var webview : WebView
//    lateinit var webview : AdvancedWebView
    var seenListener: ValueEventListener? = null
    var userrefernceforseen: DatabaseReference? = null

    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view : View = inflater.inflate(R.layout.fragment_chat, container, false)

        initt(view)

        utilities.saveStrings(requireContext(),"login","")


        val window = requireActivity().window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.white)
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        back = view.findViewById(R.id.icBack)
        /*webview = view.findViewById(R.id.webview)

        webview = view.findViewById(R.id.webview)
        webview.setListener(activity, this)
        webview.setMixedContentAllowed(false)
        webview.loadUrl("http://compassmytrip.com/fedsense/public/tawk/2");*/
//        try {
//
//            webview.getSettings().setLoadWithOverviewMode(true)
//            webview.getSettings().setUseWideViewPort(true)
//            webview.getSettings().setJavaScriptEnabled(true);
//            webview.setWebViewClient(WebViewController())
//            webview.loadUrl("http://compassmytrip.com/fedsense/public/tawk/2")
//
//        }catch (ex: Exception){
//            Log.d("exceptio",ex.toString())
//        }



        val gsonn = Gson()
        val jsonn: String = utilities.getString(requireContext(), "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)
        userName = obj.name.toString()
        if (userName.equals(""))
        {
            userName = "Fedsense User"
        }
       /* val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse("http://compassmytrip.com/fedsense/public/tawk/"+user_idd)
        )
        startActivity(intent)*/
//        webview.loadUrl("www.facebook.com")

        clicks()
       // getmesssages()
        loadChats()

        val imgFaq = view.findViewById<ImageView>(R.id.imgFaq)
        imgFaq.setOnClickListener {
            startActivity(Intent(context,FAQActivity::class.java))
        }

        seenMesssages()
        return view

    }

    class WebViewController : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }
    }

    private fun clicks() {

        back.setOnClickListener {
            val intent = Intent(requireContext(), MainActivity::class.java)
            startActivity(intent)
        }

        ivSend.setOnClickListener {

            val msg: String = et_message.text.toString()
            if (msg.isEmpty()){

                //Toast.makeText(requireContext(), "Enter message ...", Toast.LENGTH_SHORT).show()
                utilities.customToastFailure("Enter message....",requireContext())


            }else{

                sendMessage(msg)
                Notifications.sendNotification(
                    MessageNotificationData(
                        reciever_user_id,
                        userName,
                        "reciever_image",msg,"chat"),
                    reciever_user_id, context
                )
               /* Toast.makeText(context,reciever_user_id+"reciever id",Toast.LENGTH_SHORT).show()
                Toast.makeText(context,user_id+"user id",Toast.LENGTH_SHORT).show()*/
            }
        }

    }


    private fun sendMessage(msg: String) {
        if (mDatabase != null)
        {
            val message = Message()
            message.read  = "false"
            message.senderUid = user_id
            message.receiverUid = "1"
            message.type = "text"
            message.data = msg
            message.token = ""
            message.seen = "false"
            message.image = image
            message.time = getcurrent_time()
            val messagesNode = getMessagesNode()
            val messageNode = mDatabase.reference.child(database.NODE_MESSAGES).child(messagesNode).push().key
            mDatabase.reference.child(database.NODE_MESSAGES).child(messagesNode)
                .child(messageNode!!).setValue(message)


            val userChat = UserChat()
            userChat.lastMessageTime = getcurrent_time()
            userChat.lastMessage = msg

            val myTopPostsQuery = mDatabase.reference.child(database.NODE_USER_CHATS).child("1")
                .orderByChild("unreadMessageCount")

            myTopPostsQuery.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    if (dataSnapshot.exists()) {
                        for (snapshot in dataSnapshot.children) {
                            val message1 = snapshot.getValue(UserChat::class.java)
                            if(!message1!!.equals(""))
                            {
                                unreadMessageCount = message1!!.unreadMessageCount.toInt()
                            }

                        }
                    }
                    Log.d("kjkjkj",unreadMessageCount.toString())
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    /*Toast.makeText(requireContext(), databaseError.message, Toast.LENGTH_SHORT)
                        .show()*/
                    utilities.customToastFailure(databaseError.message,requireContext())

                }
            })
            Log.d("dvvvv",myTopPostsQuery.toString())
            mDatabase.reference.child(database.NODE_USER_CHATS).child("1")
// add user chat in sending user
            userChat.user_image = image
            userChat.uid = currnet_user_name
            userChat.unique_key = user_id
            userChat.token = ("no token")
            unreadMessageCount = unreadMessageCount+1
            userChat.unreadMessageCount = unreadMessageCount.toString()

            userChat.read = "false"
            mDatabase.getReference().child(database.NODE_USER_CHATS).child(reciever_user_id)
                .child(user_id).setValue(userChat)

            // add user chat in receiving user
            userChat.user_image = image
            userChat.uid = reciever_user_name
            userChat.unique_key = (reciever_user_id)
            userChat.token = "no token"
            userChat.read = "false"
            userChat.unreadMessageCount = (unreadMessageCount+1).toString()
            mDatabase.getReference().child(database.NODE_USER_CHATS).child(user_id)
                .child(reciever_user_id).setValue(userChat)
            et_message.setText("")

        }




    }
    fun loadChats() {
        if (isAdded)
        {
            if (!messageNode.isEmpty()) {
                val messageQuery = mDatabase.reference.child(database.NODE_MESSAGES).child(messageNode)

                messageQuery.addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        if (dataSnapshot.exists()) {

                            messageArrayList.clear()
                            for (snapshot in dataSnapshot.children) {
                                val message = snapshot.getValue(Message::class.java)
                                message!!.key = (snapshot.key!!)
                                messageArrayList.add(message)
                            }
                            rv_messages.layoutManager = LinearLayoutManager(context)
                            messageAdapter = FirebaseMessageAdapter(context!!,messageArrayList)
                            rv_messages.adapter = messageAdapter
                            rv_messages.smoothScrollToPosition(messageAdapter!!.getItemCount() - 1)
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        /*Toast.makeText(requireContext(), databaseError.message, Toast.LENGTH_SHORT)
                            .show()*/
                        utilities.customToastFailure(databaseError.message,requireContext())

                    }
                })

            }
        }

    }

    private fun initt(view: View) {

        apiClient = ApiClient()
        utilities = Utilities(requireContext())
        constants = Constants(requireContext())
        mDatabase = FirebaseDatabase.getInstance()
        if (!::utilities.isInitialized) utilities = Utilities(requireContext())
        back = view.findViewById(R.id.icBack)
        ivSend = view.findViewById(R.id.ivSend)
        et_message = view.findViewById(R.id.et_message)
        rv_messages = view.findViewById(R.id.rv_messages)
        val gsonn = Gson()
        val jsonn: String = utilities.getString(requireContext(), "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        user_id = java.lang.String.valueOf(obj.id)
        currnet_user_id = java.lang.String.valueOf(obj.id)

        currnet_user_name = obj.name.toString()
         image = obj.profile_image!!
        messageNode = getMessagesNode()

    }


    fun getMessagesNode(): String {

        var messageNode: String = ""
        val sendingUID: String = user_id
        val receivingUID = "1"
        messageNode = constants!!.getMessageNode(sendingUID, receivingUID)
        return messageNode

    }

    fun getcurrent_time(): String {
        val c = Calendar.getInstance().time
        val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        return df.format(c)
    }


    private fun seenMesssages() {
        userrefernceforseen =
            FirebaseDatabase.getInstance().reference.child(database.NODE_MESSAGES).child(messageNode)
        seenListener = userrefernceforseen!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (dataSnapshot in snapshot.children) {
                    val message = dataSnapshot.getValue(Message::class.java)
                    if (message?.receiverUid.equals(currnet_user_id) && message?.senderUid.equals(reciever_user_id)
                    ) {
                        val seenMessagehashmap = HashMap<String, Any>()
                        seenMessagehashmap["seen"] = "true"
                        dataSnapshot.ref.updateChildren(seenMessagehashmap)


                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {}
        })
    }


    override fun onPause() {
        super.onPause()
        userrefernceforseen!!.removeEventListener(seenListener!!)
    }

}