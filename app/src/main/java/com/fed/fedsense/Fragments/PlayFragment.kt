package com.fed.fedsense.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.fed.fedsense.Adapters.ViewPagerAdapters
import com.fed.fedsense.R
import com.google.android.material.tabs.TabLayout

class PlayFragment : Fragment() {

    lateinit var tabLayout:  TabLayout
    lateinit var viewPager : ViewPager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view : View = inflater.inflate(R.layout.fragment_play, container, false)

        return view
    }

    override fun onResume() {
        super.onResume()

        tabLayout = view?.findViewById(R.id.tablayout)!!
        viewPager = view?.findViewById(R.id.viewPager)!!

        setupViewPager(viewPager)
        tabLayout.setupWithViewPager(viewPager)
        tabLayout.setBackgroundResource(R.drawable.ripple_effect)

    }


    private fun setupViewPager(viewPager: ViewPager) {

        val viewPagerAdapters = ViewPagerAdapters(childFragmentManager)
        viewPagerAdapters.addFragment(FreeVideoFragment(), "FREE")
        viewPager.adapter = viewPagerAdapters
    }

}
