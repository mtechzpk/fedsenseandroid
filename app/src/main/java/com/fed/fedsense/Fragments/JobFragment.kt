package com.fed.fedsense.Fragments

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fed.fedsense.Adapters.CareerAdapter
import com.fed.fedsense.Adapters.FinalCareerListAdapter
import com.fed.fedsense.Adapters.JobAdapter
import com.fed.fedsense.Adapters.jobs.JobsSelectionListAdapter
import com.fed.fedsense.Models.JobSearch.JobsSearchResponse
import com.fed.fedsense.Models.JobSearch.SearchResultItems
import com.fed.fedsense.Models.jobs.GetJobsResponseModel
import com.fed.fedsense.Models.jobs.JobsDataModel
import com.fed.fedsense.R
import com.fed.fedsense.util.Utilities
import com.google.android.material.card.MaterialCardView
import com.mtechsoft.compassapp.services.ApiClient
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_job.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class JobFragment : Fragment(), CareerAdapter.onExchangeClick,
    FinalCareerListAdapter.onCareerClick {


    private var adapter: JobAdapter? = null
    private lateinit var rvJob: RecyclerView
    private var list: ArrayList<SearchResultItems> = ArrayList()
    private var emptyList: ArrayList<SearchResultItems> = ArrayList()
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    var positioninterest: String = ""
    var keyword: String = ""
    private var loadingPB: ProgressBar? = null
    private lateinit var tvNoFound: TextView
    private var nestedSV: NestedScrollView? = null
    private lateinit var tvApply: ImageView
    var stringBuilder: StringBuilder = StringBuilder()
    var stringBuilderInt: StringBuilder = StringBuilder()
    private lateinit var adapter1: CareerAdapter

    var page: Int = 1
    var limit: Int = 10
    private lateinit var rvCareers: RecyclerView
    var careerList: ArrayList<JobsDataModel> = ArrayList()
    private lateinit var finalCareerAdapter: FinalCareerListAdapter
    lateinit var nameList: Array<String>
    lateinit var tvtitle: TextView
    lateinit var mySelectedList: String
    private var jobsSelectionListAdapter: JobsSelectionListAdapter? = null
    private var locality = ""
    private var stateName = ""
    private var url  = ""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_job, container, false)

        initt(view)
        if (positioninterest.equals("")) {
            keyword = "Software"

        } else {
            keyword = positioninterest
        }
        loadJobs(keyword)
        getCareerListL()


        nestedSV!!.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            // on scroll change we are checking when users scroll as bottom.
            if (scrollY == v.getChildAt(0).measuredHeight - v.measuredHeight) {
                // in this method we are incrementing page number,
                // making progress bar visible and calling get data method.
                page++
                loadingPB!!.visibility = View.VISIBLE
                loadJobs(keyword)
            }
        })


        utilities.saveStrings(requireContext(), "login", "")

        return view
    }

    private fun initt(view: View) {

        apiClient = ApiClient()
        utilities = Utilities(requireContext())
        positioninterest = utilities.getString(requireContext(), "positioninterest")
        stateName = utilities.getString(requireContext(), "stateName")
        locality = utilities.getString(requireContext(), "locality")
        //utilities.getString(requireContext(), "positioninterest")

        tvNoFound = view.findViewById(R.id.tvNoFound)
        tvApply = view.findViewById(R.id.tvApply)
        if (!::utilities.isInitialized) utilities = Utilities(requireContext())

        rvJob = view.findViewById(R.id.rvJob)
        nestedSV = view.findViewById(R.id.nested)
        loadingPB = view.findViewById(R.id.idPBLoading)
        rvCareers = view.findViewById(R.id.rvCareer)
        tvtitle = view.findViewById(R.id.tvtitle)
        tvApply.setOnClickListener {
            if (careerList.isEmpty()) {
                utilities.customToastFailure("Processing data...", requireContext())
            } else {
                submit()
            }
        }
        mySelectedList = utilities.getString(requireContext(), "myLis")
        if (!mySelectedList.equals("")) {
            utilities.saveString(requireContext(), "careerlist", mySelectedList.toString())
            val names = mySelectedList.toString()
            nameList = names.split(",").toTypedArray()
            rvCareers.layoutManager = LinearLayoutManager(
                requireContext(),
                RecyclerView.HORIZONTAL, false
            )
            rvCareers.adapter = FinalCareerListAdapter(requireContext(), nameList, this)
            rvCareers.visibility = View.VISIBLE
            utilities.saveString(requireContext(), "careerlist", nameList.toString())

        } else {
            //nothing here
        }
    }

    private fun getCareerListL() {

        //  utilities.showProgressDialog(context, "Loading ...")
        apiClient.getApiService()
            .jobsGroups()
            .enqueue(object : Callback<GetJobsResponseModel> {

                override fun onResponse(
                    call: Call<GetJobsResponseModel>,
                    response: Response<GetJobsResponseModel>
                ) {

                    // utilities.hideProgressDialog()

                    if (response.isSuccessful) {
                        val respon = response.body()
                        /*rvJob.setLayoutManager(
                            LinearLayoutManager(
                                context,
                                LinearLayoutManager.VERTICAL,
                                false
                            )
                        )
                        adapter = context?.let { JobAdapter(it, emptyList) }
                        rvJob.setAdapter(adapter)
                        if (list.isEmpty()) {
                            tvNoFound.visibility = View.VISIBLE
                            nestedSV?.visibility = View.GONE

                        } else {
                            tvNoFound.visibility = View.GONE
                            nestedSV?.visibility = View.VISIBLE

                        }*/
                        if (respon!!.status == true) {
                            careerList = ArrayList()
                            careerList = respon.data
                        } else {
                            utilities.customToastFailure(
                                "There is some issue to proceed",
                                requireContext()
                            )
                        }
                    } else {

                        Toast.makeText(
                            requireContext(),
                            "" + response.message().toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        utilities.customToastFailure(response.message(), requireContext())


                    }
                }

                override fun onFailure(call: Call<GetJobsResponseModel>, t: Throwable) {

                    //   utilities.hideProgressDialog()
//                    Toast.makeText(requireContext(), t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(), requireContext())

                }


            })

    }


    private fun loadJobs(keyword: String) {
        limit = 10
        if (page > limit) {

            utilities.customToastSuccess("That's all the data..", requireContext())

            loadingPB?.setVisibility(View.GONE);
            return;
        }
        utilities.showProgressDialog(context, "Loading ...")
        if (stateName.equals("")) {
            url = "https://data.usajobs.gov/api/search?Keyword=" + keyword + "&page=" + page
        } else {
            url =
                "https://data.usajobs.gov/api/search?Keyword=" + keyword + "&LocationName=" + stateName + "&page=" + page
        }
        if (isAdded)
        {
            apiClient.getApiService()
                .jobsSearch(url)
                .enqueue(object : Callback<JobsSearchResponse> {
                    override fun onResponse(
                        call: Call<JobsSearchResponse>,
                        response: Response<JobsSearchResponse>
                    ) {

                        utilities.hideProgressDialog()

                        if (response.isSuccessful) {

                            loadingPB?.setVisibility(View.GONE);
                            limit = response.body()!!.SearchResult.UserArea.NumberOfPages.toInt()
                            list = response.body()!!.SearchResult.SearchResultItems
                            emptyList.addAll(list)
                            rvJob.setLayoutManager(
                                LinearLayoutManager(
                                    context,
                                    LinearLayoutManager.VERTICAL,
                                    false
                                )
                            )
                            adapter = context?.let { JobAdapter(it, emptyList) }
                            rvJob.setAdapter(adapter)
                            if (emptyList.isEmpty()) {
                                tvNoFound.visibility = View.VISIBLE
                                nestedSV?.visibility = View.GONE
                                utilities.hideProgressDialog()

                            } else {
                                tvNoFound.visibility = View.GONE
                                nestedSV?.visibility = View.VISIBLE
                                utilities.hideProgressDialog()
                            }


                        } else {
                            utilities.hideProgressDialog()
                            utilities.customToastFailure(response.message(), requireContext())

                        }
                    }

                    override fun onFailure(call: Call<JobsSearchResponse>, t: Throwable) {

                        utilities.hideProgressDialog()
                        utilities.customToastFailure(t.toString(), requireContext())

                    }


                })
        }


    }

    private fun submit() {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_career)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val rvCareerList = dialog.findViewById<RecyclerView>(R.id.rlCareerList)

        rvCareerList.layoutManager = GridLayoutManager(requireContext(), 2)
        jobsSelectionListAdapter = JobsSelectionListAdapter(requireContext(), careerList)
        rvCareerList.adapter = jobsSelectionListAdapter


        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener(View.OnClickListener {
            stringBuilder = java.lang.StringBuilder()
            if (jobsSelectionListAdapter!!.selected.size > 0) {
                for (i in 0 until jobsSelectionListAdapter!!.selected.size) {
                    stringBuilder.append(jobsSelectionListAdapter!!.selected.get(i).name)
                    stringBuilder.append(",")
                }


                // showToast(stringBuilder.toString().trim { it <= ' ' })
                utilities.saveString(requireContext(), "myselectedList", stringBuilder.toString())
                if (!stringBuilder.toString().equals("")) {
                    stringBuilder = stringBuilder.deleteCharAt(stringBuilder.length - 1)
                    utilities.saveString(requireContext(), "myLis", stringBuilder.toString())
                    val names = stringBuilder.toString()
                    nameList = arrayOf("")
                    nameList = names.split(",").toTypedArray()
                    rvCareers.layoutManager = LinearLayoutManager(
                        requireContext(),
                        RecyclerView.HORIZONTAL, false
                    )
                    rvCareers.adapter = FinalCareerListAdapter(requireContext(), nameList, this)
                    rvCareers.visibility = View.VISIBLE
                } else {
                    Toast.makeText(requireContext(), "List is empty", Toast.LENGTH_SHORT).show()
                }
                Log.d("codehere", stringBuilder.toString())
            } else {
                utilities.customToastFailure("No Selection", requireContext())
            }

            dialog.dismiss()

        })

        dialog.show()
    }


    override fun onClickEchange(position: Int) {
        adapter1 = CareerAdapter(requireContext(), careerList, this)
        if (adapter1.getSelected().size > 0) {
            stringBuilder = StringBuilder()
            stringBuilderInt = StringBuilder()
            for (i in 0 until adapter1.getSelected().size) {
                stringBuilder.append(adapter1.getSelected().get(i).name)
                stringBuilder.append(",")
            }
            if (!mySelectedList.equals("")) {
                stringBuilder = stringBuilder.deleteCharAt(stringBuilder.length - 1)
                utilities.saveString(requireContext(), "", stringBuilder.toString())
                utilities.saveString(requireContext(), "myLis", stringBuilder.toString())
            } else {
                stringBuilder = stringBuilder.deleteCharAt(stringBuilder.length - 1)
                utilities.saveString(requireContext(), "myLis", stringBuilder.toString())
            }
        } else {
            utilities.customToastSuccess("Nothing Selected", requireContext())

        }
    }

    override fun onCareerClick(position: Int) {
        val name = nameList[position]
        keyword = name
        emptyList = ArrayList()
        utilities.saveString(requireContext(), "positioninterest", keyword)
        loadJobs(keyword)
    }


    private fun showToast(msg: String) {
        Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
    }
}