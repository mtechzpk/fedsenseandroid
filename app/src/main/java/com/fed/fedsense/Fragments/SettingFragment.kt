package com.fed.fedsense.Fragments

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.facebook.login.LoginManager
import com.fed.fedsense.Activities.*
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.R
import com.fed.fedsense.util.Utilities
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.gson.Gson
import com.mtechsoft.compassapp.networking.Constants
import com.ssw.linkedinmanager.events.LinkedInManagerResponse
import com.ssw.linkedinmanager.ui.LinkedInRequestManager
import android.content.ActivityNotFoundException
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.fed.fedsense.Adapters.MessageAdapter
import com.fed.fedsense.Models.BaseResponse
import com.fed.fedsense.Models.Login.LoginReponse
import com.fed.fedsense.Models.SignUp.SignupReponse
import com.fed.fedsense.Models.getmessage.GetMessageResponseModel
import com.fed.fedsense.Notification.Notifications
import com.google.android.material.card.MaterialCardView
import com.google.android.material.imageview.ShapeableImageView
import com.google.firebase.messaging.FirebaseMessaging
import com.mtechsoft.compassapp.services.ApiClient
import kotlinx.android.synthetic.main.fragment_setting.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SettingFragment : Fragment() {

    lateinit var profile: RelativeLayout
    lateinit var glossary: RelativeLayout
    lateinit var aboutus: RelativeLayout
    lateinit var contactUs: RelativeLayout
    lateinit var logout: RelativeLayout
    lateinit var payment: RelativeLayout
    lateinit var rlSuggestions: RelativeLayout
    lateinit var profileImage: ShapeableImageView
    lateinit var name: TextView
    lateinit var email: TextView
    lateinit var utilities: Utilities
    lateinit var ed_suggestions: EditText
    lateinit var switch: Switch
    var loginWithGoogle = ""
    var loginWithFacebook = ""
    var loginWithLinkedin = ""
    var review = ""
    private lateinit var apiClient: ApiClient
    lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var linkedInRequestManager: LinkedInRequestManager
    lateinit var imgNotification : ImageView
    var notificationstatus = ""
    lateinit var notificationstatustext : TextView
    var notificationOff = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view: View = inflater.inflate(R.layout.fragment_setting, container, false)

        initt(view)


        clicks()

        utilities.saveStrings(requireContext(),"login","")

        return view
    }

    private fun clicks() {


        profile.setOnClickListener {

            val intent = Intent(context, ProfileActivity::class.java)
            startActivity(intent)

        }
        glossary.setOnClickListener {

            val intent = Intent(context, GlossaryActivity::class.java)
            startActivity(intent)

        }
        aboutus.setOnClickListener {

            val intent = Intent(context, AboutUsActivity::class.java)
            startActivity(intent)

        }

        contactUs.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + "fedsense@gmail.com"))
                intent.putExtra(Intent.EXTRA_SUBJECT, "your_subject")
                intent.putExtra(Intent.EXTRA_TEXT, "your_text")
                startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                //TODO smth
            }
        }

        logout.setOnClickListener {
            if (loginWithGoogle.equals("google")) {
                signOut()
            } else if (loginWithFacebook.equals("facebook")) {
                LoginManager.getInstance().logOut();
                utilities.clearSharedPref(requireContext())
                startActivity(
                    Intent(
                        requireContext(),
                        LoginActivity::class.java
                    ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                )
                activity?.finish()
            } else if (loginWithLinkedin.equals("linkedin")) {
                utilities.clearSharedPref(requireContext())
                startActivity(
                    Intent(
                        requireContext(),
                        LoginActivity::class.java
                    ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                )
                activity?.finish()
            } else {
                utilities.clearSharedPref(requireContext())
                startActivity(Intent(requireContext(), LoginActivity::class.java))
                activity?.finish()
            }

        }
        payment.setOnClickListener {
            val intent = Intent(Intent(requireContext(), CardListActivity::class.java))
            intent.putExtra("from", "setting")
            startActivity(intent)
        }
        rlSuggestions.setOnClickListener {
            addReviewPopUp()
        }


        imgNotification.setOnClickListener {
            muteNotification()
        }

        switch.setOnCheckedChangeListener({ _, isChecked ->

            muteNotificationApi()

    //            if (isChecked){
    //                muteNotificationApi()
    //            }
        })

    }


    private fun initt(view: View) {

        profile = view.findViewById(R.id.viewProfile)
        glossary = view.findViewById(R.id.glossary)
        aboutus = view.findViewById(R.id.aboutus)
        contactUs = view.findViewById(R.id.contactus)
        logout = view.findViewById(R.id.logout)
        profileImage = view.findViewById(R.id.profile_image)
        payment = view.findViewById(R.id.paymentInfo)
        name = view.findViewById(R.id.tvname)
        email = view.findViewById(R.id.tvEmail)
        rlSuggestions = view.findViewById(R.id.rlSuggestions)
        imgNotification = view.findViewById(R.id.imgNotification)
        switch = view.findViewById(R.id.switchBtn)
        apiClient = ApiClient()
        if (!::utilities.isInitialized) utilities = Utilities(requireContext())
        loginWithFacebook = context?.let { utilities.getString(it, "loginfacebook") }.toString()
        loginWithGoogle = context?.let { utilities.getString(it, "logingoogle") }.toString()
        loginWithLinkedin = context?.let { utilities.getString(it, "loginLinkedin") }.toString()
        Log.i("check", loginWithFacebook + "" + loginWithGoogle)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(requireContext(), gso);
        notificationstatus = utilities.getString(requireContext(),"mute")

        val gsonn = Gson()
        val jsonn: String = utilities.getString(requireContext(), "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var noti: String = java.lang.String.valueOf(obj.allow_notifications)
        notificationOff = utilities.getString(requireContext(),"notificationOff")
        if (notificationOff.equals("off"))
        {
            switch.isChecked = false
        }else{
            switch.isChecked = true
        }
        if (noti.equals("1")){
            switch.isChecked = true
        }

        if (noti.equals("0")){
            switch.isChecked = false
        }


//        switch.isChecked = utilities.getBoolean(requireContext(),"notification")


    }

    override fun onResume() {
        super.onResume()
        setData()
    }

    private fun muteNotification() {
        val dialog = Dialog(requireContext())

        dialog.setContentView(R.layout.mute_notification_popup)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val notificationstatustext = dialog.findViewById<TextView>(R.id.notificationStatus)

        val imgyes = dialog.findViewById<ImageView>(R.id.imgyes)
        val imgNo = dialog.findViewById<ImageView>(R.id.imgNo)
        val ed_suggestions = dialog.findViewById<EditText>(R.id.ed_suggestions)
        close.setOnClickListener { dialog.dismiss() }
        imgNo.setOnClickListener { dialog.dismiss() }
        imgyes.setOnClickListener {
            muteNotificationApi()
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun muteNotificationApi() {

        val gsonn = Gson()
        val jsonn: String = utilities.getString(requireContext(), "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        val user_idd: String = java.lang.String.valueOf(obj.id)
        utilities.showProgressDialog(requireContext(),"Please wait...")
        val url = Constants.BASE_URL + "mute_notifications/" + user_idd
        apiClient.getApiService().muteNotification(url).enqueue(object :
            Callback<SignupReponse> {

            override fun onResponse(
                call: Call<SignupReponse>, response: Response<SignupReponse>
            ) {
                val signupResponse = response.body()
                utilities.hideProgressDialog()
                if (signupResponse!!.status == true) {
                    val notificationstatus = signupResponse.message
                    utilities.saveString(requireContext(), "mute", notificationstatus)
                    utilities.customToastSuccess(signupResponse.message, requireContext())
                    if (signupResponse.message.equals("Notifications Turned On")){

                        /*val gsonn = Gson()
                        val jsonn: String = utilities.getString(requireContext(), "user")
                        val obj: User = gsonn.fromJson(jsonn, User::class.java)
                        obj.allow_notifications = "1"

                        val gson = Gson()
                        val json = gson.toJson(obj)
                        utilities.saveString(requireContext(), "user", json)*/
                        val user = response.body()!!.user
                        val gson = Gson()
                        val json = gson.toJson(user)
                        utilities.saveString(requireContext(), "user", json)
                        switch.isChecked = true

                    }

                    if (signupResponse.message.equals("Notifications Turned Off")){

                        /*val gsonn = Gson()
                        val jsonn: String = utilities.getString(requireContext(), "user")
                        val obj: User = gsonn.fromJson(jsonn, User::class.java)
                        obj.allow_notifications = "0"

                        val gson = Gson()
                        val json = gson.toJson(obj)
                        utilities.saveString(requireContext(), "user", json)*/
                        val user = response.body()!!.user
                        val gson = Gson()
                        val json = gson.toJson(user)
                        utilities.saveString(requireContext(), "user", json)
                        switch.isChecked = false

                    }


                } else {

                    utilities.customToastFailure(signupResponse.message, requireContext())

                }
            }

            override fun onFailure(call: Call<SignupReponse>, t: Throwable) {
                utilities.hideProgressDialog()
                utilities.customToastFailure(t.toString(), requireContext())
            }


        })

    }

    private fun signOut() {

        mGoogleSignInClient.signOut()
        utilities.clearSharedPref(requireContext())

        startActivity(
            Intent(
                context,
                LoginActivity::class.java
            ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        )
        requireActivity().finish()

    }

    private fun setData() {

        val gsonn = Gson()
        val jsonn: String = utilities.getString(requireContext(), "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)

        val image = obj.profile_image
        val namee = obj.name
        val emaill = obj.email

        if (!image!!.isEmpty()) {
            Glide.with(this)
                .load(Constants.BASE_URL_IMG + image)
                .into(profileImage)
        }

        if (!namee!!.isEmpty()) {
            name.setText(namee)
        }

        if (!emaill!!.isEmpty()) {
            email.setText(emaill)
        }

    }

    private fun addReviewPopUp() {
        val dialog = Dialog(requireContext())

        dialog.setContentView(R.layout.review_and_suggestions_dialog)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        val ed_suggestions = dialog.findViewById<EditText>(R.id.ed_suggestions)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {
            review = ed_suggestions.text.toString()
            if (review.equals("")) {
                utilities.customToastFailure("Please Enter Your Review", requireContext())
            } else {
                dialog.dismiss()
                reviewAndSuggestionApi(review)
            }


        }
        dialog.show()
    }


    private fun reviewAndSuggestionApi( message: String) {
        val gsonn = Gson()
        val jsonn: String = utilities.getString(requireContext(), "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        val user_idd: String = java.lang.String.valueOf(obj.id)
        if (utilities.isConnectingToInternet(requireContext())) {
            utilities.showProgressDialog(context, "Submitting Suggestion ...")
            apiClient.getApiService().reviewAndSuggestions(user_idd, message)
                .enqueue(object : Callback<BaseResponse> {

                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                        utilities.hideProgressDialog()
                        // Toast.makeText(applicationContext, t.toString(), Toast.LENGTH_SHORT).show()
                        utilities.customToastFailure(t.toString(), requireContext())


                    }

                    override fun onResponse(
                        call: Call<BaseResponse>,
                        response: Response<BaseResponse>
                    ) {
                        val signupResponse = response.body()
                        utilities.hideProgressDialog()
                        if (signupResponse!!.status == true) {
                            val message  = response.body()!!.message
                            utilities.customToastSuccess(message,requireContext())
//                            notificationstatustext.text = message
                        } else {
                            utilities.customToastFailure(signupResponse.message, requireContext())
                        }
                    }
                })
        } else {
            Toast.makeText(
                requireContext(),
                "Check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        }

    }


}