package com.fed.fedsense.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fed.fedsense.util.Utilities
import com.fed.fedsense.Adapters.FreeVideoAdapter
import com.fed.fedsense.Adapters.PaidVideoAdapter
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.Models.videolesson.VideoLessonFreeDataModel
import com.fed.fedsense.Models.videolesson.VideoLessonPremiumDataModel
import com.fed.fedsense.Models.videolesson.VideoLessonResponseModel
import com.fed.fedsense.R
import com.google.gson.Gson
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FreeVideoFragment : Fragment() {

    var adapter: FreeVideoAdapter? = null
    var paidAdapter: PaidVideoAdapter? = null
    lateinit var rvFree : RecyclerView
    lateinit var rvPaid : RecyclerView
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    var freeVideoData: java.util.ArrayList<VideoLessonFreeDataModel> = java.util.ArrayList<VideoLessonFreeDataModel>()
    var paidVideoData: java.util.ArrayList<VideoLessonPremiumDataModel> = java.util.ArrayList<VideoLessonPremiumDataModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view: View = inflater.inflate(R.layout.fragment_free_video, container, false)

        rvFree = view.findViewById(R.id.rvFree)
        rvPaid = view.findViewById(R.id.rvPaid)
        apiClient = ApiClient()
        utilities = Utilities(requireContext())
        if (!::utilities.isInitialized) utilities = Utilities(requireContext())
        initRecyclerView()
        utilities.saveStrings(requireContext(),"login","")

        return view
    }

    private fun initRecyclerView() {
      //  list = ArrayList<VideoDataModel>()
       /* for (i in 0..2){
            val item = VideoDataModel()
            item.title = "Lorem Ipsum"
            item.desc = "Lorem is simply dummy text"
            item.icon = R.drawable.play
            item.image = R.drawable.ic_img_video


            list.add(item)
        }*/
/*
        list.add(VideoDataModel(R.drawable.ic_img_video,"Lorem Ipsum","Lorem is simply dummy text","",R.drawable.play))
        list.add(VideoDataModel(R.drawable.ic_img_video,"Lorem Ipsum","Lorem is simply dummy text","",R.drawable.play))
        list.add(VideoDataModel(R.drawable.ic_img_video,"Lorem Ipsum","Lorem is simply dummy text","",R.drawable.play))
        list.add(VideoDataModel(R.drawable.ic_img_video,"Lorem Ipsum","Lorem is simply dummy text","",R.drawable.play))

        rvDoc.setLayoutManager(LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false))
        adapter = VideoAdpater(requireContext(), list)
        rvDoc.setAdapter(adapter)*/
        getFreeVideoLesson()


    }
    private fun getFreeVideoLesson() {

        val gsonn = Gson()
        val jsonn: String = utilities.getString(requireContext(), "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        val url = Constants.BASE_URL+"get_video_lessons/"+user_idd
        utilities.showProgressDialog(context, "Loading ...")
        apiClient.getApiService().getvideoLesson(url).enqueue(object : Callback<VideoLessonResponseModel> {

                override fun onResponse(call: Call<VideoLessonResponseModel>, response: Response<VideoLessonResponseModel>
                ) {
                    if (isAdded)
                    {
                        val signupResponse = response.body()
                        utilities.hideProgressDialog()
                        if (signupResponse!!.status == true) {

                            freeVideoData = response.body()!!.data.videofreelist
                            rvFree.setLayoutManager(LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false))
                            adapter = FreeVideoAdapter(requireContext(), freeVideoData)
                            rvFree.setAdapter(adapter)


                            paidVideoData = response.body()!!.data.videopremiumlist
                            rvPaid.setLayoutManager(LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false))
                            paidAdapter = PaidVideoAdapter(requireContext(), paidVideoData)
                            rvPaid.setAdapter(paidAdapter)


                        } else {

                            /*Toast.makeText(
                                requireContext(),
                                "" + signupResponse.message,
                                Toast.LENGTH_SHORT
                            ).show()*/
                            utilities.customToastFailure(signupResponse.message,requireContext())

                        }
                    }

                }

                override fun onFailure(call: Call<VideoLessonResponseModel>, t: Throwable) {
                    if (isAdded)
                    {
                        utilities.hideProgressDialog()
                        //Toast.makeText(requireContext(), t.toString(), Toast.LENGTH_SHORT).show()
                        utilities.customToastFailure(t.toString(),requireContext())
                    }

                }


            })

    }

}