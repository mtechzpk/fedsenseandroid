package com.fed.fedsense.util

import android.app.Activity
import android.app.ProgressDialog
import android.content.ContentValues.TAG
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Base64
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.fed.fedsense.R
import org.aviran.cookiebar2.CookieBar
import org.aviran.cookiebar2.CookieBarDismissListener
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class Utilities(context: Context) {

    private val context: Context? = null
    var dialog: ProgressDialog? = null


    fun showProgressDialog(ctx: Context?, msg: String?) {
        dialog = ProgressDialog(ctx)
        dialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        dialog!!.setMessage(msg)
        dialog!!.setIndeterminate(true)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
    }

    fun hideProgressDialog() {
        if (dialog != null && dialog!!.isShowing()) {
            dialog!!.cancel()
            dialog = null
        }
    }

    fun saveInt(context: Context, key: String?, value: Int) {

        val sharedPref = context.getSharedPreferences("fedsenseappshared", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putInt(key, value)
        editor.apply()
    }


    fun getInt(context: Context, key: String?): Int {
        val sharedPref = context.getSharedPreferences("fedsenseappshared", Context.MODE_PRIVATE)
        return sharedPref.getInt(key, 0)
    }


    fun saveString(
        context: Context, key: String, value: String
    ) {
        val sharedPref = context.getSharedPreferences("fedsenseappshared", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun getString(context: Context, key: String): String {
        val sharedPref = context.getSharedPreferences("fedsenseappshared", Context.MODE_PRIVATE)
        return sharedPref.getString(key, "").toString()
    }


    fun clearSharedPref(context: Context) {
        val sharedPref =
            context.getSharedPreferences("fedsenseappshared", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.clear()
        editor.apply()
    }

    fun printHashKey(Context: Context) {
        try {
            val info = Context.packageManager.getPackageInfo(
                Context.packageName,
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey: String = String(Base64.encode(md.digest(), 0))
                Log.i(TAG, "printHashKey() Hash Key: $hashKey")
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e(TAG, "printHashKey()", e)
        } catch (e: Exception) {
            Log.e(TAG, "printHashKey()", e)
        }
    }


    fun saveStrings(
        context: Context, key: String, value: String
    ) {
        val sharedPref = context.getSharedPreferences("loginshared", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun getStrings(context: Context, key: String): String {
        val sharedPref = context.getSharedPreferences("loginshared", Context.MODE_PRIVATE)
        return sharedPref.getString(key, "").toString()
    }


    fun saveBoolean(
        context: Context, key: String, value: Boolean
    ) {
        val sharedPref = context.getSharedPreferences("fedsenseappshared", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun getBoolean(context: Context, key: String): Boolean {
        val sharedPref = context.getSharedPreferences("fedsenseappshared", Context.MODE_PRIVATE)
        return sharedPref.getBoolean(key,false)
    }

    fun customToastSuccess(message : String,context: Context)
    {
        CookieBar.build(context as Activity?)
            .setBackgroundColor(R.color.border)
            .setMessage(message)
            .setIcon(R.drawable.ic_baseline_check_24)
            .setDuration(2000)
            .setCookieListener(CookieBarDismissListener { dismissType ->

            })
            .show()
    }

    fun customToastFailure(message : String,context: Context)
    {
        CookieBar.build(context as Activity?)
            .setBackgroundColor(R.color.redColor)
            .setMessage(message)
            .setIcon(R.drawable.ic_baseline_close_24)
            .setDuration(2000)
            .setCookieListener(CookieBarDismissListener { dismissType ->

            })
            .show()
    }

    fun saveStringInterest(
        context: Context, key: String, value: String
    ) {
        val sharedPref = context.getSharedPreferences("interest", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun getStringInterest(context: Context, key: String): String {
        val sharedPref = context.getSharedPreferences("interest", Context.MODE_PRIVATE)
        return sharedPref.getString(key, "").toString()
    }

    fun isConnectingToInternet(context: Context): Boolean {
        val connectivity = context.getSystemService(
            AppCompatActivity.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        if (connectivity != null) {
            val info = connectivity.allNetworkInfo
            if (info != null) for (i in info.indices) if (info[i].state == NetworkInfo.State.CONNECTED) {
                return true
            }
        }
        return false
    }
    fun setWhiteBars(activity: Activity) {
        val window = activity.window
        val view = window.decorView
        view.systemUiVisibility = view.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        view.systemUiVisibility =
            view.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        window.navigationBarColor = activity.resources.getColor(android.R.color.white)
        window.statusBarColor = activity.resources.getColor(android.R.color.white)
    }

}