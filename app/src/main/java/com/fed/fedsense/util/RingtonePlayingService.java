package com.fed.fedsense.util;

import android.app.Service;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.view.View;

import androidx.annotation.Nullable;

public class RingtonePlayingService extends Service {

    private Ringtone ringtone;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
         ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
        ringtone.play();

//        Uri ringtoneUri = Uri.parse(intent.getExtras().getString("ringtone-uri"));
//        this.ringtone = RingtoneManager.getRingtone(this, ringtoneUri);
//        ringtone.play();

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy()
    {
        ringtone.stop();
    }
}
