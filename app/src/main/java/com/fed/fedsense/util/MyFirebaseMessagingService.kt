package com.fed.fedsense.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.fed.fedsense.Activities.IncommigCallActivity
import com.fed.fedsense.R
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONObject
import java.lang.Exception
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.Constants


class MyFirebaseMessagingService: FirebaseMessagingService() {

    var intent: Intent = Intent()
    var title = ""
    var message = ""
    var notification_type = ""
    var token = ""
    var channel = ""
    var notificationType = ""

    private var utilities: Utilities = Utilities(this)
    lateinit var broadcaster: LocalBroadcastManager
    lateinit var pendingIntent : PendingIntent



    override fun onCreate() {
        super.onCreate()
        broadcaster = LocalBroadcastManager.getInstance(this);

    }


    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        if (remoteMessage.data.isNotEmpty()) {


            if (remoteMessage.data.get("notification_type").equals("missed_video_call")){

                val sharedPref : SharedPreferences =
                    getSharedPreferences("TOKEN", Context.MODE_PRIVATE)
                val editor : SharedPreferences.Editor = sharedPref.edit()
                editor.putString("IN_CALL", "")
                editor.apply()

                val intent = Intent("MissedCall")
                broadcaster.sendBroadcast(intent)

            }else{

                try {
                    val params = remoteMessage.data
                    val `object` = JSONObject(params as Map<*, *>)
                    Log.d("param",params.toString())
                    title = `object`.getString("title")
                    message = `object`.getString("message")
                    notification_type = `object`.getString("notification_type")
                    notificationType = `object`.getString("notificationType")



                    Log.e("title", title)
                    Log.e("message", message  )
                    Log.d(TAG, "notitype ${notification_type}")

                    Log.e("notitype",notification_type)

                } catch (e: Exception) {
                    Log.d("exception", e.toString())
                }

                if (notification_type.equals("video_call")){


                    val params = remoteMessage.data
                    val `object` = JSONObject(params as Map<*, *>)
                    token = `object`.getString("call_token")
                    channel = `object`.getString("channel_name")
                    sendNotification(title,message,remoteMessage)
                    notificationType = ""

                    val sharedPreff = getSharedPreferences("TOKEN", Context.MODE_PRIVATE)
                    var incall= sharedPreff.getString("IN_CALL","").toString()
                    if (!incall.equals("true")){

                        startActivity(Intent(this, IncommigCallActivity::class.java)
                            .putExtra("action","direct")
                            .putExtra("token",token)
                            .putExtra("channel",channel).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))

                        val sharedPref : SharedPreferences =
                            getSharedPreferences("TOKEN", Context.MODE_PRIVATE)
                        val editor : SharedPreferences.Editor = sharedPref.edit()
                        editor.putString("IN_CALL", "true")
                        editor.apply()

                        return
                    }



                }else if (notification_type.equals(""))
                {

           //         Notifications.displayNotifications(applicationContext, remoteMessage)
//                    sendNotification(title,message)

                }else{

                    val sharedPref : SharedPreferences =
                        getSharedPreferences("notification", Context.MODE_PRIVATE)
                    val editor : SharedPreferences.Editor = sharedPref.edit()
                    editor.putString("notification", "yes")
                    editor.apply()
                    sendNotification(title,message,remoteMessage)
                    remoteMessage.notification?.let {
                        Log.d(TAG, "Corps de notification de message: ${it.body}")
                    }


                }

            }
            }

        // Check if message contains a notification payload.

    }

    override fun onNewToken(token: String) {

        Log.d(TAG, "Refreshed token: $token")
        val sharedPref : SharedPreferences =
            getSharedPreferences("TOKEN", Context.MODE_PRIVATE)
        val editor : SharedPreferences.Editor = sharedPref.edit()
        editor.putString("FCM_TOKEN", token)
        editor.apply()
    }

    private fun sendNotification(title: String, message: String, remoteMessage: RemoteMessage) {


        Log.d("notification_type",notification_type)
        if (notification_type.equals("video_call")) {


            com.mtechsoft.compassapp.networking.Constants.token = token
            com.mtechsoft.compassapp.networking.Constants.channel = channel
            intent = Intent(this, IncommigCallActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra("action","manual")
            intent.putExtra("token",token)
            intent.putExtra("channel",channel)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        }

        val pendingIntent = PendingIntent.getActivity(this, 0   , intent,
            PendingIntent.FLAG_MUTABLE)

        val channelId = "channel_notification_linker"
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.main_logo)
            .setContentTitle(title)
            .setContentText(message)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0   , notificationBuilder.build())
    }

    companion object {
        private const val TAG = "MyFirebaseMsgService"
    }
}