package com.asad.mykotlinproject

import android.content.Context

class Constants(context: Context) {

    fun getMessageNode(sendingUid: String, receivingUid: String): String {
        var node = ""
        val result = sendingUid.compareTo(receivingUid)
        if (result == 0) {
        }
        else if (result < 0) {
            node = "$sendingUid-$receivingUid"
        }
        else if (result > 0) {
            node = "$receivingUid-$sendingUid"
        }
        return node
    }

}