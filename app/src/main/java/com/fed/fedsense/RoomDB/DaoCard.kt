package com.fed.fedsense.RoomDB

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query


@Dao
interface DaoCard {

    @Query("SELECT * FROM CraditCard")
    fun loadAll(): List<CraditCard>

    @Delete
    fun deleteCard(card: CraditCard)

    @Insert
    fun insertStudent(card: CraditCard)
}