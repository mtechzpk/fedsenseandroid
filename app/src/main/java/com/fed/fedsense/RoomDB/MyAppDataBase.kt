package com.fed.fedsense.RoomDB

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [CraditCard::class], version = 1)
abstract class MyAppDataBase : RoomDatabase() {
    abstract fun cardDao(): DaoCard
}