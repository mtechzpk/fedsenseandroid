package com.fed.fedsense.RoomDB

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CraditCard(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "cardNumber") var cardNumber: String,
    @ColumnInfo(name = "cardHolderName") var cardHolderName: String,
    @ColumnInfo(name = "expiryData") var expiryData: String,
    @ColumnInfo(name = "cvv") var cvv: String,
    @ColumnInfo(name = "status") var status: String

)
