package com.fed.fedsense.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fed.fedsense.Models.CareersDataModel
import com.fed.fedsense.Models.JobDataModel
import com.fed.fedsense.Models.jobs.JobsDataModel
import com.fed.fedsense.R


class CareerAdapter(
    val context: Context,
    var list: ArrayList<JobsDataModel>,
    var mListener: onExchangeClick
) :
    RecyclerView.Adapter<CareerAdapter.ViewHolder>() {

    class ViewHolder(itemView: View, listener: onExchangeClick) : RecyclerView.ViewHolder(itemView) {
        val tvCategory: TextView = itemView.findViewById(R.id.tvCategory);
        var checking : ImageView = itemView.findViewById(R.id.checking)
        var checking2 : ImageView = itemView.findViewById(R.id.checking2)
        var imgSrc : ImageView = itemView.findViewById(R.id.imgSrc)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_career_selection, parent, false)
        return CareerAdapter.ViewHolder(view, mListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model: JobsDataModel = list.get(position)
        holder.tvCategory.text = model.name
        Glide.with(context).load(model.image).into(holder.imgSrc)
        holder.itemView.setOnClickListener {
            mListener.onClickEchange(position)
            if (model.isChecked)
            {
                holder.checking2.visibility = View.GONE
                model.isChecked = false
            }else{
                holder.checking2.visibility = View.VISIBLE
                model.isChecked = true
            }
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface onExchangeClick {
        fun onClickEchange(position: Int)
    }

    fun getData(): ArrayList<JobsDataModel>{
        return list
    }

    fun setData(categoriesListDataModel: ArrayList<JobsDataModel>)
    {
        this.list = ArrayList()
        this.list = categoriesListDataModel

    }
    fun getSelected() : ArrayList<JobsDataModel>
    {
        val selected : ArrayList<JobsDataModel> = ArrayList()
        for (i in 0 until list.size) {
            if (list.get(i).isChecked)
            {
                selected.add(list.get(i))
            }
        }
        return selected
    }

}