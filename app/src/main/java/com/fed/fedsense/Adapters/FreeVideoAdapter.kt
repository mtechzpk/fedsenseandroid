package com.fed.fedsense.Adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.fed.fedsense.Models.videolesson.VideoLessonFreeDataModel
import com.fed.fedsense.R
import com.mtechsoft.compassapp.networking.Constants
import com.bumptech.glide.request.RequestOptions
import com.fed.fedsense.Activities.VideoDetailActivity


class FreeVideoAdapter(val context: Context, val list: ArrayList<VideoLessonFreeDataModel>) :
    RecyclerView.Adapter<FreeVideoAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val image: ImageView = itemView.findViewById(R.id.ivImage);
        val title: TextView = itemView.findViewById(R.id.title);
        val desc: TextView = itemView.findViewById(R.id.desc);
        val ic: ImageView = itemView.findViewById(R.id.icon);
        val play: RelativeLayout = itemView.findViewById(R.id.btnPlay)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_video, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model: VideoLessonFreeDataModel = list.get(position)
        val requestOptions = RequestOptions()
        requestOptions.isMemoryCacheable
        val drawable = CircularProgressDrawable(context)
        drawable.setColorSchemeColors(R.color.black, R.color.black, R.color.black);
        drawable.setCenterRadius(25f);
        drawable.setStrokeWidth(6f);
        drawable.start();
        Glide.with(context).setDefaultRequestOptions(requestOptions)
            .load(Constants.BASE_URL_IMG + model.video).placeholder(drawable).into(holder.image)
        /*Glide.with(context)
            .load(Constants.BASE_URL_IMG + model.video)
            .placeholder(R.color.black)
            .into(holder.iv_profile)*/
        holder.title.text = model.video_title
        holder.desc.text = model.video_description
        //holder.ic.setImageResource(model.icon)

        val videoUrl = model.video
        val videoName = model.video_title
        val islike = model.is_liked
        val isDislike = model.is_disliked
        val numOfLike = model.total_likes
        val numOfDislike = model.total_dislikes
        val desc = model.video_description
        val id = model.id
        holder.play.setOnClickListener {

            holder.play.setBackgroundResource(R.drawable.ripple_effect)
            val intent = Intent(context, VideoDetailActivity::class.java)
            val bundle = Bundle()
            bundle.putString("videourl", videoUrl);
            bundle.putString("videoname", videoName);
            bundle.putBoolean("islike", islike);
            bundle.putBoolean("isdislike", isDislike);
            bundle.putString("totallike", numOfLike);
            bundle.putString("totaldislike", numOfDislike);
            bundle.putString("desc", desc);
            bundle.putString("id",id)
            intent.putExtras(bundle)
            context.startActivity(intent)
        }
        holder.itemView.setOnClickListener {
            val intent = Intent(context, VideoDetailActivity::class.java)
            val bundle = Bundle()
            bundle.putString("videourl", videoUrl);
            bundle.putString("videoname", videoName);
            bundle.putBoolean("islike", islike);
            bundle.putBoolean("isdislike", isDislike);
            bundle.putString("totallike", numOfLike);
            bundle.putString("totaldislike", numOfDislike);
            bundle.putString("desc", desc);
            bundle.putString("id",id)
            intent.putExtras(bundle)
            context.startActivity(intent)
        }


    }

    override fun getItemCount(): Int {
        return list.size
    }

}