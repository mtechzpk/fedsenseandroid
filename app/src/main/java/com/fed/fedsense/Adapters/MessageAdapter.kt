package com.fed.fedsense.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fed.fedsense.Activities.FullImageViewActivity
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.Models.getmessage.GetMessageDataModel
import com.fed.fedsense.R
import com.fed.fedsense.util.Utilities
import com.google.android.material.imageview.ShapeableImageView
import com.google.gson.Gson
import com.mtechsoft.compassapp.networking.Constants
import java.util.*


class MessageAdapter(val context: Context, val listModel: ArrayList<GetMessageDataModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun getItemViewType(position: Int): Int {
        val model :GetMessageDataModel = listModel.get(position)
        if (model.media_type.lowercase().contains("image"))
        {

            return 0
        }else if (model.media_type.lowercase().contains("file"))
        {
            return 1;
        }
        return 2
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view : View
        if (viewType == 0)
        {
            view = layoutInflater.inflate(R.layout.item_image_message,parent,false)
            return ViewHolderImage(view)
        }else if (viewType == 1)
        {
            view = layoutInflater.inflate(R.layout.item_pdf_message,parent,false)
            return ViewHolderPdf(view)
        }else{
            view = layoutInflater.inflate(R.layout.item_text_message,parent,false)
            return ViewHolderText(view)
        }
    }

    @SuppressLint("SimpleDateFormat")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model : GetMessageDataModel = listModel.get(position)
        val utilities = Utilities(context)
        val gsonn = Gson()
        val jsonn: String = utilities.getString(context, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        val user_idd = java.lang.String.valueOf(obj.id)
        val fromId = model.from_id


        if (model.media_type.toLowerCase().contains("image"))
        {
            val viewHolderImage : ViewHolderImage = holder as ViewHolderImage
            val calendar = Calendar.getInstance(Locale.ENGLISH)
            calendar.timeInMillis = model.time.toLong() * 1000L
            val date = DateFormat.format("dd-MM-yyyy HH:mm:ss",calendar).toString()
            viewHolderImage.tv_Sendertime.text = date
            viewHolderImage.tv_Recievertime.text = date
            holder.itemView.setOnClickListener {
                val intent = Intent(context, FullImageViewActivity::class.java)
                intent.putExtra("image",model.media)
                context.startActivity(intent)
            }
            if (user_idd.equals(fromId))
            {

                Glide.with(context).load(Constants.BASE_URL_IMG + model.sender_avatar).into(viewHolderImage.iv_senderProfile)
                Glide.with(context).load(Constants.BASE_URL_IMG + model.media).into(viewHolderImage.senderImageData)
                viewHolderImage.senderLayoutImage.visibility = View.VISIBLE
                viewHolderImage.recieverLayoutImage.visibility = View.GONE

            }else{

                Glide.with(context).load(Constants.BASE_URL_IMG + model.receiver_avatar).into(viewHolderImage.iv_recieverProfile)
                Glide.with(context).load(Constants.BASE_URL_IMG + model.media).into(viewHolderImage.recieverImageData)
                viewHolderImage.senderLayoutImage.visibility = View.GONE
                viewHolderImage.recieverLayoutImage.visibility = View.VISIBLE
            }

        }else if (model.media_type.toLowerCase().contains("file"))
        {
            val viewHolderPdf : ViewHolderPdf = holder as ViewHolderPdf
            val calendar = Calendar.getInstance(Locale.ENGLISH)
            calendar.timeInMillis = model.time.toLong() * 1000L
            val date = DateFormat.format("dd-MM-yyyy HH:mm:ss",calendar).toString()
            viewHolderPdf.tv_Sendertime.text = date
            viewHolderPdf.tv_Recievertime.text = date
            viewHolderPdf.itemView.setOnClickListener {
                utilities.showProgressDialog(context,"Loading...")
                val imageurl: String = model.media
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(Constants.BASE_URL_IMG+imageurl))
                context.startActivity(browserIntent)
                Log.d("pdfurl",Constants.BASE_URL_IMG+imageurl)
                utilities.hideProgressDialog()
            }

            if (user_idd.equals(fromId))
            {

               // viewHolderPdf.tv_Sendertime.text= model.time
                Glide.with(context).load(Constants.BASE_URL_IMG + model.sender_avatar).into(viewHolderPdf.iv_senderProfile)
              //  Glide.with(context).load(Constants.BASE_URL_IMG + model.media).into(viewHolderPdf.senderPdf)
                viewHolderPdf.senderPdf.setImageResource(R.drawable.doc)
                viewHolderPdf.senderLayoutPdf.visibility = View.VISIBLE
                viewHolderPdf.recieverLayoutPdf.visibility = View.GONE
            }else{

               // viewHolderPdf.tv_Recievertime.text= model.time
                Glide.with(context).load(Constants.BASE_URL_IMG + model.receiver_avatar).into(viewHolderPdf.iv_recieverProfile)
              //  Glide.with(context).load(Constants.BASE_URL_IMG + model.media).into(viewHolderPdf.recieverPdf)
                viewHolderPdf.recieverPdf.setImageResource(R.drawable.doc)

                viewHolderPdf.senderLayoutPdf.visibility = View.GONE
                viewHolderPdf.recieverLayoutPdf.visibility = View.VISIBLE
            }

        }else{


            val viewHolderText : ViewHolderText = holder as ViewHolderText
            val calendar = Calendar.getInstance(Locale.ENGLISH)
            calendar.timeInMillis = model.time.toLong() * 1000L
            val date = DateFormat.format("dd-MM-yyyy HH:mm:ss",calendar).toString()
            viewHolderText.tv_Sendertime.text = date
            viewHolderText.tv_Recievertime.text = date


            if (user_idd.equals(fromId))
            {

                //viewHolderText.tv_Sendertime.text= model.time
                Glide.with(context).load(Constants.BASE_URL_IMG + model.sender_avatar).into(viewHolderText.iv_senderProfile)
                viewHolderText.tv_senderMessage.text = model.text
                viewHolderText.senderLayoutText.visibility = View.VISIBLE
                viewHolderText.recieverLayoutText.visibility = View.GONE
            }else{

                //viewHolderText.tv_Recievertime.text= model.time
                Glide.with(context).load(Constants.BASE_URL_IMG + model.receiver_avatar).into(viewHolderText.iv_recieverProfile)
                viewHolderText.tvRecieverMessage.text = model.text
                viewHolderText.senderLayoutText.visibility = View.GONE
                viewHolderText.recieverLayoutText.visibility = View.VISIBLE
            }
        }

    }

    override fun getItemCount(): Int {
        return listModel.size
    }

    class ViewHolderText(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val senderLayoutText : LinearLayout = itemView.findViewById(R.id.senderLayoutText)
        val recieverLayoutText : LinearLayout = itemView.findViewById(R.id.recieverLayoutText)
        val tv_Sendertime : TextView = itemView.findViewById(R.id.tv_Sendertime)
        val iv_senderProfile : ShapeableImageView = itemView.findViewById(R.id.iv_senderProfile)
        val tv_senderMessage : TextView = itemView.findViewById(R.id.tv_senderMessage)
        val tv_Recievertime : TextView = itemView.findViewById(R.id.tv_Recievertime)
        val tvRecieverMessage : TextView = itemView.findViewById(R.id.tvRecieverMessage)
        val iv_recieverProfile : ShapeableImageView = itemView.findViewById(R.id.iv_recieverProfile)
    }

    class ViewHolderImage(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val senderLayoutImage : LinearLayout = itemView.findViewById(R.id.senderLayoutImage)
        val recieverLayoutImage : LinearLayout = itemView.findViewById(R.id.recieverLayoutImage)
        val tv_Sendertime : TextView = itemView.findViewById(R.id.tv_Sendertime)
        val iv_senderProfile : ShapeableImageView = itemView.findViewById(R.id.iv_senderProfile)
        val senderImageData : ImageView = itemView.findViewById(R.id.senderImageData)
        val tv_Recievertime : TextView = itemView.findViewById(R.id.tv_Recievertime)
        val recieverImageData : ImageView = itemView.findViewById(R.id.recieverImageData)
        val iv_recieverProfile : ShapeableImageView = itemView.findViewById(R.id.iv_recieverProfile)
    }

    class ViewHolderPdf(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val senderLayoutPdf : LinearLayout = itemView.findViewById(R.id.senderLayoutPdf)
        val recieverLayoutPdf : LinearLayout = itemView.findViewById(R.id.recieverLayoutPdf)
        val tv_Sendertime : TextView = itemView.findViewById(R.id.tv_Sendertime)
        val iv_senderProfile : ShapeableImageView = itemView.findViewById(R.id.iv_senderProfile)
        val senderPdf : ImageView = itemView.findViewById(R.id.senderPdf)
        val tv_Recievertime : TextView = itemView.findViewById(R.id.tv_Recievertime)
        val recieverPdf : ImageView = itemView.findViewById(R.id.recieverPdf)
        val iv_recieverProfile : ShapeableImageView = itemView.findViewById(R.id.iv_recieverProfile)
    }
}