package com.fed.fedsense.Adapters.jobs

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fed.fedsense.Models.CareersDataModel
import com.fed.fedsense.Models.jobs.JobsDataModel
import com.fed.fedsense.R
import com.google.android.material.imageview.ShapeableImageView


class JobsSelectionListAdapter(private val context: Context, employees: ArrayList<JobsDataModel>) :
    RecyclerView.Adapter<JobsSelectionListAdapter.MultiViewHolder?>() {
    private var employees: ArrayList<JobsDataModel>

    fun setEmployees(employees: ArrayList<JobsDataModel>) {
        this.employees = ArrayList<JobsDataModel>()
        this.employees = employees
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MultiViewHolder {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.item_career_selection, parent, false)
        return MultiViewHolder(view)
    }


    override fun onBindViewHolder(@NonNull multiViewHolder: JobsSelectionListAdapter.MultiViewHolder, position: Int) {
        multiViewHolder.bind(employees[position])
    }



    inner class MultiViewHolder(@NonNull itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private val textView: TextView
        private val imageView: ImageView
        private val shapeableimage : ShapeableImageView
        fun bind(employee: JobsDataModel) {
            imageView.visibility = if (employee.isChecked) View.VISIBLE else View.GONE
            textView.setText(employee.name)
            Glide.with(context).load(employee.image).into(shapeableimage)
            itemView.setOnClickListener(View.OnClickListener {
                employee.isChecked = !employee.isChecked
                imageView.visibility = if (employee.isChecked) View.VISIBLE else View.GONE

            })
        }

        init {
            textView = itemView.findViewById(R.id.tvCategory)
            imageView = itemView.findViewById(R.id.checking2)
            shapeableimage = itemView.findViewById(R.id.imgSrc)
        }
    }

    val all: ArrayList<JobsDataModel>
        get() = employees

    val selected: ArrayList<JobsDataModel>
        get() {
            val selected: ArrayList<JobsDataModel> = ArrayList<JobsDataModel>()
            for (i in employees.indices) {
                if (employees[i].isChecked) {
                    selected.add(employees[i])
                }
            }
            return selected
        }

    init {
        this.employees = employees
    }


    override fun getItemCount(): Int {
        return employees.size
    }


}