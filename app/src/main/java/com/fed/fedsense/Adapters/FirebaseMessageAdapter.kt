package com.fed.fedsense.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.asad.mykotlinproject.Message
import com.bumptech.glide.Glide
import com.fed.fedsense.util.Utilities
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.R
import com.google.android.material.imageview.ShapeableImageView
import com.google.gson.Gson
import com.mtechsoft.compassapp.networking.Constants
import java.util.ArrayList

class FirebaseMessageAdapter(
    val context: Context,
    val listModel: ArrayList<Message>
) :

    RecyclerView.Adapter<FirebaseMessageAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var rl_admin_message: RelativeLayout = itemView.findViewById(R.id.rl_admin_message)
        var rl_my_message: RelativeLayout = itemView.findViewById(R.id.rl_my_message)
        var sendMessageBody: TextView = itemView.findViewById(R.id.sendMessageBody)
        var receiveMessage_body: TextView = itemView.findViewById(R.id.receiveMessage_body)
        var time: TextView = itemView.findViewById(R.id.tv_time)
        var iv_profile: ShapeableImageView = itemView.findViewById(R.id.iv_profile)
        var iv_seenMessage : ImageView = itemView.findViewById(R.id.imgGood)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_message, parent, false)
        return FirebaseMessageAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val util: Utilities? = Utilities(context)
        var currnet_user_id = ""
        var images = ""
        val message: Message = listModel.get(position)
        val gsonn = Gson()
        val jsonn: String = util?.getString(context, "user")!!
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        currnet_user_id = java.lang.String.valueOf(obj.id)
        images = obj.profile_image!!


        if (message.senderUid.equals(currnet_user_id)) {
            holder.rl_admin_message.visibility = View.GONE
            holder.rl_my_message.visibility = View.VISIBLE
            holder.sendMessageBody.setText(message.data)
            holder.time.setText(message.time)
            /*if (message.seen == "false") {

                holder.iv_seenMessage.visibility = View.GONE

            } else {

                holder.iv_seenMessage.visibility = View.VISIBLE
            }*/
            Glide.with(context).load(Constants.BASE_URL_IMG + images).placeholder(R.drawable.placehold).into(holder.iv_profile)
        } else {
            holder.rl_admin_message.visibility = View.VISIBLE
            holder.rl_my_message.visibility = View.GONE
            holder.receiveMessage_body.setText(message.data)
            holder.time.setText(message.time)
            Glide.with(context).load(Constants.BASE_URL_IMG + images).placeholder(R.drawable.placehold).into(holder.iv_profile)

        }

    }

    override fun getItemCount(): Int {
        return listModel.size
    }
}