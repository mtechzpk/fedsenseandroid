package com.fed.fedsense.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fed.fedsense.Models.CareersDataModel
import com.fed.fedsense.R


class FinalCareerListAdapter(
    val context: Context,
    var list: Array<String>,
    var mListener: onCareerClick
) :
    RecyclerView.Adapter<FinalCareerListAdapter.ViewHolder>() {
    var isChecked : Boolean = false

    class ViewHolder(itemView: View, listener: onCareerClick) : RecyclerView.ViewHolder(itemView) {
        val tvCategory: TextView = itemView.findViewById(R.id.tvCareer);
        val tvApply: RelativeLayout = itemView.findViewById(R.id.tvApply);

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_careers, parent, false)
        return FinalCareerListAdapter.ViewHolder(view, mListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvCategory.text = list.get(position)
        holder.itemView.setOnClickListener {
            /*if (!isChecked)
            {
                holder.tvApply.background  =  context.resources.getDrawable(R.drawable.butonback)
                isChecked = true
            }else{
                holder.tvApply.background  =  context.resources.getDrawable(R.drawable.bg_edittext)
                isChecked = false
            }*/
            mListener.onCareerClick(position) }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface onCareerClick {
        fun onCareerClick(position: Int)
    }



}