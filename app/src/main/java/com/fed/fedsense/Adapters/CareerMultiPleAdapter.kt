package com.fed.fedsense.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.fed.fedsense.Models.CareersDataModel
import com.fed.fedsense.Models.jobs.JobsDataModel
import com.fed.fedsense.R
import java.util.ArrayList


class CareerMultiPleAdapter(private val context: Context, employees: ArrayList<JobsDataModel>,    var mListener: CareerAdapter.onExchangeClick
) :
    RecyclerView.Adapter<CareerMultiPleAdapter.MultiViewHolder?>() {
    private var employees: ArrayList<JobsDataModel>
    fun setEmployees(employees: ArrayList<JobsDataModel>) {
        this.employees = ArrayList<JobsDataModel>()
        this.employees = employees
        notifyDataSetChanged()
    }



    override fun onBindViewHolder(@NonNull multiViewHolder: MultiViewHolder, position: Int) {
        multiViewHolder.bind(employees[position])
    }


    inner class MultiViewHolder(@NonNull itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private val textView: TextView
        private val imageView: ImageView
        fun bind(employee: JobsDataModel) {
            imageView.visibility = if (employee.isChecked) View.VISIBLE else View.GONE
            textView.setText(employee.name)
            itemView.setOnClickListener(View.OnClickListener {
                employee.isChecked = (!employee.isChecked)
                imageView.visibility = if (employee.isChecked) View.VISIBLE else View.GONE
            })
        }

        init {
            textView = itemView.findViewById(R.id.textView)
            imageView = itemView.findViewById(R.id.imageView)
        }
    }


    val selected: ArrayList<JobsDataModel>
        get() {
            val selected: ArrayList<JobsDataModel> = ArrayList<JobsDataModel>()
            for (i in employees.indices) {
                if (employees[i].isChecked) {
                    selected.add(employees[i])
                }
            }
            return selected
        }

    init {
        this.employees = employees
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MultiViewHolder {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.item_career_selection, parent, false)
        return MultiViewHolder(view)
    }

    override fun getItemCount(): Int {
        return employees.size
    }
}