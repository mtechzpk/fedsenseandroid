package com.fed.fedsense.Adapters

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.fed.fedsense.Activities.AddCardActivity
import com.fed.fedsense.Activities.CardListActivity
import com.fed.fedsense.Activities.MainActivity
import com.fed.fedsense.Activities.VideoDetailActivity
import com.fed.fedsense.Models.BaseResponse
import com.fed.fedsense.Models.Home.HomeData
import com.fed.fedsense.Models.Home.HomeResponse
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.Models.payment.Card
import com.fed.fedsense.Models.videolesson.VideoLessonPremiumDataModel
import com.fed.fedsense.R
import com.fed.fedsense.RoomDB.CraditCard
import com.fed.fedsense.RoomDB.MyAppDataBase
import com.fed.fedsense.util.Utilities
import com.google.android.material.card.MaterialCardView
import com.google.gson.Gson
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PaidVideoAdapter(val context: Context, val list: ArrayList<VideoLessonPremiumDataModel>) :
    RecyclerView.Adapter<PaidVideoAdapter.ViewHolder>() {

    var utilities: Utilities = Utilities(context)
    val apiClient: ApiClient = ApiClient()
    var videoPrice: String = ""
    var videoId: String = ""
    var singlevideoprice = ""
    var homedata: java.util.ArrayList<HomeData> = java.util.ArrayList()
    var user_id = ""

    private var myAppDatabase: MyAppDataBase = Room.databaseBuilder(
        context,
        MyAppDataBase::class.java, "FEDENSEDB"
    ).allowMainThreadQueries().build()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_paid_video, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val utilities: Utilities = Utilities(context)
        val model: VideoLessonPremiumDataModel = list.get(position)
        val requestOptions = RequestOptions()
        requestOptions.isMemoryCacheable
        val drawable = CircularProgressDrawable(context)
        drawable.setColorSchemeColors(R.color.black, R.color.black, R.color.black);
        drawable.setCenterRadius(25f);
        drawable.setStrokeWidth(6f);
        drawable.start();
        Glide.with(context).setDefaultRequestOptions(requestOptions)
            .load(Constants.BASE_URL_IMG + model.video).placeholder(drawable).into(holder.image)

        holder.title.text = model.video_title
        holder.desc.text = model.video_description
        val videoUrl = model.video
        val videoName = model.video_title
        val islike = model.is_liked
        val isDislike = model.is_disliked
        val numOfLike = model.total_likes
        val numOfDislike = model.total_dislikes
        val desc = model.video_description
        videoId = model.id
        singlevideoprice = model.video_price
        videoPrice = model.video_price + " " + model.video_price_currency
        holder.amountToPay.text = videoPrice

        //holder.ic.setImageResource(model.icon)
        holder.play.setOnClickListener {

            holder.play.setBackgroundResource(R.drawable.ripple_effect)
        }

        val isPaid = model.is_subscribed
        if (isPaid.equals(true)) {

            holder.icon.visibility = View.VISIBLE
            holder.amountToPay.text = videoPrice
            holder.amountToPay.visibility = View.GONE
        } else {
            holder.icon.visibility = View.GONE
            holder.amountToPay.visibility = View.VISIBLE
        }
        holder.itemView.setOnClickListener {
            if (isPaid.equals(true)) {

                val intent = Intent(context, VideoDetailActivity::class.java)
                val bundle = Bundle()
                bundle.putString("videourl", videoUrl);
                bundle.putString("videoname", videoName);
                bundle.putBoolean("islike", islike);
                bundle.putBoolean("isdislike", isDislike);
                bundle.putString("totallike", numOfLike);
                bundle.putString("totaldislike", numOfDislike);
                bundle.putString("desc", desc);
                bundle.putString("id", videoId)
                intent.putExtras(bundle)
                context.startActivity(intent)
            } else {

                Toast.makeText(context, "Please Subscribe first", Toast.LENGTH_SHORT).show()
            }
        }
        holder.play.setOnClickListener {

            if (isPaid.equals(true)) {

                val intent = Intent(context, VideoDetailActivity::class.java)
                val bundle = Bundle()
                bundle.putString("videourl", videoUrl);
                bundle.putString("videoname", videoName);
                bundle.putBoolean("islike", islike);
                bundle.putBoolean("isdislike", isDislike);
                bundle.putString("totallike", numOfLike);
                bundle.putString("totaldislike", numOfDislike);
                bundle.putString("desc", desc);
                bundle.putString("id", videoId)
                intent.putExtras(bundle)
                context.startActivity(intent)
            } else {

                /*Toast.makeText(context,position.toString(),Toast.LENGTH_SHORT).show()
                Toast.makeText(context,model.id.get(position).toString(),Toast.LENGTH_SHORT).show()*/
                videoId = model.id
                videoPrice = model.video_price
                //videoPrice = model.video_price.get(position).toString()
                // Toast.makeText(context,position.toString()+videoId+videoPrice,Toast.LENGTH_SHORT).show()
                /*Toast.makeText(context,videoId,Toast.LENGTH_SHORT).show()
                Toast.makeText(context,videoPrice,Toast.LENGTH_SHORT).show()*/
                /*val intent = Intent(context, CardListActivity::class.java)
                intent.putExtra("amount",model.video_price)
                intent.putExtra("from","paidvideo")
                intent.putExtra("videoid",videoId)
                context.startActivity(intent)
                Toast.makeText(context,videoId,Toast.LENGTH_SHORT).show()*/
                check()

            }
        }


    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val image: ImageView = itemView.findViewById(R.id.ivImage);
        val title: TextView = itemView.findViewById(R.id.title);
        val desc: TextView = itemView.findViewById(R.id.desc);
        val ic: ImageView = itemView.findViewById(R.id.icon);
        val play: RelativeLayout = itemView.findViewById(R.id.btnPlay)
        val icon: ImageView = itemView.findViewById(R.id.icon)
        val amountToPay: TextView = itemView.findViewById(R.id.amountToPay)


    }

    private fun check() {
        var count = myAppDatabase.cardDao().loadAll().size
        if (count > 0) {


            var have = utilities.getString(context, "have")
            if (have.equals("true")) {

                val gsonn = Gson()
                val jsonn: String = utilities.getStrings(context, "mycard")
                if (!jsonn.isEmpty()) {
                    val obj: Card = gsonn.fromJson(jsonn, Card::class.java)
                }
                if (jsonn.isEmpty()) {

                    val intent = Intent(context, CardListActivity::class.java)
                    intent.putExtra("amount", "90")
                    intent.putExtra("from", "schedule")
                    context.startActivity(intent)

                } else {

                    val gsonn = Gson()
                    val jsonn: String = utilities.getStrings(context, "mycard")
                    val card: CraditCard = gsonn.fromJson(jsonn, CraditCard::class.java)
                    payAmount(card)
                }

            } else {

                Toast.makeText(context, "Please select a card", Toast.LENGTH_SHORT).show()
                context.startActivity(Intent(context, CardListActivity::class.java))

            }


        } else {

            addCardPopup()
        }


    }

    private fun addCardPopup() {
        val dialog = Dialog(context)

        dialog.setContentView(R.layout.add_card_dialog)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {

            dialog.dismiss()
            val intent = Intent(Intent(context, AddCardActivity::class.java))
            context.startActivity(intent)

        }
        dialog.show()
    }

    private fun payAmount(card: CraditCard) {

        val gsonn = Gson()
        val jsonn: String = utilities.getString(context, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        user_id = java.lang.String.valueOf(obj.id)
        if (utilities.isConnectingToInternet(context)) {

            var expiry = card.expiryData

            val separated = expiry.split("/").toTypedArray()
            val month = separated[0]
            val year = separated[1]
            utilities.showProgressDialog(context, "Processing ...")
            apiClient.getApiService().charge_payment(
                user_id,
                card.cardNumber,
                month,
                year,
                card.cvv,
                videoPrice

            )
                .enqueue(object : Callback<BaseResponse> {

                    override fun onResponse(
                        call: Call<BaseResponse>,
                        response: Response<BaseResponse>
                    ) {
                        val signupResponse = response.body()
                        utilities.hideProgressDialog()
                        if (signupResponse!!.status == true) {

                            callPaidVideoApi()
                        } else {
                            utilities.hideProgressDialog()
                            utilities.customToastFailure(signupResponse.message, context)

                        }
                    }

                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        utilities.hideProgressDialog()
                        utilities.customToastFailure(t.toString(), context)
                    }
                })
        } else {
            Toast.makeText(
                context,
                "Check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        }


    }

    private fun callPaidVideoApi() {
        val gsonn = Gson()
        val jsonn: String = utilities.getString(context, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        apiClient.getApiService().paidVideoLesson(
            user_idd,
            "video_lesson", videoId, videoPrice, "$"
        )
            .enqueue(object : Callback<BaseResponse> {

                override fun onResponse(
                    call: Call<BaseResponse>, response: Response<BaseResponse>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()
                    if (signupResponse!!.status == true) {
                        utilities.customToastSuccess(response.message(), context)
                        val i = Intent(context, MainActivity::class.java)
                        //pass data to an intent to load a specific fragment of reader activity
                        //pass data to an intent to load a specific fragment of reader activity
                        val fragmnet = "forum"
                        i.putExtra("fragment", fragmnet)
                        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        context.startActivity(i)
                    } else {
                        utilities.customToastFailure(signupResponse.message, context)
                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                    // Toast.makeText(this@Activity3B4, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(), context)

                }


            })


    }

    private fun getHomeData() {


        if (utilities.isConnectingToInternet(context)) {

            val gsonn = Gson()
            val jsonn: String = utilities.getString(context, "user")
            val obj: User = gsonn.fromJson(jsonn, User::class.java)
            var user_idd: String = java.lang.String.valueOf(obj.id)

            val url = Constants.BASE_URL + "home_screen_data/" + user_idd
            utilities.showProgressDialog(context, "Loading ...")
            apiClient.getApiService().getHomeData(url)
                .enqueue(object : Callback<HomeResponse> {

                    override fun onResponse(
                        call: Call<HomeResponse>, response: Response<HomeResponse>
                    ) {
                        val signupResponse = response.body()
                        val data = response.body()?.homedata
                        utilities.hideProgressDialog()
                        if (signupResponse!!.status == true) {

                            homedata = response.body()!!.homedata
                            val gson = Gson()
                            val json = gson.toJson(signupResponse)
                            utilities.saveString(context, "homedata", json)
                            context.startActivity(Intent(context, MainActivity::class.java))


                        } else {
                            utilities.hideProgressDialog()
                            utilities.customToastFailure(
                                signupResponse.message,
                                context

                            )

                        }
                    }

                    override fun onFailure(call: Call<HomeResponse>, t: Throwable) {

                        utilities.hideProgressDialog()
                        utilities.customToastFailure(t.toString(), context)

                    }


                })
        } else {
            Toast.makeText(
                context,
                "Check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        }

    }


}