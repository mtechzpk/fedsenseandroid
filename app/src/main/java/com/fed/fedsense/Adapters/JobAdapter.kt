package com.fed.fedsense.Adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.fed.fedsense.Models.JobSearch.SearchResultItems
import com.fed.fedsense.R


class JobAdapter(val context: Context, val list: ArrayList<SearchResultItems>) :
    RecyclerView.Adapter<JobAdapter.ViewHolder>() {


        class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

            val companyName : TextView = itemView.findViewById(R.id.compnayName)
            val jobpost : TextView = itemView.findViewById(R.id.jobPost)
            val location : TextView = itemView.findViewById(R.id.tvLoaction)
            val apply : CardView = itemView.findViewById(R.id.tvApply)
            val overview: CardView = itemView.findViewById(R.id.tvOverview)

        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_jobs, parent, false)
        return JobAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model: SearchResultItems = list.get(position)
        holder.jobpost.text = model.MatchedObjectDescriptor.PositionTitle
        holder.location.text = model.MatchedObjectDescriptor.PositionLocationDisplay
        holder.companyName.text = model.MatchedObjectDescriptor.OrganizationName
        holder.apply.setOnClickListener {

            holder.apply.setBackgroundResource(R.drawable.ripple_effect)
            val viewIntent = Intent(
                "android.intent.action.VIEW",
                Uri.parse(model.MatchedObjectDescriptor.ApplyURI[0]))
            context.startActivity(viewIntent)
        }

        holder.overview.setOnClickListener {

            holder.overview.setBackgroundResource(R.drawable.ripple_effect)
            val viewIntent = Intent(
                "android.intent.action.VIEW",
                Uri.parse(model.MatchedObjectDescriptor.PositionURI))
            context.startActivity(viewIntent)
        }

    }

    override fun getItemCount(): Int {
         return list.size
    }
}