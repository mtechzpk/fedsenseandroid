package com.fed.fedsense.Adapters

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.fed.fedsense.R
import com.fed.fedsense.RoomDB.CraditCard
import com.fed.fedsense.RoomDB.MyAppDataBase
import com.fed.fedsense.util.Utilities
import com.google.gson.Gson
import com.mtechsoft.compassapp.networking.Constants


class CardListAdapter(val context: Context, val list: List<CraditCard>,val clicklisnetr: SearchItemClickListener) :
    RecyclerView.Adapter<CardListAdapter.ViewHolder>() {

    var id: String? = null
    var utilities: Utilities = Utilities(context)
    var myAppDatabase: MyAppDataBase =
        Room.databaseBuilder(context, MyAppDataBase::class.java, "FEDENSEDB")
            .allowMainThreadQueries().build()


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        val tvCardNumber: TextView = itemView.findViewById(R.id.tvCardNumber)
        val tvCardHolder: TextView = itemView.findViewById(R.id.tvCardHolder)
        val tvCardExpiry: TextView = itemView.findViewById(R.id.tvCardExpiry)
        val ivSelected: ImageView = itemView.findViewById(R.id.ivSelected)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_layout_card, parent, false)
        return CardListAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem: CraditCard = list.get(position)
        holder.tvCardNumber.text =
            "xxxx" + currentItem.cardNumber.substring(currentItem.cardNumber.length - 4)
        holder.tvCardHolder.setText(currentItem.cardHolderName)
        holder.tvCardExpiry.text = "EXP " + currentItem.expiryData

        var selectedCard = utilities.getInt(context, "selectedCard")
        if (currentItem.id == selectedCard) {
            holder.ivSelected.visibility = View.VISIBLE
            Constants.card = currentItem

            val gson = Gson()
            val json = gson.toJson(currentItem)
            utilities.saveStrings(context, "mycard", json)

        }else{
            holder.ivSelected.visibility = View.GONE

        }

        holder.itemView.setOnClickListener {
            utilities.saveInt(context,"selectedCard",currentItem.id)
            utilities.saveString(context,"have","true")
            Constants.card = currentItem

            val gson = Gson()
            val json = gson.toJson(currentItem)
            utilities.saveStrings(context, "mycard", json)

            notifyDataSetChanged()
        }

        holder.itemView.setOnLongClickListener {
            Exitalert(currentItem,position)
            false
           }

//        if (currentItem.status.equals("true")) {
//            holder.ivSelected.visibility = View.VISIBLE
//        } else {
//            holder.ivSelected.visibility = View.GONE
//        }

//        holder.itemView.setOnClickListener {
//
//            id = currentItem.id.toString()
//            Constants.card = currentItem
//            Constants.card_selected = true
//
//            val gson = Gson()
//            val json = gson.toJson(currentItem)
//            utilities.saveStrings(context, "mycard", json)
//
//            for (i in list.indices) {
//                if (id.equals(list[i].id.toString())) {
//
//                    list[i].status = "true"
//                    notifyDataSetChanged()
//                } else {
//                    list[i].status = "false"
//                    notifyDataSetChanged()
//                }
//            }
//        }


    }

    private fun Exitalert(card: CraditCard, position: Int) {

        val builder = AlertDialog.Builder(context)
        builder.setTitle("Delete")

        builder.setPositiveButton("Yes") { dialog, which ->

            var selectedCardid = utilities.getInt(context, "selectedCard")
            if (selectedCardid == card.id){
                utilities.saveString(context,"mycard","")
                utilities.saveString(context,"have","false")

            }

            myAppDatabase.cardDao().deleteCard(card)
            notifyItemRemoved(position)
            notifyDataSetChanged()
            clicklisnetr.onItemDeleted()




        }

        builder.setNegativeButton("No") { dialog, which ->

        }

        builder.show()
    }

    override fun getItemCount(): Int {
        return list.size
    }

    interface SearchItemClickListener {
        fun onItemDeleted()
    }

}