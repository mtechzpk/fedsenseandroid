package com.fed.fedsense.Adapters

import android.content.Context
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fed.fedsense.R
import com.google.android.material.imageview.ShapeableImageView
import com.mtechsoft.compassapp.networking.Constants
import java.util.*
import kotlin.collections.ArrayList

class NotificationAdapter(val context: Context, val list: ArrayList<com.fed.fedsense.Models.notification.NotificationDataModel>) :
    RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val image: ShapeableImageView = itemView.findViewById(R.id.imgNotifi);
        val date: TextView = itemView.findViewById(R.id.date);
        val desc: TextView = itemView.findViewById(R.id.desc);
        val mainView : RelativeLayout = itemView.findViewById(R.id.mainView)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_notifaction, parent, false)
        return NotificationAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model: com.fed.fedsense.Models.notification.NotificationDataModel = list.get(position)
        Glide.with(context)
            .load(Constants.BASE_URL_IMG + model.profile_image).placeholder(R.drawable.placehold).into(holder.image)
        holder.desc.text = model.notification
        holder.date.text = getDate(model.notification_time.toLong())



    }

    private fun getDate(time: Long): String? {
        val cal: Calendar = Calendar.getInstance(Locale.ENGLISH)
        cal.setTimeInMillis(time * 1000)
        return DateFormat.format("dd-MM-yyyy", cal).toString()
    }

    override fun getItemCount(): Int {
        return  list.size

    }

}