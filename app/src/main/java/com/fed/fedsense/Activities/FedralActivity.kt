package com.fed.fedsense.Activities

import android.Manifest.permission
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Gravity
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.WindowManager
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.view.isVisible
import com.fed.fedsense.util.Utilities
import com.fed.fedsense.Models.BaseResponse
import com.fed.fedsense.Models.Home.HomeData
import com.fed.fedsense.Models.Home.HomeResponse
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.R
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.android.material.card.MaterialCardView
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import kotlinx.android.synthetic.main.activity_fedral.*
import kotlinx.android.synthetic.main.resume_popup_yes.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.ArrayList
import kotlin.concurrent.thread


class FedralActivity : AppCompatActivity() {

    lateinit var next: CardView
    lateinit var title: TextView
    lateinit var description: TextView
    lateinit var img_play: ImageView
    lateinit var img_pause: ImageView
    lateinit var video_rel: RelativeLayout
    private val SPLASH_TIME_OUT = 2000L
    private val PICK_FILE = 101
    private val PERMISSION_REQUEST_CODE = 200
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    lateinit var fileto_uploadd: File
    private var player: SimpleExoPlayer? = null
    private var playerView: PlayerView? = null
     var isSubscribes: Boolean = false

    var homedata: ArrayList<HomeData> = ArrayList<HomeData>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_SECURE,
            WindowManager.LayoutParams.FLAG_SECURE
        )
        setContentView(R.layout.activity_fedral)

        initt()
        clicks()
      //  getHomeData()


        next.setOnClickListener {
            dialog()
        }
        img_fullScreen.setOnClickListener {
            val intent : Intent = Intent(this@FedralActivity,FullScreenVideoActivity::class.java)
            utilities.saveString(this@FedralActivity,"videourl",Constants.BASE_URL_IMG + "service_videos/federal_application_manager.mp4")
            startActivity(intent)
        }
    }

    private fun clicks() {

        img_play.setOnClickListener {
            player!!.play()
            img_play.visibility = GONE
        }
        img_pause.setOnClickListener {
            player!!.pause()
            img_play.visibility = VISIBLE

        }

        video_rel.setOnClickListener {

            if (player!!.isPlaying){

                if (img_pause.isVisible){
                    img_pause.visibility = GONE
                }else{
                    img_pause.visibility = VISIBLE
                }
            }
        }

    }

    private fun initt() {

        next = findViewById(R.id.ivNext)
        title = findViewById(R.id.titleText)
        description = findViewById(R.id.discText)
        playerView = findViewById(R.id.video_view)
        img_play = findViewById(R.id.img_play)
        img_pause = findViewById(R.id.img_pause)
        video_rel = findViewById(R.id.video_rel)

        val bundle = intent.extras
        title.setText(intent.getStringExtra("title"))
        isSubscribes = bundle!!.getBoolean("isSubsribed")
        description.setText(intent.getStringExtra("description"))
        Log.i("sub",isSubscribes.toString())
        utilities = Utilities(this)
        apiClient = ApiClient()
        if (!::utilities.isInitialized) utilities = Utilities(this)

        Dexter.withActivity(this@FedralActivity)
            .withPermissions(
                permission.READ_EXTERNAL_STORAGE,
                permission.WRITE_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                    }
                    if (report.isAnyPermissionPermanentlyDenied) {
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).check()

       // isSubscribes = utilities.getBoolean(this@FedralActivity,"isNow")


        setVideo()
    }

    fun setVideo(){

        val appNameStringRes = R.string.app_name
        val trackSelectorDef: TrackSelector = DefaultTrackSelector(this)
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelectorDef)
        val userAgent = Util.getUserAgent(this, this.getString(appNameStringRes))
        val defaultDataSourceFactory = DefaultDataSourceFactory(this, userAgent)
        val uriOfContentUrl = Uri.parse(Constants.BASE_URL_IMG + "service_videos/federal_application_manager.mp4")
        val mediaSource: MediaSource = ProgressiveMediaSource.Factory(defaultDataSourceFactory)
            .createMediaSource(uriOfContentUrl)
        //MediaSource mediaSource = new ExtractorMediaSource(uriOfContentUrl, new CacheDataSourceFactory(context, 100 * 1024 * 1024, 500 * 1024 * 1024), new DefaultExtractorsFactory(), null, null);
        //MediaSource mediaSource = new ExtractorMediaSource(uriOfContentUrl, new CacheDataSourceFactory(context, 100 * 1024 * 1024, 500 * 1024 * 1024), new DefaultExtractorsFactory(), null, null);
        player!!.prepare(mediaSource)
        player!!.setPlayWhenReady(false)
        playerView!!.requestFocus()
        playerView!!.player = player
        playerView!!.setShowBuffering(PlayerView.SHOW_BUFFERING_WHEN_PLAYING)
        player!!.setVolume(1f)

    }



    private fun dialog() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.resume_dialog)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<CardView>(R.id.tvYes)
        val no = dialog.findViewById<CardView>(R.id.tvNo)
        val imgYes = dialog.findViewById<ImageView>(R.id.imgyes)
        val imgNo = dialog.findViewById<ImageView>(R.id.imgNo)
        close.setOnClickListener { dialog.dismiss() }

        yes.setOnClickListener {

//            yes.setBackgroundResource(R.drawable.bg_button)

            imgYes.setBackgroundResource(R.drawable.butonback)
            imgNo.setBackgroundResource(R.drawable.greybuton)
           /* if (isSubscribes.equals(false))
            {
                startActivity(Intent(this@FedralActivity,SubscriptionActivity::class.java))
                finish()
            }else{
                resume()
            }*/
            resume()
            dialog.dismiss()


        }
        no.setOnClickListener {
            val intent = Intent(this@FedralActivity,SubscriptionActivity::class.java)
            val bundle = Bundle()
            bundle.putString("noclick", "noclick");
            intent.putExtras(bundle)

            startActivity(intent)

            dialog.dismiss() }
        // no.setBackgroundResource(R.drawable.bg_buttonnoo)
        imgYes.setBackgroundResource(R.drawable.greybuton)


        dialog.show()

    }

    private fun resume() {

        val dialog = Dialog(this)
        dialog.setContentView(R.layout.resume_popup_yes)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val upload = dialog.findViewById<MaterialCardView>(R.id.tvUplaod)
        close.setOnClickListener { dialog.dismiss() }
        upload.setOnClickListener {

            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
          //  intent.type = "application/pdf"
            intent.type = "application/*"
            startActivityForResult(intent, PICK_FILE)
            dialog.dismiss()

        }
        dialog.show()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {


            PICK_FILE -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val uri = data.data
                    if (uri != null) {

                        val fileName = getFileNameByUri(uri)
                        copyUriToExternalFilesDir(uri, fileName)

                    }
                }
            }
        }

    }

    private fun getFileNameByUri(uri: Uri): String {
        var fileName = System.currentTimeMillis().toString()
        val cursor = contentResolver.query(uri, null, null, null, null)
        if (cursor != null && cursor.count > 0) {
            cursor.moveToFirst()
            fileName = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME))
            cursor.close()
        }
        return fileName
    }

    private fun copyUriToExternalFilesDir(uri: Uri, fileName: String) {
        thread {
            val inputStream = contentResolver.openInputStream(uri)
            val tempDir = getExternalFilesDir("temp")
            if (inputStream != null && tempDir != null) {
                fileto_uploadd = File("$tempDir/$fileName")
                val fos = FileOutputStream(fileto_uploadd)
                val bis = BufferedInputStream(inputStream)
                val bos = BufferedOutputStream(fos)
                val byteArray = ByteArray(1024)
                var bytes = bis.read(byteArray)
                while (bytes > 0) {
                    bos.write(byteArray, 0, bytes)
                    bos.flush()
                    bytes = bis.read(byteArray)
                }
                bos.close()
                fos.close()
                Log.d("address",fileto_uploadd.toString())
                runOnUiThread {
                    if (isSubscribes.equals(false))
                    {
                        val intent = Intent(this@FedralActivity,SubscriptionActivity::class.java)
                        val bundle = Bundle()
                        bundle.putString("filePath", fileto_uploadd.toString());
                        intent.putExtras(bundle)
                        startActivity(intent)
                    }else{
                        uploadResume(fileto_uploadd)

                    }

//                    Toast.makeText(this, "Copy file into $tempDir succeeded.", Toast.LENGTH_LONG).show()
                }
            }
        }
    }



    private fun uploadResume(file: File) {

        utilities.showProgressDialog(this, "Uploading ...")

        val gsonn = Gson()
        val jsonn: String = utilities.getString(this, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        val user_id: RequestBody = RequestBody.create(MediaType.parse("text/plain"), user_idd)
        val service_id: RequestBody = RequestBody.create(MediaType.parse("text/plain"), "1")
        val requestBody: RequestBody = RequestBody.create(MediaType.parse("*/*"), file)
        val fileToUpload = MultipartBody.Part.createFormData("resume", file.getName(), requestBody)

        apiClient.getApiService().upload_resume(
            user_id,
            service_id,
            fileToUpload
        )
            .enqueue(object : retrofit2.Callback<BaseResponse> {
                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {

                    if (response.isSuccessful) {

                        utilities.hideProgressDialog()
                        val status: Boolean = response.body()!!.status
                        if (status.equals(true)) {

                            val message: String = response.body()!!.message
                           // Toast.makeText(this@FedralActivity, message, Toast.LENGTH_SHORT).show()
                            utilities.customToastSuccess(message,this@FedralActivity)

                            ook()


                        } else {
                            val message: String = response.body()!!.message
                           // Toast.makeText(this@FedralActivity, message, Toast.LENGTH_SHORT).show()
                            utilities.customToastFailure(message,this@FedralActivity)

                        }
                    } else {

                        utilities.hideProgressDialog()
                        val message: String = response.body()!!.message
                      //  Toast.makeText(this@FedralActivity, message, Toast.LENGTH_SHORT).show()
                        utilities.customToastFailure(message,this@FedralActivity)

                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    utilities.hideProgressDialog()
                    /*Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                        .show()*/
                    utilities.customToastFailure(t.message.toString(),this@FedralActivity)

                }
            })

    }

    private fun afteruplaodDialouge() {

        val dialog = Dialog(this)

        dialog.setContentView(R.layout.popup5c)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {
            ook()
        }

        dialog.show()
    }

    private fun ook() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.dialog_federal_after_success_upload)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            dialog.dismiss()
            finish()

        }

        dialog.show()
    }

    override fun onDestroy() {
        super.onDestroy()

        player!!.stop()
        player!!.release()
    }




}
