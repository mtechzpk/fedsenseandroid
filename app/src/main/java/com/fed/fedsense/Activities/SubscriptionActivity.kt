package com.fed.fedsense.Activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.android.billingclient.api.*
import com.fed.fedsense.Models.BaseResponse
import com.fed.fedsense.Models.Home.HomeData
import com.fed.fedsense.Models.Home.HomeResponse
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.R
import com.fed.fedsense.util.Utilities
import com.google.android.material.card.MaterialCardView
import com.google.gson.Gson
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class SubscriptionActivity : AppCompatActivity(), PurchasesUpdatedListener {

    lateinit var subsscribe: CardView
    lateinit var back: ImageView
    var billingClient: BillingClient? = null
    var packagename: String = "upload_resume"
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    var todayDate = ""
    var endDate = ""
    var startTime = ""
    var homedata: java.util.ArrayList<HomeData> = java.util.ArrayList<HomeData>()
    private var video_Url: String = ""
    private var description: String = ""
    private var title: String = ""
    private var isSubscribed = false

    var filePath: File? = null
    var noClick = ""



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val window = window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_subscription)

        init()
        onClicks()

    }
    private fun init()
    {
        subsscribe = findViewById(R.id.tvSubNow)
        back = findViewById(R.id.icBack)
        back.setOnClickListener { onBackPressed()}

        val bundle = intent.extras

        noClick = intent.getStringExtra("noclick").toString()
        if (!noClick.equals("noclick"))
        {
            filePath = File(intent.getStringExtra("filePath"))
        }
//        Toast.makeText(this,filePath.toString(),Toast.LENGTH_LONG).show()

        startTime = SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Date())
        val now = Calendar.getInstance()
        todayDate = now[Calendar.YEAR].toString()+"-"+(now[Calendar.MONTH] + 1).toString()+"-"+now[Calendar.DATE].toString()
        // add days to current date using Calendar.add method
        now.add(Calendar.MONTH, 2)
        endDate = now[Calendar.YEAR].toString() + "-" + (now[Calendar.MONTH]).toString()+"-"+now[Calendar.DATE].toString()
        Log.d("date",startTime+""+todayDate)
        Log.d("time",startTime+""+endDate)
        billingClient = BillingClient.newBuilder(this).enablePendingPurchases().setListener(this).build()

        apiClient = ApiClient()
        utilities = Utilities(this@SubscriptionActivity)
        if (!::utilities.isInitialized) utilities = Utilities(this)


    }
    private fun onClicks()
    {
        subsscribe.setOnClickListener {

            subscribe()
        }
    }


    fun subscribe() {
        //check if service is already connected
        if (billingClient!!.isReady) {
            initiatePurchase()
        } else {
            billingClient = BillingClient.newBuilder(this).enablePendingPurchases().setListener(this).build()
            billingClient!!.startConnection(object : BillingClientStateListener {
                override fun onBillingSetupFinished(billingResult: BillingResult) {
                    if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                        initiatePurchase()
                    } else {
                       // Toast.makeText(applicationContext, "Error " + billingResult.debugMessage, Toast.LENGTH_SHORT).show()
                        utilities.customToastFailure(billingResult.debugMessage,this@SubscriptionActivity)

                    }
                }

                override fun onBillingServiceDisconnected() {
                  //  Toast.makeText(applicationContext, "Service Disconnected ", Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure("Service Disconnected ",this@SubscriptionActivity)

                }
            })
        }
    }

    private fun initiatePurchase() {

        val skuList: MutableList<String> = ArrayList()
        skuList.add(packagename)
        val params = SkuDetailsParams.newBuilder()
        params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS)
        val billingResult = billingClient!!.isFeatureSupported(BillingClient.FeatureType.SUBSCRIPTIONS)
        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
            billingClient!!.querySkuDetailsAsync(params.build()
            ) { billingResult, skuDetailsList ->
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                    if (skuDetailsList != null && skuDetailsList.size > 0) {
                        val flowParams = BillingFlowParams.newBuilder()
                            .setSkuDetails(skuDetailsList[0])
                            .build()
                        billingClient!!.launchBillingFlow(this, flowParams)
                    } else {
                        //try to add subscription item "sub_example" in google play console
                      //  Toast.makeText(applicationContext, "Item not Found", Toast.LENGTH_SHORT).show()
                        utilities.customToastFailure("Item not Found",this@SubscriptionActivity)

                    }
                } else {

                    utilities.customToastFailure(billingResult.debugMessage,this@SubscriptionActivity)

                }
            }
        } else {
            utilities.customToastFailure("Sorry Subscription not Supported. Please Update Play Store",this@SubscriptionActivity)

        }
    }


    fun handlePurchases(purchases: List<Purchase>) {
        for (purchase in purchases) {
            //if item is purchased
            for (sku in purchase.skus)
            {
                if (packagename == sku && purchase.purchaseState == Purchase.PurchaseState.PURCHASED)
                {
                    if (!purchase.isAcknowledged) {
                        val acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder()
                            .setPurchaseToken(purchase.purchaseToken)
                            .build()
                        billingClient!!.acknowledgePurchase(acknowledgePurchaseParams, ackPurchase)
                    } else {
                        // Grant entitlement to the user on item purchase
                        // restart activity

                        // Toast.makeText(applicationContext, "Item Purchased", Toast.LENGTH_SHORT).show()
                        // utilities.customToastSuccess("Item Purchased",this@SubscriptionActivity)

                    }
                }else if (packagename == sku && purchase.purchaseState == Purchase.PurchaseState.PENDING) {
                    /*Toast.makeText(applicationContext,
                        "Purchase is Pending. Please complete Transaction", Toast.LENGTH_SHORT).show()*/
                    utilities.customToastFailure("Purchase is Pending. Please complete Transaction",this@SubscriptionActivity)

                } else if (packagename == sku && purchase.purchaseState == Purchase.PurchaseState.UNSPECIFIED_STATE) {
//                Toast.makeText(applicationContext, "Purchase Status Unknown", Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure("Purchase Status Unknown",this@SubscriptionActivity)

                }
            }
            /*if (packagename == purchase.skus && purchase.purchaseState == Purchase.PurchaseState.PURCHASED) {

                if (!purchase.isAcknowledged) {
                    val acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder()
                        .setPurchaseToken(purchase.purchaseToken)
                        .build()
                    billingClient!!.acknowledgePurchase(acknowledgePurchaseParams, ackPurchase)
                } else {
                    // Grant entitlement to the user on item purchase
                    // restart activity

                   // Toast.makeText(applicationContext, "Item Purchased", Toast.LENGTH_SHORT).show()
                   // utilities.customToastSuccess("Item Purchased",this@SubscriptionActivity)

                }
            } else if (packagename == purchase.sku && purchase.purchaseState == Purchase.PurchaseState.PENDING) {
                *//*Toast.makeText(applicationContext,
                    "Purchase is Pending. Please complete Transaction", Toast.LENGTH_SHORT).show()*//*
                utilities.customToastFailure("Purchase is Pending. Please complete Transaction",this@SubscriptionActivity)

            } else if (packagename == purchase.sku && purchase.purchaseState == Purchase.PurchaseState.UNSPECIFIED_STATE) {
//                Toast.makeText(applicationContext, "Purchase Status Unknown", Toast.LENGTH_SHORT).show()
                utilities.customToastFailure("Purchase Status Unknown",this@SubscriptionActivity)

            }*/
        }
    }


    var ackPurchase = AcknowledgePurchaseResponseListener { billingResult ->
        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {

//           uploadReumse()
//            Toast.makeText(this@SubscriptionActivity,"You have subscribed",Toast.LENGTH_LONG).show()
            utilities.customToastSuccess("You have subscribed",this@SubscriptionActivity)
            federalApplicationApi()


        }else{
            Toast.makeText(this,"Not Subscribed",Toast.LENGTH_SHORT).show()
        }
    }

    private fun federalApplicationApi() {

            val gsonn = Gson()
            val jsonn: String = utilities.getString(this@SubscriptionActivity, "user")
            val obj: User = gsonn.fromJson(jsonn, User::class.java)
            var user_idd: String = java.lang.String.valueOf(obj.id)

            apiClient.getApiService().federalApplication(user_idd,
                "federal_application_manager",
            "30","days",
                todayDate+" "+startTime,
                endDate+" "+startTime,
                "130","$")
                .enqueue(object : Callback<BaseResponse> {

                    override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>
                    ) {
                        val signupResponse = response.body()
                        val data = response.body()
                        if (signupResponse!!.status == true) {
                           // getHomeData()
                            if (noClick.equals("noclick"))
                            {
                                ook()
                            }else{
                                uploadResume(filePath!!)

                            }





                        } else {
                            utilities.customToastFailure(signupResponse.message,this@SubscriptionActivity)


                        }
                    }

                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                        utilities.customToastFailure(t.toString(),this@SubscriptionActivity)

                    }


                })


    }



    private fun setPurchaseData() {
        TODO("Not yet implemented")
    }


    override fun onPurchasesUpdated(billingResult: BillingResult, purchases: MutableList<Purchase>?) {

        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {

            val token: String = purchases.get(0).purchaseToken
            handlePurchases(purchases)
        }
        else if (billingResult.responseCode == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
            val queryAlreadyPurchasesResult = billingClient!!.queryPurchases(BillingClient.SkuType.SUBS)
            val alreadyPurchases = queryAlreadyPurchasesResult.purchasesList
            alreadyPurchases?.let { handlePurchases(it) }
        }
        else if (billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
//            Toast.makeText(applicationContext, "Purchase Canceled", Toast.LENGTH_SHORT).show()
            utilities.customToastFailure("Purchase Canceled",this@SubscriptionActivity)

        }
        else {
//            Toast.makeText(applicationContext, "Error " + billingResult.debugMessage, Toast.LENGTH_SHORT).show()
            utilities.customToastFailure(billingResult.debugMessage,this@SubscriptionActivity)

        }
    }

    private fun uploadResume(file: File) {

        utilities.showProgressDialog(this, "Uploading ...")

        val gsonn = Gson()
        val jsonn: String = utilities.getString(this, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        val user_id: RequestBody = RequestBody.create(MediaType.parse("text/plain"), user_idd)
        val service_id: RequestBody = RequestBody.create(MediaType.parse("text/plain"), "1")
        val requestBody: RequestBody = RequestBody.create(MediaType.parse("*/*"), file)
        val fileToUpload = MultipartBody.Part.createFormData("resume", file.getName(), requestBody)
        utilities.showProgressDialog(this@SubscriptionActivity,"Uploading Resume....")
        apiClient.getApiService().upload_resume(
            user_id,
            service_id,
            fileToUpload
        )
            .enqueue(object : retrofit2.Callback<BaseResponse> {
                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {

                    if (response.isSuccessful) {

                        utilities.hideProgressDialog()
                        val status: Boolean = response.body()!!.status
                        if (status.equals(true)) {

                            val message: String = response.body()!!.message
                            // Toast.makeText(this@FedralActivity, message, Toast.LENGTH_SHORT).show()
                            utilities.customToastSuccess(message,this@SubscriptionActivity)

                            ook()


                        } else {
                            utilities.hideProgressDialog()
                            val message: String = response.body()!!.message
                            // Toast.makeText(this@FedralActivity, message, Toast.LENGTH_SHORT).show()
                            utilities.customToastFailure(message,this@SubscriptionActivity)

                        }
                    } else {

                        utilities.hideProgressDialog()
                        val message: String = response.body()!!.message
                        //  Toast.makeText(this@FedralActivity, message, Toast.LENGTH_SHORT).show()
                        utilities.customToastFailure(message,this@SubscriptionActivity)

                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    utilities.hideProgressDialog()
                    /*Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                        .show()*/
                    utilities.customToastFailure(t.message.toString(),this@SubscriptionActivity)

                }
            })

    }
    private fun ook() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.dialog_federal_after_success_upload)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            dialog.dismiss()
            finish()

        }

        dialog.show()
    }

    private fun getHomeData() {

        val gsonn = Gson()
        val jsonn: String = utilities.getString(this@SubscriptionActivity, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)
        utilities.showProgressDialog(this, "Please wait ...")

        val url = Constants.BASE_URL+"home_screen_data/"+user_idd
        apiClient.getApiService().getHomeData(url)
            .enqueue(object : Callback<HomeResponse> {

                override fun onResponse(call: Call<HomeResponse>, response: Response<HomeResponse>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()?.homedata
                    if (signupResponse!!.status == true) {
                        utilities.hideProgressDialog()
                        uploadResume(filePath!!)
                        video_Url = data!![0].service_video!!
                        description = data[0].service_description!!
                        title = data[0].service_title!!
                        isSubscribed = data[0].is_subscribed!!
                        /* val intent = Intent(this@SubscriptionActivity, FedralActivity::class.java)
                         intent.putExtra("url",video_Url)
                         intent.putExtra("description",description)
                         intent.putExtra("title",title)
                         intent.putExtra("isSubsribed",isSubscribed)
                         Log.i("ckk",isSubscribed.toString())
                         startActivity(intent)
                         finish()*/

                    } else {


                        utilities.hideProgressDialog()
                        utilities.customToastFailure(signupResponse.message,this@SubscriptionActivity)


                    }
                }

                override fun onFailure(call: Call<HomeResponse>, t: Throwable) {
                    utilities.hideProgressDialog()
//                    Toast.makeText(this@SubscriptionActivity, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(),this@SubscriptionActivity)

                }


            })

    }

}
