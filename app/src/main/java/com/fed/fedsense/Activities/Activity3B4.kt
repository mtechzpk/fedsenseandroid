package com.fed.fedsense.Activities

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.fed.fedsense.Models.BaseResponse
import com.fed.fedsense.Models.Home.HomeResponse
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.R
import com.fed.fedsense.util.Utilities
import com.google.android.material.card.MaterialCardView
import com.google.gson.Gson
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class Activity3B4 : AppCompatActivity() {

    lateinit var submit: CardView
    lateinit var imgBack: ImageView

    lateinit var closingDates: TextView
    lateinit var announcementNumberEd: EditText
    lateinit var edPositionTitleEd: EditText
    private lateinit var datePickerDialog: DatePickerDialog
    var amountToPay = ""
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    private var video_Url: String = ""
    private var description: String = ""
    private var title: String = ""
    private var isSubscribed = false
    lateinit var obj: HomeResponse
    var yes: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val window = window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_activity3_b4)


        init()
        onClicksI()
        datePicker()


    }

    private fun init() {


        apiClient = ApiClient()
        utilities = Utilities(this@Activity3B4)
        if (!::utilities.isInitialized) utilities = Utilities(this)
        //current time
        startTime = SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Date())
        //end time with one hour of increment here
        val sdf = SimpleDateFormat("HH:mm:ss")
        val calendarAdd = Calendar.getInstance()
        calendarAdd.add(Calendar.HOUR, 1)
        endTime = sdf.format(calendarAdd.time)

        val now = Calendar.getInstance()
        todayDate =
            now[Calendar.YEAR].toString() + "-" + (now[Calendar.MONTH] + 1).toString() + "-" + now[Calendar.DATE].toString()
        // add days to current date using Calendar.add method
        now.add(Calendar.MONTH, 2)
        endDate =
            now[Calendar.YEAR].toString() + "-" + (now[Calendar.MONTH]).toString() + "-" + now[Calendar.DATE].toString()
        submit = findViewById(R.id.btnSubmit)
        closingDates = findViewById(R.id.closingDate)
        imgBack = findViewById(R.id.imgbacks)
        announcementNumberEd = findViewById(R.id.announcementNumber)
        edPositionTitleEd = findViewById(R.id.edPositionTitle)
        amountToPay = intent.getStringExtra("amount").toString()

        val gsonn = Gson()
        val jsonn: String = utilities.getString(this@Activity3B4, "homedata").toString()

        obj = gsonn.fromJson(jsonn, HomeResponse::class.java)
        yes = utilities.getString(this@Activity3B4, "yes")

    }

    private fun onClicksI() {
        submit.setOnClickListener {
            checkValidation()
        }
        imgBack.setOnClickListener {
            onBackPressed()
        }


    }

    private fun checkValidation() {
        announcementNumber = announcementNumberEd.text.toString().trim()
        closingDate = closingDates.text.toString()
        edPositionTitle = edPositionTitleEd.text.toString().trim()
        if (announcementNumber.equals("")) {
            // Toast.makeText(this@Activity3B4,"Enter Announcement Number",Toast.LENGTH_SHORT).show()
            utilities.customToastFailure("Enter Announcement Number", this@Activity3B4)

        } else if (closingDate.equals("")) {
            // Toast.makeText(this@Activity3B4,"Enter Closing Date",Toast.LENGTH_SHORT).show()
            utilities.customToastFailure("Enter Closing Date", this@Activity3B4)


        } else if (edPositionTitle.equals("")) {
            //Toast.makeText(this@Activity3B4,"Enter Position Title",Toast.LENGTH_SHORT).show()
            utilities.customToastFailure("Enter Position Title", this@Activity3B4)


        } else {

            /*val intent = Intent(this, CardListActivity::class.java)
            intent.putExtra("amount",amountToPay)
            intent.putExtra("from","anouncments")
            startActivity(intent)
            finish()*/
            callQualificationApi()
        }
    }

    private fun callQualificationApi() {
        val gsonn = Gson()
        val jsonn: String = utilities.getString(this@Activity3B4, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)
        utilities.showProgressDialog(this@Activity3B4, "Please wait...")

        apiClient.getApiService().quickApplication(
            user_idd,
            "quick_qualification_consultation",
            "48", "hours",
            todayDate + " " + startTime,
            todayDate + " " + endTime,
            amountToPay, "$", announcementNumber,
            closingDate, edPositionTitle
        )
            .enqueue(object : Callback<BaseResponse> {

                override fun onResponse(
                    call: Call<BaseResponse>, response: Response<BaseResponse>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()
                    if (signupResponse!!.status == true) {
                        //Toast.makeText(this@Activity3B4,response.message(),Toast.LENGTH_SHORT).show()
                       // utilities.customToastSuccess(response.message(), this@Activity3B4)

                        getHomeData()
                        utilities.hideProgressDialog()


                    } else {

                        /*Toast.makeText(
                            this@Activity3B4,
                            "" + signupResponse.message,
                            Toast.LENGTH_SHORT).show()*/
                        utilities.hideProgressDialog()
                        utilities.customToastFailure(signupResponse.message, this@Activity3B4)


                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    utilities.hideProgressDialog()
                    // Toast.makeText(this@Activity3B4, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(), this@Activity3B4)

                }


            })


    }

    private fun submit() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.popup_3b5)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {

            getHomeData()
        }

        dialog.show()
    }

    private fun getHomeData() {

        val gsonn = Gson()
        val jsonn: String = utilities.getString(this@Activity3B4, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        val url = Constants.BASE_URL + "home_screen_data/" + user_idd
        apiClient.getApiService().getHomeData(url)
            .enqueue(object : Callback<HomeResponse> {

                override fun onResponse(
                    call: Call<HomeResponse>, response: Response<HomeResponse>
                ) {
                    var signupResponse = response.body()
                    val data = response.body()?.homedata
                    if (signupResponse!!.status == true) {
                        signupResponse = response.body()!!
                        val gson = Gson()
                        val json = gson.toJson(signupResponse)
                        utilities.saveString(this@Activity3B4, "homedata", json)
                        submit2()


                    } else {

                        /*Toast.makeText(
                            this@Activity3B4,
                            "" + signupResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()*/
                        utilities.customToastFailure(signupResponse.message, this@Activity3B4)


                    }
                }

                override fun onFailure(call: Call<HomeResponse>, t: Throwable) {

                    // Toast.makeText(this@Activity3B4, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(), this@Activity3B4)

                }


            })

    }

    private fun datePicker() {
        closingDates.setOnClickListener {
            val c = Calendar.getInstance()
            val mYear = c[Calendar.YEAR] // current year
            val mMonth = c[Calendar.MONTH] // current month
            val mDay = c[Calendar.DAY_OF_MONTH] // current day
            datePickerDialog = DatePickerDialog(
                this@Activity3B4,
                { view, year, monthOfYear, dayOfMonth -> // set day of month , month and year value in the edit text
                    closingDate =
                        closingDates.setText(year.toString() + "-" + (monthOfYear + 1) + "-" + dayOfMonth.toString())
                            .toString()
                    //   Toast.makeText(this@Activity3B4, closingDate, Toast.LENGTH_SHORT).show()
                }, mYear, mMonth, mDay
            )

            datePickerDialog.show()

        }
    }

    companion object {

        var todayDate = ""
        var endDate = ""
        var startTime = ""
        var endTime = ""
        var announcementNumber: String = ""
        var closingDate: String = ""
        var edPositionTitle: String = ""

    }

    private fun submit2() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.popup_3b5)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {
            dialog.dismiss()
            if (yes.equals("yes")) {
                val intent = Intent(this@Activity3B4, Activity3B::class.java)
                utilities.saveString(this@Activity3B4, "announce", Activity3B4.announcementNumber)
                utilities.saveString(this@Activity3B4, "date", Activity3B4.closingDate)
                utilities.saveString(this@Activity3B4, "pos", Activity3B4.edPositionTitle)
                utilities.saveString(this@Activity3B4, "pre", "pre")
                startActivity(intent)
                finish()
            } else {
                val intent = Intent(this@Activity3B4, Activity3B::class.java)
                utilities.saveString(
                    this@Activity3B4,
                    "nohere",
                    "You have successfully subscribed to quick qualification"
                )
                utilities.saveString(this@Activity3B4, "pre", "preno")
                startActivity(intent)
                finish()
            }
            /*}else{
                val intent = Intent(this@CardListActivity,MainActivity::class.java)
                startActivity(intent)
                finish()
            }*/

        }

        dialog.show()
    }

}