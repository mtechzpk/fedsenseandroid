package com.fed.fedsense.Activities

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.text.Editable
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.fed.fedsense.util.Utilities
import com.bumptech.glide.Glide
import com.fed.fedsense.Models.EditProfile.EditProfileReponse
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.R
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import java.io.File
import android.widget.AdapterView
import android.widget.Spinner
import com.fed.fedsense.Models.scheduling.AdminSchedulesResponseModel
import com.fed.fedsense.Models.stateandcity.StateAndCityResponseModel
import com.google.android.material.card.MaterialCardView
import com.google.android.material.imageview.ShapeableImageView
import retrofit2.Callback
import java.util.ArrayList
import android.widget.RadioButton
import com.hbb20.CountryCodePicker
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.FileOutputStream
import kotlin.concurrent.thread


class ProfileActivity : AppCompatActivity() {

    lateinit var save: CardView
    lateinit var interestSpinner: Spinner
    lateinit var stateSpinner: Spinner
    lateinit var citySpinner: Spinner
    lateinit var profileImage: ShapeableImageView
    lateinit var etName: EditText
    lateinit var r_group_resume: RadioGroup
    lateinit var r_group_citizen: RadioGroup
    lateinit var rb_resume_yes: RadioButton
    lateinit var rb_resume_no: RadioButton
    lateinit var rb_citizen_yes: RadioButton
    lateinit var rb_citizen_no: RadioButton
    lateinit var uri1: Uri
    lateinit var icBack: RelativeLayout
    var imagefile: File? = null
    var fileto_uploadd: File? = null
    lateinit var selected_img_bitmap: Bitmap
    private var imageUploaded: Boolean = false
    private var fileUploaded: Boolean = false
    private val PICK_FILE = 101
    private val PICK_IMAGE = 101
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    private lateinit var user: User
    private lateinit var tvUploadResume : TextView
    private lateinit var tvUsCitizen : TextView

    var user_idd: String = ""
    var upload_resume = "Yes"
    var citizen = "Yes"
    var checking = ""
    var str_name: String = ""
    var newPos = ""
    var interest = ""
    var state = ""
    var city = ""
    var name = ""
    var idState: String = ""
    var pos: Int = 0
    var idCity: String = ""
    lateinit var imageFileToUpload: MultipartBody.Part
    lateinit var pdfFileToUpload: MultipartBody.Part
    var list: ArrayList<StateAndCityResponseModel> = ArrayList()
    var firstTimeLogin = ""
    lateinit var lnHide : LinearLayout
    lateinit var tvTitle : TextView
    var stateName = ""
    var phoneCode = ""
    var phoneNumber = ""
    lateinit var codePicker : CountryCodePicker
    lateinit var number : EditText
    var cod= ""
    var num = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        initt()
        clicks()
        radiofunction()
        spinnersfunction()
        getStateAndCities()


        val gradientDrawable = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM,
            intArrayOf(
                Color.parseColor("#ADFF2F"),
                Color.parseColor("#008000")
            )

        )
        gradientDrawable.cornerRadius = 8f;
        save.setBackground(gradientDrawable)

    }

    private fun spinnersfunction() {

        interestSpinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                interest =
                    interestSpinner.getItemAtPosition(interestSpinner.getSelectedItemPosition())
                        .toString()
                //utilities.saveString(this@ProfileActivity,"positioninterest",interest)
                checking = "yo"

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                //nothing
            }

        })


    }

    //private method of your class
    private fun getIndex(spinner: Spinner, myString: String): Int {
        for (i in 0 until spinner.count) {
            if (spinner.getItemAtPosition(i).toString().equals(myString, ignoreCase = true)) {
                return i
            }
        }
        return 0
    }

    private fun radiofunction() {

        r_group_resume.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rb_upload_yes -> {

                    upload_resume = "Yes"
                    resume()

                }
                R.id.rb_upload_no -> {

                    upload_resume = "No"
                }
            }
        })


        r_group_citizen.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rb_citizen_yes -> {

                    citizen = "Yes"
                }
                R.id.rb_citizen_no -> {

                    citizen = "No"

                }
            }
        })
    }

    private fun clicks() {
        tvUploadResume.setOnClickListener {
            dialogwanttouploadresume()
        }
        tvUsCitizen.setOnClickListener {
            dialoguscitizen()
        }
        profileImage.setOnClickListener {

            Dexter.withActivity(this@ProfileActivity)
                .withPermissions(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
                )
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {

                            ImagePicker.with(this@ProfileActivity)
                                .crop()
                                .compress(1024)
                                .maxResultSize(1080, 1080)
                                .start()

                        }
                        if (report.isAnyPermissionPermanentlyDenied) {
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: List<PermissionRequest>,
                        token: PermissionToken
                    ) {
                        token.continuePermissionRequest()
                    }
                }).check()

        }


        save.setOnClickListener {
            firstTimeLogin = utilities.getString(this@ProfileActivity,"firstTime")
            phoneCode = codePicker.selectedCountryCode.toString()
            phoneNumber = number.text.toString()

            if (firstTimeLogin.equals("firstTime"))
            {
                dialogwanttouploadresume()
            }else{
                if (imageUploaded || fileUploaded) {
                    uplaodflow()
                } else {
                    withoutUploadFlow()
                }
            }

        }
        icBack.setOnClickListener { onBackPressed() }

    }

    private fun resume() {

        val dialog = Dialog(this)
        dialog.setContentView(R.layout.resume_popup_yes)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val upload = dialog.findViewById<MaterialCardView>(R.id.tvUplaod)
        close.setOnClickListener {

            dialog.dismiss()
            if (firstTimeLogin.equals("firstTime"))
            {
                dialoguscitizen()
            }
        }
        upload.setOnClickListener {

            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            //  intent.type = "application/pdf"
            intent.type = "application/*"
            startActivityForResult(intent, PICK_FILE)
            dialog.dismiss()

            Handler().postDelayed(
                {
                    if (firstTimeLogin.equals("firstTime"))
                    {
                        dialoguscitizen()
                    }
                }, 1500)
        }
        dialog.show()
    }

    private fun withoutUploadFlow() {


        var str_name: String = etName.text.toString()
        if (str_name.isEmpty()) {

            //Toast.makeText(this, "Name is empty", Toast.LENGTH_SHORT).show()
            utilities.customToastFailure("Name is empty", this@ProfileActivity)


        } else {
            updateProfilewithOutImage(
                user_idd,
                str_name,
                "",
                state,
                city,
                upload_resume,
                citizen
            )
        }


    }

    private fun updateProfilewithOutImage(
        userIdd: String,
        strName: String,
        interest: String,
        state: String,
        city1: String,
        uploadResume: String,
        citizen: String
    ) {

        utilities.showProgressDialog(this, "Uploading ...")
        if (city.equals(""))
        {
            city = "1"
        }
        apiClient.getApiService().eidtprofileWithoutImage(
            userIdd,
            strName,
            state,
            city,
            "phoneCode",
            "phoneNumber",
            upload_resume,
            citizen
        )
            .enqueue(object : retrofit2.Callback<EditProfileReponse> {
                override fun onResponse(
                    call: Call<EditProfileReponse>,
                    response: Response<EditProfileReponse>
                ) {

                    if (response.isSuccessful) {

                        utilities.hideProgressDialog()
                        val status: Boolean = response.body()!!.status
                        if (status.equals(true)) {

                            val message: String = response.body()!!.message
                            Toast.makeText(this@ProfileActivity, message, Toast.LENGTH_SHORT).show()
                            utilities.customToastSuccess(message, this@ProfileActivity)

                            user = response.body()!!.user
                            val gson = Gson()
                            val json = gson.toJson(user)
                            utilities.saveString(this@ProfileActivity, "user", json)

                            //submitVideo()
                            startActivity(Intent(this@ProfileActivity, MainActivity::class.java))

                            finish()
                        } else {
                            val message: String = response.body()!!.message
                            // Toast.makeText(this@ProfileActivity, message, Toast.LENGTH_SHORT).show()
                            utilities.customToastFailure(message, this@ProfileActivity)

                        }
                    } else {

                        utilities.hideProgressDialog()
                        val message: String = response.body()!!.message
                        //  Toast.makeText(this@ProfileActivity, message, Toast.LENGTH_SHORT).show()
                        utilities.customToastFailure(message, this@ProfileActivity)

                    }
                }

                override fun onFailure(call: Call<EditProfileReponse>, t: Throwable) {
                    utilities.hideProgressDialog()
                    /*Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                        .show()*/
                    utilities.customToastFailure(t.message.toString(), this@ProfileActivity)

                }
            })


    }

    private fun uplaodflow() {


        str_name = etName.text.toString()
        if (str_name.isEmpty()) {

//            Toast.makeText(this, "Name is empty", Toast.LENGTH_SHORT).show()
            utilities.customToastFailure("Name is empty", this@ProfileActivity)


        } else {
            updateProfilewithImage()

        }
    }

    private fun updateProfilewithImage(

    ) {

        utilities.showProgressDialog(this, "Uploading ...")

        val user_id: RequestBody = RequestBody.create(MediaType.parse("text/plain"), user_idd)
        val name_to_send: RequestBody = RequestBody.create(MediaType.parse("text/plain"), str_name)
        val state_to_send: RequestBody = RequestBody.create(MediaType.parse("text/plain"), state)
        val city_to_send: RequestBody = RequestBody.create(MediaType.parse("text/plain"), city)
        val phoneCod: RequestBody = RequestBody.create(MediaType.parse("text/plain"), "phoneCode")
        val phoneNumbe: RequestBody = RequestBody.create(MediaType.parse("text/plain"), "phoneNumber")
        val resume_to_send: RequestBody =
            RequestBody.create(MediaType.parse("text/plain"), upload_resume)
        val citizen_to_send: RequestBody =
            RequestBody.create(MediaType.parse("text/plain"), citizen)
        if (imagefile == null) {
            val attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "")
            imageFileToUpload = MultipartBody.Part.createFormData("image", "", attachmentEmpty)
            val requestBody: RequestBody =
                RequestBody.create(MediaType.parse("*/*"), fileto_uploadd!!)
            pdfFileToUpload =
                MultipartBody.Part.createFormData("resume", fileto_uploadd?.name, requestBody)
        } else if (fileto_uploadd == null) {
            val attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "")
            pdfFileToUpload = MultipartBody.Part.createFormData("resume", "", attachmentEmpty)
            val requestBody: RequestBody = RequestBody.create(MediaType.parse("*/*"), imagefile!!)
            imageFileToUpload =
                MultipartBody.Part.createFormData("image", imagefile!!.name, requestBody)

        } else {
            val requestBody: RequestBody =
                RequestBody.create(MediaType.parse("*/*"), fileto_uploadd!!)
            pdfFileToUpload =
                MultipartBody.Part.createFormData("resume", fileto_uploadd!!.name, requestBody)
            val requestBody1: RequestBody = RequestBody.create(MediaType.parse("*/*"), imagefile!!)
            imageFileToUpload =
                MultipartBody.Part.createFormData("image", imagefile!!.name, requestBody1)
        }



        apiClient.getApiService().eidtprofileWithImage(
            user_id,
            name_to_send,
            state_to_send,
            city_to_send,
            resume_to_send,
            citizen_to_send,
            phoneCod,
            phoneNumbe,
            imageFileToUpload,
            pdfFileToUpload
        )
            .enqueue(object : retrofit2.Callback<EditProfileReponse> {
                override fun onResponse(
                    call: Call<EditProfileReponse>,
                    response: Response<EditProfileReponse>
                ) {

                    if (response.isSuccessful) {

                        utilities.hideProgressDialog()
                        val status: Boolean = response.body()!!.status
                        if (status.equals(true)) {

                            val message: String = response.body()!!.message
                            // Toast.makeText(this@ProfileActivity, message, Toast.LENGTH_SHORT).show()
                            utilities.customToastSuccess(message, this@ProfileActivity)

                            user = response.body()!!.user
                            val gson = Gson()
                            val json = gson.toJson(user)
                            utilities.saveString(this@ProfileActivity, "user", json)
                            //submitVideo()
                            startActivity(Intent(this@ProfileActivity, MainActivity::class.java))
                            finish()

                        } else {
                            val message: String = response.body()!!.message
                            // Toast.makeText(this@ProfileActivity, message, Toast.LENGTH_SHORT).show()
                            utilities.customToastFailure(message, this@ProfileActivity)

                        }
                    } else {

                        utilities.hideProgressDialog()
                        val message: String = response.body()!!.message
                        //  Toast.makeText(this@ProfileActivity, message, Toast.LENGTH_SHORT).show()
                        utilities.customToastFailure(message, this@ProfileActivity)

                    }
                }

                override fun onFailure(call: Call<EditProfileReponse>, t: Throwable) {
                    utilities.hideProgressDialog()
                    /* Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                         .show()*/
                    utilities.customToastFailure(t.message.toString(), this@ProfileActivity)

                }
            })


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            PICK_FILE -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val uri = data.data
                    if (uri != null) {
                        val fileName = getFileNameByUri(uri)
                        copyUriToExternalFilesDir(uri, fileName)
                    }
                }
            }
            else -> {
                if (resultCode == Activity.RESULT_OK) {
                    uri1 = data?.data!!
                    selected_img_bitmap =
                        MediaStore.Images.Media.getBitmap(this.contentResolver, uri1)
                    profileImage.setImageBitmap(selected_img_bitmap)
                    //You can get File object from intent
                    val file: File = ImagePicker.getFile(data)!!
                    imagefile = file
                    imageUploaded = true

                } else if (resultCode == ImagePicker.RESULT_ERROR) {
                    Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
                }
            }
        }

    }

    private fun getFileNameByUri(uri: Uri): String {
        var fileName = System.currentTimeMillis().toString()
        val cursor = contentResolver.query(uri, null, null, null, null)
        if (cursor != null && cursor.count > 0) {
            cursor.moveToFirst()
            fileName =
                cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME))
            cursor.close()
        }
        return fileName
    }

    private fun copyUriToExternalFilesDir(uri: Uri, fileName: String) {
        thread {
            val inputStream = contentResolver.openInputStream(uri)
            val tempDir = getExternalFilesDir("temp")
            if (inputStream != null && tempDir != null) {
                fileto_uploadd = File("$tempDir/$fileName")
                val fos = FileOutputStream(fileto_uploadd)
                fileUploaded = true
                val bis = BufferedInputStream(inputStream)
                val bos = BufferedOutputStream(fos)
                val byteArray = ByteArray(1024)
                var bytes = bis.read(byteArray)
                while (bytes > 0) {
                    bos.write(byteArray, 0, bytes)
                    bos.flush()
                    bytes = bis.read(byteArray)
                }
                bos.close()
                fos.close()
                Log.d("address", fileto_uploadd.toString())

            }
        }
    }

    private fun initt() {

        save = findViewById(R.id.save)
        interestSpinner = findViewById(R.id.spinneer)
        stateSpinner = findViewById(R.id.stateSpinner)
        citySpinner = findViewById(R.id.citySpinner)
        profileImage = findViewById(R.id.profile_image)
        etName = findViewById(R.id.etName)
        r_group_resume = findViewById(R.id.resumeradiogroup)
        r_group_citizen = findViewById(R.id.citizenradiogroup)
        rb_resume_yes = findViewById(R.id.rb_upload_yes)
        rb_resume_no = findViewById(R.id.rb_upload_no)
        rb_citizen_yes = findViewById(R.id.rb_citizen_yes)
        rb_citizen_no = findViewById(R.id.rb_citizen_no)
        icBack = findViewById(R.id.icBack)
        tvUploadResume = findViewById(R.id.tvUploadResume)
        tvUsCitizen = findViewById(R.id.tvUsCitizen)
        lnHide = findViewById(R.id.lnHide)
        tvTitle = findViewById(R.id.tvTitle)
        number = findViewById(R.id.edPhoneNumber)
        apiClient = ApiClient()
        utilities = Utilities(this)
        codePicker = findViewById<CountryCodePicker>(R.id.countryCodePicker)
        if (!::utilities.isInitialized) utilities = Utilities(this)
        firstTimeLogin = utilities.getString(this@ProfileActivity,"firstTime")
        if (firstTimeLogin.equals("firstTime"))
        {
            lnHide.visibility = View.GONE
            tvTitle.text = "Next"
        }else{
            lnHide.visibility = View.VISIBLE
        }
        setprofileData()


    }

    private fun setprofileData() {

        val gsonn = Gson()
        val jsonn: String = utilities.getString(this, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        user_idd = java.lang.String.valueOf(obj.id)
        cod= obj.country_code.toString()
        num = obj.phone.toString()

        val image = obj.profile_image
        val name = obj.name
        val interest = obj.position_of_interest
        val state = obj.state
        val city = obj.city
        val citizenn = obj.are_you_in_US
        val resume = obj.like_to_upload
        val resume1 = obj.like_to_upload

        if (!image!!.isEmpty()) {
            Glide.with(this)
                .load(Constants.BASE_URL_IMG + image)
                .into(profileImage)
        }

        if (!name!!.isEmpty()) {
            etName.setText(name)
        }
        if (!interest!!.isEmpty()) {
            interestSpinner.setSelection(getIndex(interestSpinner, interest))
        }
        if (!state!!.isEmpty()) {

        }
        if (!city!!.isEmpty()) {
            //   citySpinner.setText(city)
        }
        if (!num.isEmpty())
        {
            number.text = Editable.Factory.getInstance().newEditable(num)
        }
        if (!resume!!.isEmpty()) {

            if (resume.equals("Yes")) {
                rb_resume_yes.isChecked = true
                rb_resume_no.isChecked = false
                upload_resume = resume
                tvUploadResume.text = upload_resume
            } else {
                rb_resume_yes.isChecked = false
                rb_resume_no.isChecked = true
                upload_resume = resume
                tvUploadResume.text = upload_resume

            }

        }
        if (!citizenn!!.isEmpty()) {

            if (citizenn.equals("Yes")) {

                rb_citizen_yes.isChecked = true
                rb_citizen_no.isChecked = false
                citizen = "Yes"
                tvUsCitizen.text = citizen
            } else {
                rb_citizen_yes.isChecked = false
                rb_citizen_no.isChecked = true
                citizen = "No"
                tvUsCitizen.text = citizen

            }
        }
    }

    private fun getStateAndCities() {
        utilities.showProgressDialog(this@ProfileActivity, "Loading ...")
        val gsonn = Gson()
        val jsonn: String = utilities.getString(this, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        state = obj.state.toString()
        city = obj.city.toString()
        apiClient.getApiService().stateAndCities()
            .enqueue(object : Callback<StateAndCityResponseModel> {

                override fun onResponse(
                    call: Call<StateAndCityResponseModel>,
                    response: Response<StateAndCityResponseModel>
                ) {

                    val adminSchedule = response.body()
                    utilities.hideProgressDialog()
                    if (adminSchedule!!.status == true) {

                        // Toast.makeText(this@SchedulingActivity,adminSchedule.message,Toast.LENGTH_SHORT).show()
                        // utilities.customToastSuccess(adminSchedule.message,this@SchedulingActivity)

                        val item = ArrayList<String>()
                        for (i in 0..adminSchedule.data.size - 1) {
                            item.add(adminSchedule.data.get(i).state)

                        }
                        if (item != null) {
                            val adapter = ArrayAdapter(
                                this@ProfileActivity,
                                R.layout.spinner_text,
                                item
                            )

                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                            stateSpinner.setAdapter(adapter)
                            if (!state.isEmpty()) {
                                for (i in 0 until adminSchedule.data.size) {
                                    if (adminSchedule.data.get(i).state.contains(state.trim())) {
                                        stateSpinner.setSelection(getIndex(stateSpinner, state))

                                    }
                                }
                            }


                        } else {
                            utilities.hideProgressDialog()
                            /*Toast.makeText(
                                this@SchedulingActivity,
                                "No Date Are Available",
                                Toast.LENGTH_SHORT
                            ).show()*/
                            utilities.customToastFailure(
                                "No Date Are Available",
                                this@ProfileActivity
                            )


                        }
                        stateSpinner.setOnItemSelectedListener(object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View,
                                position: Int,
                                id: Long
                            ) {
                                val subCat =
                                    adminSchedule.data.get(position).cities
                                idState = adminSchedule.data.get(position).id
                                stateName =
                                    stateSpinner.getItemAtPosition(stateSpinner.getSelectedItemPosition())
                                        .toString()
                                utilities.saveString(this@ProfileActivity,"stateName",stateName)
                                val itemSub = ArrayList<String>()
                                for (i in 0..subCat.size - 1) {
                                    itemSub.add(subCat.get(i).city)
                                }
                                if (itemSub != null) {
                                    val adapter = ArrayAdapter(
                                        this@ProfileActivity,
                                        R.layout.spinner_text,
                                        itemSub
                                    )
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                                    citySpinner.setAdapter(adapter)
                                    pos = position
                                    val posd = adminSchedule.data.get(position).cities.size
                                    if (!city.isEmpty()) {
                                        for (i in 0 until posd) {
                                            if (adminSchedule.data.get(position).cities.get(i).city.contains(
                                                    city.trim()
                                                )
                                            ) {
                                                citySpinner.setSelection(
                                                    getIndex(
                                                        citySpinner,
                                                        city
                                                    )
                                                )
                                            }
                                        }
                                    }
                                    state = adminSchedule.data.get(position).state
                                    if (adminSchedule.data.get(position).cities.isEmpty())
                                    {
                                        city = ""
                                    }
                                }

                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        })
                        citySpinner.setOnItemSelectedListener(object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View,
                                position: Int,
                                id: Long
                            ) {
                                idCity =
                                    adminSchedule.data.get(pos).cities.get(position).id
                                city = adminSchedule.data.get(pos).cities.get(position).city

                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        })

                    } else {

                        /* Toast.makeText(
                             this@SchedulingActivity,
                             "" + adminSchedule.message,
                             Toast.LENGTH_SHORT
                         ).show()*/
                        utilities.customToastFailure(adminSchedule.message, this@ProfileActivity)

                    }
                }

                override fun onFailure(call: Call<StateAndCityResponseModel>, t: Throwable) {

                    utilities.hideProgressDialog()
                    //Toast.makeText(this@SchedulingActivity, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(), this@ProfileActivity)

                }


            })

    }

    private fun dialogwanttouploadresume() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_want_to_upload_resume)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvYes)
        val no = dialog.findViewById<MaterialCardView>(R.id.tvNo)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {
            upload_resume = "Yes"
            resume()
            dialog.dismiss()
            tvUploadResume.text = "Yes"

        }
        no.setOnClickListener {
            upload_resume = "No"
            dialog.dismiss()
            tvUploadResume.text = "No"
            if (firstTimeLogin.equals("firstTime"))
            {
                dialoguscitizen()
            }
        }

        dialog.show()
    }
    private fun dialoguscitizen() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_us_citizen)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvYes)
        val no = dialog.findViewById<MaterialCardView>(R.id.tvNo)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {
            dialog.dismiss()
            citizen = "Yes"
            tvUsCitizen.text = "Yes"
            tvTitle.text = "Save"
            utilities.saveString(this@ProfileActivity,"firstTime","")

        }
        no.setOnClickListener {
            dialog.dismiss()
            citizen = "No"
            tvUsCitizen.text = "No"
            tvTitle.text = "Save"
            utilities.saveString(this@ProfileActivity,"firstTime","")

        }

        dialog.show()
    }


    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this@ProfileActivity, MainActivity::class.java))
        finish()
    }
}