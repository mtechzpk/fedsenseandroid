package com.fed.fedsense.Activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.Image
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.fed.fedsense.Adapters.CardListAdapter
import com.fed.fedsense.Models.BaseResponse
import com.fed.fedsense.Models.Home.HomeData
import com.fed.fedsense.Models.Home.HomeResponse
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.Models.payment.Card
import com.fed.fedsense.Models.payment.CardResponse
import com.fed.fedsense.R
import com.fed.fedsense.RoomDB.CraditCard
import com.fed.fedsense.RoomDB.MyAppDataBase
import com.fed.fedsense.util.Utilities
import com.google.android.material.card.MaterialCardView
import com.google.gson.Gson
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import kotlinx.android.synthetic.main.activity_card_list.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.*

class CardListActivity : AppCompatActivity(), CardListAdapter.SearchItemClickListener  {

     lateinit var rvCards: RecyclerView
     var list: ArrayList<Card> = ArrayList()
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    var amountToPay = ""
    var from = ""
    var homedata: ArrayList<HomeData> = ArrayList()
    var yes = ""
    var videoId = ""
    var filePath: File? = null
    lateinit var imgBack : RelativeLayout
    private lateinit var myAppDatabase: MyAppDataBase
    var craditCards: List<CraditCard> = ArrayList()

    var user_id = ""



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_list)


        myAppDatabase = Room.databaseBuilder(this, MyAppDataBase::class.java, "FEDENSEDB").allowMainThreadQueries().build()
        apiClient = ApiClient()
        utilities = Utilities(this)
        if (!::utilities.isInitialized) utilities = Utilities(this)
        apiClient = ApiClient()
        val gsonn = Gson()
        val jsonn: String = utilities.getString(this, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        val homeData : String= utilities.getString(this,"homedata")
        user_id = java.lang.String.valueOf(obj.id)
        amountToPay = intent.getStringExtra("amount").toString()
        from = intent.getStringExtra("from").toString()
        videoId = intent.getStringExtra("videoid").toString()
        imgBack = findViewById(R.id.icBack)
        val textcard  = findViewById<TextView>(R.id.tvCardList)
        if (from.equals("resumeRevamp"))
        {
            filePath = File(intent.getStringExtra("filePath"))
        }else if (from.equals("setting"))
        {
         tvCardList.text = "Select Card"
            btnpay.visibility = View.GONE
        }
        yes = utilities.getString(this@CardListActivity,"yes")
       /* Toast.makeText(this@CardListActivity,"Announcement Number : ${Activity3B4.announcementNumber} " +
                "closing Date: ${Activity3B4.closingDate} " +
                "Position Status : ${Activity3B4.edPositionTitle}",Toast.LENGTH_SHORT).show()*/

        imgBack.setOnClickListener {
            onBackPressed()
        }
        click()

    }

    override fun onResume() {
        super.onResume()
//        getcard()
        getLocalCard("normal")

    }

    private fun getLocalCard(from: String) {

        craditCards = myAppDatabase.cardDao().loadAll()
//        if (from.equals("normal")){
//
//            if (craditCards.size == 1){
//                utilities.saveInt(this,"selectedCard",craditCards[0].id)
//            }
//        }else{
//
//        }


        recycler.addItemDecoration(androidx.recyclerview.widget.DividerItemDecoration(this@CardListActivity, androidx.recyclerview.widget.DividerItemDecoration.VERTICAL))
        recycler.setLayoutManager(LinearLayoutManager(this@CardListActivity, LinearLayoutManager.VERTICAL, false))
        val adapter = CardListAdapter(this@CardListActivity, craditCards,this)
        recycler.setAdapter(adapter)



    }

    private fun click() {

        addCard.setOnClickListener {
            startActivity(Intent(this@CardListActivity,AddCardActivity::class.java))
        }

        btnpay.setOnClickListener {

            if (Constants.card_selected){

                Toast.makeText(this, "Paying", Toast.LENGTH_SHORT).show()
                payAmount()

            }else{

                Toast.makeText(this, "Please select a card", Toast.LENGTH_SHORT).show()

            }
        }
    }

    private fun payAmount() {


        if (utilities.isConnectingToInternet(this)) {
            val card = Constants.card
            var expiry = card.expiryData
            val separated = expiry.split("/").toTypedArray()
            val month = separated[0]
            val year = separated[1]
//            val gsonn = Gson()
//            val jsonn: String = utilities.getString(this, "user")
//            val obj: User = gsonn.fromJson(jsonn, User::class.java)
//            var user_idd: String = java.lang.String.valueOf(obj.id)
            utilities.showProgressDialog(this, "Processing ...")
            apiClient.getApiService().charge_payment(
                user_id,
                card.cardNumber,
                month,
                year,
                card.cvv,
                amountToPay
            )
                .enqueue(object : Callback<BaseResponse> {

                    override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                        val signupResponse = response.body()
                        utilities.hideProgressDialog()
                        if (signupResponse!!.status == true) {

                            if (from.equals("anouncments")){
                                if (yes.equals("yes"))
                                {
                                    //callQualificationApi()
                                    startActivity(Intent(this@CardListActivity,Activity3B4::class.java))
                                    finish()
                                }else{
                                    callQualificationApi2()
                                }
                            }else if (from.equals("paidvideo"))
                            {
                                callPaidVideoApi()
                            }else if (from.equals("videolesson"))
                            {
                                callAttendLessionApi()
                            }else if (from.equals("resumeRevamp"))
                            {
                                resumeRevampApplicationApi()
                            }else if (from.equals("resumeno"))
                            {
                                resumeRevampApplicationApiNo()
                            }else{
                                enrollApplicationApi()
                            }
                        } else {
                            utilities.hideProgressDialog()
                            utilities.customToastFailure(signupResponse.message, this@CardListActivity)

                        }
                    }
                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        utilities.hideProgressDialog()
                        utilities.customToastFailure(t.toString(), this@CardListActivity)
                    }
                })
        } else {
            Toast.makeText(
                this,
                "Check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        }



    }


    private fun callQualificationApi() {
        val gsonn = Gson()
        val jsonn: String = utilities.getString(this@CardListActivity, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        apiClient.getApiService().quickApplication(user_idd,
            "quick_qualification_consultation",
            "48","hours",
            Activity3B4.todayDate+" "+Activity3B4.startTime,
            Activity3B4.todayDate+" "+Activity3B4.endTime,
            "20","$",Activity3B4.announcementNumber,
            Activity3B4.closingDate,Activity3B4.edPositionTitle)
            .enqueue(object : Callback<BaseResponse> {

                override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()
                    if (signupResponse!!.status == true) {
                        //Toast.makeText(this@Activity3B4,response.message(),Toast.LENGTH_SHORT).show()
                        utilities.customToastSuccess(response.message(),this@CardListActivity)
                        getHomeData()
                    } else {
                        utilities.customToastFailure(signupResponse.message,this@CardListActivity)
                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                    // Toast.makeText(this@Activity3B4, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(),this@CardListActivity)

                }


            })



    }
    private fun callAttendLessionApi() {
        val gsonn = Gson()
        val jsonn: String = utilities.getString(this@CardListActivity, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        apiClient.getApiService().attendVideoLesson(user_idd,
            "video_session","45","$")
            .enqueue(object : Callback<BaseResponse> {

                override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()
                    if (signupResponse!!.status == true) {
                        //Toast.makeText(this@Activity3B4,response.message(),Toast.LENGTH_SHORT).show()
                        utilities.customToastSuccess(response.message(),this@CardListActivity)
                        ok()
                    } else {
                        utilities.customToastFailure(signupResponse.message,this@CardListActivity)
                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                    // Toast.makeText(this@Activity3B4, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(),this@CardListActivity)

                }


            })



    }
    private fun callQualificationApi2() {
        val gsonn = Gson()
        val jsonn: String = utilities.getString(this@CardListActivity, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        apiClient.getApiService().quickApplication(user_idd,
            "quick_qualification_consultation",
            "48","hours",
            "2022-10-26 11:00:00",
            "2022-11-29 11:00:00",
            "20","$","No Annoucement",
                "12/05/2022","No Title")
            .enqueue(object : Callback<BaseResponse> {

                override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()
                    if (signupResponse!!.status == true) {
                        //Toast.makeText(this@Activity3B4,response.message(),Toast.LENGTH_SHORT).show()
                        utilities.customToastSuccess(response.message(),this@CardListActivity)
                        getHomeData()
                    } else {
                        utilities.customToastFailure(signupResponse.message,this@CardListActivity)
                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                    // Toast.makeText(this@Activity3B4, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(),this@CardListActivity)

                }


            })



    }
    private fun callPaidVideoApi() {
        val gsonn = Gson()
        val jsonn: String = utilities.getString(this@CardListActivity, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        apiClient.getApiService().paidVideoLesson(user_idd,
            "video_lesson",videoId,amountToPay,"$")
            .enqueue(object : Callback<BaseResponse> {

                override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()
                    if (signupResponse!!.status == true) {
                        //Toast.makeText(this@Activity3B4,response.message(),Toast.LENGTH_SHORT).show()
                        utilities.customToastSuccess(response.message(),this@CardListActivity)
                        getHomeData()
                    } else {
                        utilities.customToastFailure(signupResponse.message,this@CardListActivity)
                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                    // Toast.makeText(this@Activity3B4, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(),this@CardListActivity)

                }


            })



    }

    private fun submit() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.popup_3b5)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {
            dialog.dismiss()
            if (yes.equals("yes"))
            {
                val intent = Intent(this@CardListActivity,Activity3B::class.java)
                utilities.saveString(this@CardListActivity,"announce",Activity3B4.announcementNumber)
                utilities.saveString(this@CardListActivity,"date",Activity3B4.closingDate)
                utilities.saveString(this@CardListActivity,"pos",Activity3B4.edPositionTitle)
                utilities.saveString(this@CardListActivity,"pre","pre")
                startActivity(  intent)
                finish()
            }else {
                val intent = Intent(this@CardListActivity, Activity3B::class.java)
                utilities.saveString(
                    this@CardListActivity,
                    "nohere",
                    "You have successfully subscribed to quick qualification"
                )
                utilities.saveString(this@CardListActivity, "pre", "preno")
                startActivity(intent)
                finish()
            }
            /*}else{
                val intent = Intent(this@CardListActivity,MainActivity::class.java)
                startActivity(intent)
                finish()
            }*/

        }

        dialog.show()
    }

    private fun submitVideo() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.paid_video_popup)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {
            dialog.dismiss()
                /*val intent = Intent(this@CardListActivity,MainActivity::class.java)
                startActivity(intent)*/
            if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                getSupportFragmentManager(). popBackStackImmediate("MESSAGE_TAG", 0);
                finish()

        }

        dialog.show()
    }
    private fun getcard() {

        if (utilities.isConnectingToInternet(this)) {

//            val gsonn = Gson()
//            val jsonn: String = utilities.getString(this, "user")
//            val obj: User = gsonn.fromJson(jsonn, User::class.java)
//            var user_idd: String = java.lang.String.valueOf(obj.id)
            val url = Constants.BASE_URL + "get_cards/" + "2"
            utilities.showProgressDialog(this, "Loading ...")
            apiClient.getApiService().get_cards(url)
                .enqueue(object : Callback<CardResponse> {

                    override fun onResponse(call: Call<CardResponse>, response: Response<CardResponse>) {
                        val signupResponse = response.body()
                        utilities.hideProgressDialog()
                        if (signupResponse!!.status == true) {

                            val list = response.body()!!.card

//                            recycler.addItemDecoration(androidx.recyclerview.widget.DividerItemDecoration(this@CardListActivity, androidx.recyclerview.widget.DividerItemDecoration.VERTICAL))
//                            recycler.setLayoutManager(LinearLayoutManager(this@CardListActivity, LinearLayoutManager.VERTICAL, false))
//                            val adapter = CardListAdapter(this@CardListActivity, list)
//                            recycler.setAdapter(adapter)

                        } else {
                            utilities.hideProgressDialog()
                            utilities.customToastFailure(signupResponse.message, this@CardListActivity)

                        }
                    }
                    override fun onFailure(call: Call<CardResponse>, t: Throwable) {
                        utilities.hideProgressDialog()
                        utilities.customToastFailure(t.toString(), this@CardListActivity)
                    }
                })
        } else {
            Toast.makeText(
                this,
                "Check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        }

    }

    private fun enrollApplicationApi() {

        val gsonn = Gson()
        val jsonn: String = utilities.getString(this@CardListActivity, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)


        apiClient.getApiService().enrollInFedApplication(
            user_idd,
            "enroll_in_fed_app",
            "90",
            "$",
            SchedulingActivity.dateee,
            SchedulingActivity.timeee)
            .enqueue(object : Callback<BaseResponse> {

                override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()
                    if (signupResponse!!.status == true) {
                        //utilities.customToastSuccess(signupResponse.message,this@SchedulingActivity)
                        paynow()

                    } else {

                        utilities.customToastFailure(signupResponse.message,this@CardListActivity)


                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                    // Toast.makeText(this@SchedulingActivity, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(),this@CardListActivity)

                }


            })
    }

    private fun paynow() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.popup_4c)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            dialog.dismiss()
            finish()
        }

        dialog.show()
    }

    private fun getHomeData() {


        if (utilities.isConnectingToInternet(this@CardListActivity)) {

            val gsonn = Gson()
            val jsonn: String = utilities.getString(this@CardListActivity, "user")
            val obj: User = gsonn.fromJson(jsonn, User::class.java)
            var user_idd: String = java.lang.String.valueOf(obj.id)

            val url = Constants.BASE_URL + "home_screen_data/" + user_idd
            utilities.showProgressDialog(this@CardListActivity, "Loading ...")
            apiClient.getApiService().getHomeData(url)
                .enqueue(object : Callback<HomeResponse> {

                    override fun onResponse(
                        call: Call<HomeResponse>, response: Response<HomeResponse>
                    ) {
                        val signupResponse = response.body()
                        val data = response.body()?.homedata
                        utilities.hideProgressDialog()
                        if (signupResponse!!.status == true) {

                            homedata = response.body()!!.homedata
                            val gson = Gson()
                            val json = gson.toJson(signupResponse)
                            utilities.saveString(this@CardListActivity,"homedata",json)
                            if (from.equals("paidvideo"))
                            {
                                //submitVideo()
                                if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                                    getSupportFragmentManager(). popBackStackImmediate("MESSAGE_TAG", 0);
                                finish()
                            }else{
                                submit()
                            }


                        } else {
                            utilities.hideProgressDialog()
                            utilities.customToastFailure(
                                signupResponse.message,
                                this@CardListActivity
                            )

                        }
                    }

                    override fun onFailure(call: Call<HomeResponse>, t: Throwable) {

                        utilities.hideProgressDialog()
                        utilities.customToastFailure(t.toString(), this@CardListActivity)

                    }


                })
        } else {
            Toast.makeText(
                this@CardListActivity,
                "Check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        }

    }

    private fun ok() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.popup_4c)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {

            val intent = Intent(this, Activity3B::class.java)
            utilities.saveString(this@CardListActivity,"pre","lesson")
            startActivity(intent)
            dialog.dismiss()
            finish()
        }

        dialog.show()
    }
    private fun resumeRevampApplicationApi() {

        val gsonn = Gson()

        val jsonn: String = utilities.getString(this@CardListActivity, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        apiClient.getApiService().resumeRevampApplication(user_idd,
            "resume_revamp_camp",
            "45","$")
            .enqueue(object : Callback<BaseResponse> {

                override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()
                    if (signupResponse!!.status == true) {
                      //  ook()
                        uploadResume(filePath!!)

                    } else {

                        /*Toast.makeText(
                            this@ResumeActivity,
                            "" + signupResponse.message,
                            Toast.LENGTH_SHORT).show()*/
                        utilities.customToastFailure(signupResponse.message,this@CardListActivity)

                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                    // Toast.makeText(this@ResumeActivity, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(),this@CardListActivity)

                }


            })


    }
    private fun resumeRevampApplicationApiNo() {

        val gsonn = Gson()

        val jsonn: String = utilities.getString(this@CardListActivity, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        apiClient.getApiService().resumeRevampApplication(user_idd,
            "resume_revamp_camp",
            "165","$")
            .enqueue(object : Callback<BaseResponse> {

                override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()
                    if (signupResponse!!.status == true) {
                        //  ook()
                        afteruplaodDialouge()

                    } else {

                        /*Toast.makeText(
                            this@ResumeActivity,
                            "" + signupResponse.message,
                            Toast.LENGTH_SHORT).show()*/
                        utilities.customToastFailure(signupResponse.message,this@CardListActivity)

                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                    // Toast.makeText(this@ResumeActivity, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(),this@CardListActivity)

                }


            })


    }
    private fun uploadResume(file: File) {

        utilities.showProgressDialog(this, "Uploading ...")

        val gsonn = Gson()
        val jsonn: String = utilities.getString(this, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        val user_id: RequestBody = RequestBody.create(MediaType.parse("text/plain"), user_idd)
        val service_id: RequestBody = RequestBody.create(MediaType.parse("text/plain"), "1")
        val requestBody: RequestBody = RequestBody.create(MediaType.parse("*/*"), file)
        val fileToUpload = MultipartBody.Part.createFormData("resume", file.getName(), requestBody)
        utilities.showProgressDialog(this@CardListActivity,"Uploading Resume....")
        apiClient.getApiService().upload_resume(
            user_id,
            service_id,
            fileToUpload
        )
            .enqueue(object : retrofit2.Callback<BaseResponse> {
                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {

                    if (response.isSuccessful) {

                        utilities.hideProgressDialog()
                        val status: Boolean = response.body()!!.status
                        if (status.equals(true)) {

                            val message: String = response.body()!!.message
                            // Toast.makeText(this@FedralActivity, message, Toast.LENGTH_SHORT).show()
                            utilities.customToastSuccess(message,this@CardListActivity)

                            afteruplaodDialouge()


                        } else {
                            utilities.hideProgressDialog()
                            val message: String = response.body()!!.message
                            // Toast.makeText(this@FedralActivity, message, Toast.LENGTH_SHORT).show()
                            utilities.customToastFailure(message,this@CardListActivity)

                        }
                    } else {

                        utilities.hideProgressDialog()
                        val message: String = response.body()!!.message
                        //  Toast.makeText(this@FedralActivity, message, Toast.LENGTH_SHORT).show()
                        utilities.customToastFailure(message,this@CardListActivity)

                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    utilities.hideProgressDialog()
                    /*Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                        .show()*/
                    utilities.customToastFailure(t.message.toString(),this@CardListActivity)

                }
            })

    }
    private fun afteruplaodDialouge() {

        val dialog = Dialog(this)

        dialog.setContentView(R.layout.resume_revamp_dialog)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {
            //here payment method will be integrated and than we will call the api
            // uploadResume(fileto_uploadd)
            val intent = Intent(this@CardListActivity,MainActivity::class.java)
            startActivity(intent)
            dialog.dismiss()

        }

        dialog.show()
    }

    override fun onItemDeleted() {
        getLocalCard("deletion")
    }

}