package com.fed.fedsense.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.fed.fedsense.R
import com.fed.fedsense.databinding.ActivityCreatePasswordBinding
import com.fed.fedsense.util.Utilities
import com.google.gson.Gson
import com.mtechsoft.compassapp.services.ApiClient
import com.report.app.DataModel.ResetPassword.ResetPasswordResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CreatePasswordActivity : AppCompatActivity() {
    private lateinit var bindind : ActivityCreatePasswordBinding
    var email : String? = ""
    var password : String = ""
    var confirmPassword :  String = ""
    private lateinit var utilities: Utilities
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindind = ActivityCreatePasswordBinding.inflate(layoutInflater)
        setContentView(bindind.root)

        utilities = Utilities(this@CreatePasswordActivity)
        val intent = intent
        email = intent.getStringExtra("email")

        bindind.btnContinue.setOnClickListener {
            val passwords = bindind.edPass.text.toString()
            val confirmPasswords = bindind.edConfirmPass.text.toString()
            if (passwords.equals(""))
            {
                utilities.customToastFailure("Please Enter New Password",this@CreatePasswordActivity)
            }else if (passwords.length<6)
            {
                utilities.customToastFailure("Password Length Must Be Greater Than 6",this@CreatePasswordActivity)
            }else if (confirmPasswords.equals(""))
            {
                utilities.customToastFailure("Please Confirm Password",this@CreatePasswordActivity)
            }else if (!confirmPasswords.equals(passwords))
            {
                utilities.customToastFailure("Password not matched",this@CreatePasswordActivity)
            }else{
                resetpass(email,passwords,confirmPasswords)
                hideKeybBoard()
            }


        }
    }
    private fun resetpass(email: String?, passwords: String, confirmPasswords: String) {
        val apiClient: ApiClient = ApiClient()
        if (email != null) {
            utilities.showProgressDialog(this@CreatePasswordActivity,"Please wait...")
            apiClient.getApiService().restpass(email,passwords,confirmPasswords)
                .enqueue(object : Callback<ResetPasswordResponse?> {
                    override fun onResponse(
                        call: Call<ResetPasswordResponse?>,
                        response: Response<ResetPasswordResponse?>
                    ) {
                        val resetPasswordResponse = response.body()!!
                        if (response.isSuccessful){
                            utilities.hideProgressDialog()
                            val gson = Gson()
                            val json = gson.toJson(resetPasswordResponse.user)
                            utilities.saveString(this@CreatePasswordActivity, "loginResponse", json)
                            startActivity(Intent(this@CreatePasswordActivity,LoginActivity::class.java))
                            finish()
                        }else{
                            utilities.hideProgressDialog()
                            Toast.makeText(
                                applicationContext,
                                response.body()?.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }


                    }

                    override fun onFailure(call: Call<ResetPasswordResponse?>, t: Throwable) {
                        utilities.hideProgressDialog()
                        Log.d("forgot password",t.message.toString())
                        Toast.makeText(applicationContext, "Wrong Entries", Toast.LENGTH_SHORT).show()                }
                })
        }
    }

    private fun hideKeybBoard() {
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(bindind.root.windowToken, 0)
    }
}