package com.fed.fedsense.Activities

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.widget.ImageView
import com.fed.fedsense.R
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.firebase.database.core.utilities.Utilities
import com.mtechsoft.compassapp.networking.Constants

class FullScreenVideoActivity : AppCompatActivity() {
    private var player: SimpleExoPlayer? = null
    private var playerView: PlayerView? = null
    var videolink = ""
    private lateinit var utilities: com.fed.fedsense.util.Utilities

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_SECURE,
            WindowManager.LayoutParams.FLAG_SECURE
        )
        setContentView(R.layout.activity_full_screen_video)

        val intent = Intent()
        playerView = findViewById(R.id.video_view)
        val ivBack : ImageView  = findViewById(R.id.ivBack)
        utilities = com.fed.fedsense.util.Utilities(this@FullScreenVideoActivity)
         videolink = utilities.getString(this@FullScreenVideoActivity,"videourl")
        if(!videolink.equals(""))
        {
            setVideo(videolink)
        }
        ivBack.setOnClickListener {
            onBackPressed()
        }

    }
    fun setVideo(videoLink : String) {

        val appNameStringRes = R.string.app_name
        val trackSelectorDef: TrackSelector = DefaultTrackSelector(this)
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelectorDef)
        val userAgent = Util.getUserAgent(this, this.getString(appNameStringRes))
        val defaultDataSourceFactory = DefaultDataSourceFactory(this, userAgent)
        val uriOfContentUrl = Uri.parse(videoLink)
        val mediaSource: MediaSource = ProgressiveMediaSource.Factory(defaultDataSourceFactory)
            .createMediaSource(uriOfContentUrl)
        //MediaSource mediaSource = new ExtractorMediaSource(uriOfContentUrl, new CacheDataSourceFactory(context, 100 * 1024 * 1024, 500 * 1024 * 1024), new DefaultExtractorsFactory(), null, null);
        //MediaSource mediaSource = new ExtractorMediaSource(uriOfContentUrl, new CacheDataSourceFactory(context, 100 * 1024 * 1024, 500 * 1024 * 1024), new DefaultExtractorsFactory(), null, null);
        player!!.prepare(mediaSource)
        player!!.setPlayWhenReady(false)
        playerView!!.requestFocus()
        playerView!!.player = player
        playerView!!.setShowBuffering(PlayerView.SHOW_BUFFERING_WHEN_PLAYING)
        player!!.setVolume(1f)

    }

    override fun onDestroy() {
        super.onDestroy()
        player!!.stop()
        player!!.release()
    }
}
