package com.fed.fedsense.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fed.fedsense.Models.faqmodel.FAQDataListModel
import com.fed.fedsense.Models.faqmodel.FAQResponseModel
import com.fed.fedsense.Models.faqmodel.Language
import com.fed.fedsense.Models.faqmodel.RvAdapter
import com.fed.fedsense.databinding.ActivityFaqactivityBinding
import com.fed.fedsense.util.Utilities
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FAQActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFaqactivityBinding

    // get reference to the adapter class
    private var languageList = ArrayList<Language>()
    private lateinit var rvAdapter: RvAdapter
    private var page: Int = 0
    private var limit: Int = 10
    private lateinit var utils: Utilities
    private lateinit var arrivalList: ArrayList<FAQDataListModel>
    private var emptyList: ArrayList<FAQDataListModel> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFaqactivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        utils = Utilities(this@FAQActivity)
        binding.imgBack.setOnClickListener {
            onBackPressed()
        }
        getFAQ()
        binding.idNestedSV.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            // on scroll change we are checking when users scroll as bottom.
            if (scrollY == v.getChildAt(0).measuredHeight - v.measuredHeight) {
                // in this method we are incrementing page number,
                // making progress bar visible and calling get data method.
                page++
                binding.idPBLoading.setVisibility(View.VISIBLE)
                getFAQ()

            }
        })

        utils.setWhiteBars(this@FAQActivity)

    }

    private fun getFAQ() {
        if (page > limit) {
            // checking if the page number is greater than limit.
            // displaying toast message in this case when page>limit.
            // Toast.makeText(requireContext(), "That's all the data..", Toast.LENGTH_SHORT).show();

            // hiding our progress bar.
            binding.idPBLoading.setVisibility(View.GONE);
            return;
        }
        val apiClient: ApiClient = ApiClient()
        if (utils.isConnectingToInternet(this@FAQActivity)) {
            val url = Constants.BASE_URL + "get_faqs" + "?page=" + page
            apiClient.getApiService().getFaq(url)
                .enqueue(object : Callback<FAQResponseModel> {

                    override fun onResponse(
                        call: Call<FAQResponseModel>,
                        response: Response<FAQResponseModel>
                    ) {
                        val signupResponse = response.body()
                        if (signupResponse!!.status == true) {
                            if (!signupResponse.data.equals("")) {

                                arrivalList = response.body()!!.data.data
                                limit = response.body()!!.data.last_page
                                emptyList.addAll(arrivalList)
                                binding.rvList.layoutManager =
                                    LinearLayoutManager(
                                        this@FAQActivity,
                                        RecyclerView.VERTICAL,
                                        false
                                    )
                                rvAdapter = RvAdapter(emptyList)
                                binding.rvList.adapter = rvAdapter
                                binding.idPBLoading.visibility = View.GONE
                                rvAdapter.notifyDataSetChanged()


                            } else {

                                utils.customToastFailure("No Data Found", this@FAQActivity)
                            }
                        } else {

                            utils.customToastFailure(
                                signupResponse.message,
                                this@FAQActivity
                            )
                            Log.d("mess", signupResponse.message)

                        }
                    }

                    override fun onFailure(call: Call<FAQResponseModel>, t: Throwable) {
                        utils.customToastFailure(t.message!!, this@FAQActivity)
                        Log.d("mess", t.message.toString())

                    }


                })


        } else {

            utils.customToastFailure("Check your internet connection", this@FAQActivity)

        }

    }



}