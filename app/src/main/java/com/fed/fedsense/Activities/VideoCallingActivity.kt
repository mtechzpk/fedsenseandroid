package com.fed.fedsense.Activities

import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.SurfaceView
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.Models.VideoCall.VideoCallResponse
import com.fed.fedsense.R
import com.fed.fedsense.util.Utilities
import com.google.gson.Gson
import com.mtechsoft.compassapp.services.ApiClient
import io.agora.rtc2.Constants
import io.agora.rtc2.IRtcEngineEventHandler
import io.agora.rtc2.RtcEngine
import io.agora.rtc2.video.VideoCanvas
import io.agora.rtc2.video.VideoEncoderConfiguration
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.lang.RuntimeException

class VideoCallingActivity : AppCompatActivity() {
    private var mRtcEngine: RtcEngine? = null

    // Permissions
    private val PERMISSION_REQ_ID = 22
    private val REQUESTED_PERMISSIONS =
        arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA)

    private val LOG_TAG  = MainActivity::class.java.simpleName
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    var from = ""

    // Handle SDK Events
    private val mRtcEventHandler: IRtcEngineEventHandler = object : IRtcEngineEventHandler() {
        override fun onUserJoined(uid: Int, elapsed: Int) {
            runOnUiThread { // set first remote user to the main bg video container
                setupRemoteVideoStream(uid)
            }
        }

        // remote user has left channel
        override fun onUserOffline(uid: Int, reason: Int) { // Tutorial Step 7
            runOnUiThread { onRemoteUserLeft() }
        }

        // remote user has toggled their video
        override fun onRemoteVideoStateChanged(uid: Int, state: Int, reason: Int, elapsed: Int) {
            runOnUiThread { onRemoteUserVideoToggle(uid, state) }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_calling)

        apiClient = ApiClient()
        if (!::utilities.isInitialized) utilities = Utilities(this)

        if (checkSelfPermission(
                REQUESTED_PERMISSIONS.get(0),
                PERMISSION_REQ_ID
            ) &&
            checkSelfPermission(
                REQUESTED_PERMISSIONS.get(1),
                PERMISSION_REQ_ID
            ))
        {
            initAgoraEngine()

        }

        findViewById<View>(R.id.audioBtn).visibility = View.GONE // set the audio button hidden

        findViewById<View>(R.id.leaveBtn).visibility = View.GONE // set the leave button hidden

        findViewById<View>(R.id.videoBtn).visibility = View.GONE // set the video button hidden

        initt()


    }

    private fun initt() {

        from = intent.getStringExtra("from").toString()
        if (from.equals("videocall")){

            videocallApi()

        }else{

            var token = intent.getStringExtra("token")
            var channel = intent.getStringExtra("channel")
            onjoinChannelClicked(token,channel)


        }
    }

    private fun initAgoraEngine() {
        mRtcEngine = try {
            RtcEngine.create(baseContext, getString(R.string.agora_app_id), mRtcEventHandler)
        } catch (e: Exception) {
            Log.e(LOG_TAG, Log.getStackTraceString(e))
            throw RuntimeException(
                """
                    NEED TO check rtc sdk init fatal error
                    ${Log.getStackTraceString(e)}
                    """.trimIndent()
            )
        }
        setupSession()
    }

    private fun setupSession() {
        mRtcEngine!!.setChannelProfile(Constants.CHANNEL_PROFILE_COMMUNICATION)
        mRtcEngine!!.enableVideo()
        mRtcEngine!!.setVideoEncoderConfiguration(
            VideoEncoderConfiguration(
                VideoEncoderConfiguration.VD_640x480,
                VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_30,
                VideoEncoderConfiguration.STANDARD_BITRATE,
                VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT
            )
        )
    }

    private fun setupLocalVideoFeed() {

        // setup the container for the local user
        val videoContainer = findViewById<FrameLayout>(R.id.floating_video_container)
        val videoSurface = RtcEngine.CreateRendererView(baseContext)
        videoSurface.setZOrderMediaOverlay(true)
        videoContainer.addView(videoSurface)
        mRtcEngine!!.setupLocalVideo(VideoCanvas(videoSurface, VideoCanvas.RENDER_MODE_FIT, 0))
    }

    private fun setupRemoteVideoStream(uid: Int) {
        // setup ui element for the remote stream
        val videoContainer = findViewById<FrameLayout>(R.id.bg_video_container)
        // ignore any new streams that join the session
        if (videoContainer.childCount >= 1) {
            return
        }
        val videoSurface = RtcEngine.CreateRendererView(baseContext)
        videoContainer.addView(videoSurface)
        mRtcEngine!!.setupRemoteVideo(VideoCanvas(videoSurface, VideoCanvas.RENDER_MODE_FIT, uid))
        mRtcEngine!!.setRemoteSubscribeFallbackOption(Constants.STREAM_FALLBACK_OPTION_VIDEO_STREAM_LOW)
    }

    fun onAudioMuteClicked(view: View) {
        val btn = view as ImageView
        if (btn.isSelected) {
            btn.isSelected = false
            btn.setImageResource(R.drawable.audio_toggle_btn)
        } else {
            btn.isSelected = true
            btn.setImageResource(R.drawable.audio_toggle_active_btn)
        }
        mRtcEngine!!.muteLocalAudioStream(btn.isSelected)
    }
    fun onVideoMuteClicked(view: View) {
        val btn = view as ImageView
        if (btn.isSelected) {
            btn.isSelected = false
            btn.setImageResource(R.drawable.video_toggle_btn)
        } else {
            btn.isSelected = true
            btn.setImageResource(R.drawable.video_toggle_active_btn)
        }
        mRtcEngine!!.muteLocalVideoStream(btn.isSelected)
        val container = findViewById<FrameLayout>(R.id.floating_video_container)
        container.visibility = if (btn.isSelected) View.GONE else View.VISIBLE
        val videoSurface = container.getChildAt(0) as SurfaceView
        videoSurface.setZOrderMediaOverlay(!btn.isSelected)
        videoSurface.visibility = if (btn.isSelected) View.GONE else View.VISIBLE
    }

    // join the channel when user clicks UI button
    fun onjoinChannelClicked(token: String?, channel: String?) {


        mRtcEngine!!.joinChannel(
            token,
            channel,
            "Extra Optional Data",
            0
        ) // if you do not specify the uid, Agora will assign one.
        setupLocalVideoFeed()
        findViewById<View>(R.id.joinBtn).visibility = View.GONE // set the join button hidden
        findViewById<View>(R.id.audioBtn).visibility = View.GONE // set the audio button hidden
        findViewById<View>(R.id.leaveBtn).visibility = View.VISIBLE // set the leave button hidden
        findViewById<View>(R.id.videoBtn).visibility = View.GONE // set the video button hidden

    }
    fun onLeaveChannelClicked(view: View?) {

        if (from.equals("video_call")){

            leaveChannel()
            removeVideo(R.id.floating_video_container)
            removeVideo(R.id.bg_video_container)
            findViewById<View>(R.id.joinBtn).visibility = View.GONE // set the join button visible
            findViewById<View>(R.id.audioBtn).visibility = View.GONE // set the audio button hidden
            findViewById<View>(R.id.leaveBtn).visibility = View.GONE // set the leave button hidden
            findViewById<View>(R.id.videoBtn).visibility = View.GONE // set the video button hidden
            val sharedPref : SharedPreferences =
                getSharedPreferences("TOKEN", Context.MODE_PRIVATE)
            val editor : SharedPreferences.Editor = sharedPref.edit()
            editor.putString("IN_CALL", "")
            editor.apply()
        }else{

            leaveChannel()
            finish()
            val sharedPref : SharedPreferences =
                getSharedPreferences("TOKEN", Context.MODE_PRIVATE)
            val editor : SharedPreferences.Editor = sharedPref.edit()
            editor.putString("IN_CALL", "")
            editor.apply()
        }

    }

    private fun leaveChannel() {
        mRtcEngine!!.leaveChannel()
    }

    private fun removeVideo(containerID: Int) {
        val videoContainer = findViewById<FrameLayout>(containerID)
        videoContainer.removeAllViews()
    }

    private fun onRemoteUserVideoToggle(uid: Int, state: Int) {

        if (state == 0){

            finish()

            val sharedPref : SharedPreferences = getSharedPreferences("TOKEN", Context.MODE_PRIVATE)
            val editor : SharedPreferences.Editor = sharedPref.edit()
            editor.putString("IN_CALL", "")
            editor.apply()
        }


        // add an icon to let the other user know remote video has been disabled
        if (state == 0) {
//            val noCamera = ImageView(this)
//            noCamera.setImageResource(R.drawable.video_disabled)
//            videoContainer.addView(noCamera)
        } else {
//            val noCamera = videoContainer.getChildAt(1) as ImageView
//            if (noCamera != null) {
//                videoContainer.removeView(noCamera)
//            }
        }
    }

    private fun onRemoteUserLeft() {
        removeVideo(R.id.bg_video_container)
    }


    fun checkSelfPermission(permission: String, requestCode: Int): Boolean {
        Log.i(LOG_TAG, "checkSelfPermission $permission $requestCode")
        if (ContextCompat.checkSelfPermission(
                this,
                permission
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                REQUESTED_PERMISSIONS,
                requestCode
            )
            return false
        }
        return true
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.i(
           LOG_TAG,
            "onRequestPermissionsResult " + grantResults[0] + " " + requestCode
        )
        when (requestCode) {
            PERMISSION_REQ_ID -> {
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    Log.i(
                        LOG_TAG,
                        "Need permissions " + Manifest.permission.RECORD_AUDIO + "/" + Manifest.permission.CAMERA
                    )

                }
                initAgoraEngine()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        leaveChannel()
        RtcEngine.destroy()
        mRtcEngine = null
    }

    fun showLongToast(msg: String?) {
        runOnUiThread { Toast.makeText(applicationContext, msg, Toast.LENGTH_LONG).show() }
    }

    private fun videocallApi() {

        val gsonn = Gson()

        val jsonn: String = utilities.getString(this@VideoCallingActivity, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        apiClient.getApiService().video_call(user_idd,
            "34")
            .enqueue(object : Callback<VideoCallResponse> {

                override fun onResponse(call: Call<VideoCallResponse>, response: Response<VideoCallResponse>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()
                    if (signupResponse!!.status == true) {

                        /*mRtcEngine!!.joinChannel(
                            signupResponse.data.call_token,
                            signupResponse.data.channel_name,
                            "Extra Optional Data",
                            0
                        ) // if you do not specify the uid, Agora will assign one.
                        setupLocalVideoFeed()
                        findViewById<View>(R.id.joinBtn).visibility = View.GONE // set the join button hidden
                        findViewById<View>(R.id.audioBtn).visibility = View.VISIBLE // set the audio button hidden
                        findViewById<View>(R.id.leaveBtn).visibility = View.VISIBLE // set the leave button hidden
                        findViewById<View>(R.id.videoBtn).visibility = View.VISIBLE // set the video button hidden*/
                            onjoinChannelClicked(signupResponse.data.call_token,signupResponse.data.channel_name)
                        Toast.makeText(this@VideoCallingActivity, signupResponse.message, Toast.LENGTH_SHORT).show()

                    } else {
                        /*Toast.makeText(
                            this@ResumeActivity,
                            "" + signupResponse.message,
                            Toast.LENGTH_SHORT).show()*/
                        utilities.customToastFailure(signupResponse.message,this@VideoCallingActivity)

                    }
                }

                override fun onFailure(call: Call<VideoCallResponse>, t: Throwable) {

                    // Toast.makeText(this@ResumeActivity, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(),this@VideoCallingActivity)

                }


            })


    }

}