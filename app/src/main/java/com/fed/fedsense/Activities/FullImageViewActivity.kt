package com.fed.fedsense.Activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.fed.fedsense.R
import com.mtechsoft.compassapp.networking.Constants

class FullImageViewActivity : AppCompatActivity() {
    var image = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_image_view)

        val imageView = findViewById<ImageView>(R.id.img)
        val intent = intent
        image = intent.getStringExtra("image").toString()
        Glide.with(this@FullImageViewActivity).load(Constants.BASE_URL_IMG + image).into(imageView)
    }
}