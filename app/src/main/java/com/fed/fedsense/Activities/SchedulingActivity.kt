package com.fed.fedsense.Activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.room.Room
import com.fed.fedsense.Models.BaseResponse
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.Models.payment.Card
import com.fed.fedsense.util.Utilities
import com.fed.fedsense.Models.scheduling.AdminSchedulesResponseModel
import com.fed.fedsense.R
import com.fed.fedsense.RoomDB.CraditCard
import com.fed.fedsense.RoomDB.MyAppDataBase
import com.fed.fedsense.util.googlepay.PaymentsUtil
import com.google.android.material.card.MaterialCardView
import com.google.gson.Gson
import com.mtechsoft.compassapp.services.ApiClient
import com.stripe.android.PaymentConfiguration
import com.stripe.android.googlepaylauncher.GooglePayEnvironment
import com.stripe.android.googlepaylauncher.GooglePayLauncher
import kotlinx.android.synthetic.main.item_notifaction.*
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class SchedulingActivity : AppCompatActivity() {

    private lateinit var myAppDatabase: MyAppDataBase
    lateinit var back: RelativeLayout
    lateinit var submit: CardView
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    lateinit var dateSpinner: Spinner
    lateinit var timeSpinner: Spinner
    var idDate: String = ""
    var pos: Int = 0
    var idTime: String = ""

    var spinnerTimeText: String = ""
    var spinnerDateText: String = ""
    lateinit var googlePayLauncher: GooglePayLauncher

    //google pay
    private val shippingCost = (90 * 1000000).toLong()
    private lateinit var garmentList: JSONArray
    private lateinit var selectedGarment: JSONObject
    private val LOAD_PAYMENT_DATA_REQUEST_CODE = 991
    var user_id = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val window = window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_scheduling)


        init()
        onClicks()
        getAdminSchdulingTime()


    }

    private fun init() {

        myAppDatabase = Room.databaseBuilder(this, MyAppDataBase::class.java, "FEDENSEDB")
            .allowMainThreadQueries().build()
        back = findViewById(R.id.icBack)
        submit = findViewById(R.id.tvSubmit)
        dateSpinner = findViewById(R.id.ed_spinner_date)
        timeSpinner = findViewById(R.id.ed_spinner_susbCat)
        apiClient = ApiClient()
        utilities = Utilities(this@SchedulingActivity)
        if (!::utilities.isInitialized) utilities = Utilities(this@SchedulingActivity)
        back.setOnClickListener { onBackPressed() }
        apiClient = ApiClient()
        val gsonn = Gson()
        val jsonn: String = utilities.getString(this, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        user_id = java.lang.String.valueOf(obj.id)

    }


    private fun onClicks() {
        submit.setOnClickListener {
            /*spinnerTimeText = timeSpinner.selectedItem.toString()
            spinnerDateText = dateSpinner.selectedItem.toString()*/
            submit()
        }
    }

    private fun submit() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.popup_4b)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {
            dialog.dismiss()
            /*dateee = dateSpinner.selectedItem.toString()
            timeee = timeSpinner.selectedItem.toString()*/

            var count = myAppDatabase.cardDao().loadAll().size
            if (count > 0) {


                var have = utilities.getString(this, "have")
                if (have.equals("true")) {

                    val gsonn = Gson()
                    val jsonn: String = utilities.getStrings(this, "mycard")
                    if (!jsonn.isEmpty()) {
                        val obj: Card = gsonn.fromJson(jsonn, Card::class.java)
                    }
                    if (jsonn.isEmpty()) {

                        val intent = Intent(this, CardListActivity::class.java)
                        intent.putExtra("amount", "90")
                        intent.putExtra("from", "schedule")
                        startActivity(intent)
                        finish()

                    } else {

                        val gsonn = Gson()
                        val jsonn: String = utilities.getStrings(this, "mycard")
                        val card: CraditCard = gsonn.fromJson(jsonn, CraditCard::class.java)
                        payAmount(card)
                    }

                } else {

                    Toast.makeText(this, "Please select a card", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this@SchedulingActivity, CardListActivity::class.java))

                }


            } else {

                addCardPopup()
            }


        }

        dialog.show()
    }

    private fun payAmount(card: CraditCard) {


        if (utilities.isConnectingToInternet(this)) {

            var expiry = card.expiryData

            val separated = expiry.split("/").toTypedArray()
            val month = separated[0]
            val year = separated[1]
            utilities.showProgressDialog(this, "Processing ...")
            apiClient.getApiService().charge_payment(
                user_id,
                card.cardNumber,
                month,
                year,
                card.cvv,
                "140"

            )
                .enqueue(object : Callback<BaseResponse> {

                    override fun onResponse(
                        call: Call<BaseResponse>,
                        response: Response<BaseResponse>
                    ) {
                        val signupResponse = response.body()
                        utilities.hideProgressDialog()
                        if (signupResponse!!.status == true) {

                            enrollApplicationApi()
                        } else {
                            utilities.hideProgressDialog()
                            utilities.customToastFailure(
                                signupResponse.message,
                                this@SchedulingActivity
                            )

                        }
                    }

                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        utilities.hideProgressDialog()
                        utilities.customToastFailure(t.toString(), this@SchedulingActivity)
                    }
                })
        } else {
            Toast.makeText(
                this,
                "Check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        }


    }

    private fun enrollApplicationApi() {
        val gsonn = Gson()
        val jsonn: String = utilities.getString(this@SchedulingActivity, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)
        if (timeee.equals(""))
        {
            timeee = "1"
        }
        apiClient.getApiService().enrollInFedApplication(
            user_idd,
            "enroll_in_fed_app", "110",
            "$", dateee,
            timeee
        )
            .enqueue(object : Callback<BaseResponse> {

                override fun onResponse(
                    call: Call<BaseResponse>, response: Response<BaseResponse>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()
                    if (signupResponse!!.status == true) {
                        //utilities.customToastSuccess(signupResponse.message,this@SchedulingActivity)
                        Log.i("time", timeSpinner.selectedItem.toString() + " " + dateSpinner.selectedItem.toString())
                        paynow()

                    } else {

                        utilities.customToastFailure(
                            signupResponse.message,
                            this@SchedulingActivity
                        )


                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                    // Toast.makeText(this@SchedulingActivity, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(), this@SchedulingActivity)

                }


            })
    }


    private fun paynow() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.popup_4c)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {


            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            dialog.dismiss()
            finish()
        }

        dialog.show()
    }

    private fun getAdminSchdulingTime() {


        utilities.showProgressDialog(this@SchedulingActivity, "Loading ...")
        apiClient.getApiService().adminSchdules()
            .enqueue(object : Callback<AdminSchedulesResponseModel> {

                override fun onResponse(
                    call: Call<AdminSchedulesResponseModel>,
                    response: Response<AdminSchedulesResponseModel>
                ) {
                    val adminSchedule = response.body()
                    utilities.hideProgressDialog()
                    if (adminSchedule!!.status == true) {
                        // Toast.makeText(this@SchedulingActivity,adminSchedule.message,Toast.LENGTH_SHORT).show()
                        // utilities.customToastSuccess(adminSchedule.message,this@SchedulingActivity)

                        val item = ArrayList<String>()
                        for (i in 0..adminSchedule.data.size - 1) {
                            item.add(adminSchedule.data.get(i).schedule_date)
                        }
                        if (item != null) {
                            val adapter = ArrayAdapter(
                                this@SchedulingActivity,
                                android.R.layout.simple_spinner_item,
                                item
                            )


                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                            dateSpinner.setAdapter(adapter)


                        } else {
                            utilities.hideProgressDialog()
                            /*Toast.makeText(
                                this@SchedulingActivity,
                                "No Date Are Available",
                                Toast.LENGTH_SHORT
                            ).show()*/
                            utilities.customToastFailure(
                                "No Date Are Available",
                                this@SchedulingActivity
                            )


                        }
                        dateSpinner.setOnItemSelectedListener(object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View,
                                position: Int,
                                id: Long
                            ) {
                                val subCat =
                                    adminSchedule.data.get(position).time_slots
                                idDate = adminSchedule.data.get(position).id
                                dateee = adminSchedule.data.get(position).schedule_date
                                if (adminSchedule.data.get(position).time_slots.isEmpty())
                                {
                                    timeee = ""
                                }
                                val itemSub = ArrayList<String>()
                                for (i in 0..subCat.size - 1) {
                                    itemSub.add(subCat.get(i).schedule_time)
                                }
                                if (itemSub != null) {
                                    val adapter = ArrayAdapter(
                                        this@SchedulingActivity,
                                        android.R.layout.simple_spinner_item,
                                        itemSub
                                    )
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                                    timeSpinner.setAdapter(adapter)
                                    pos = position
                                }

                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        })
                        timeSpinner.setOnItemSelectedListener(object :
                            AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View,
                                position: Int,
                                id: Long
                            ) {
                                idTime = adminSchedule.data.get(pos).time_slots.get(position).id
                                timeee= adminSchedule.data.get(pos).time_slots.get(position).schedule_time
                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {}
                        })

                    } else {

                        /* Toast.makeText(
                             this@SchedulingActivity,
                             "" + adminSchedule.message,
                             Toast.LENGTH_SHORT
                         ).show()*/
                        utilities.customToastFailure(adminSchedule.message, this@SchedulingActivity)

                    }
                }

                override fun onFailure(call: Call<AdminSchedulesResponseModel>, t: Throwable) {

                    utilities.hideProgressDialog()
                    //Toast.makeText(this@SchedulingActivity, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(), this@SchedulingActivity)

                }


            })

    }

    private fun addCardPopup() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.add_card_dialog)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {

            dialog.dismiss()
            val intent = Intent(Intent(this, AddCardActivity::class.java))
            startActivity(intent)

        }
        dialog.show()
    }


    companion object {

        var dateee = ""
        var timeee = ""
    }
}