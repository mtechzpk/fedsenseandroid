package com.fed.fedsense.Activities

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.fed.fedsense.util.Utilities
import com.fed.fedsense.R
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Log
import com.google.android.exoplayer2.util.Util
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import kotlinx.android.synthetic.main.activity_quick_qualififcation.*
import kotlin.math.log

class QuickQualififcationActivity : AppCompatActivity() {


    lateinit var next: CardView
    lateinit var title: TextView
    lateinit var description: TextView
    lateinit var video_rel: RelativeLayout
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    private var player: SimpleExoPlayer? = null
    private var playerView: PlayerView? = null
    var isSubscribes: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_SECURE,
            WindowManager.LayoutParams.FLAG_SECURE
        )
        setContentView(R.layout.activity_quick_qualififcation)


        initt()


        next.setOnClickListener {
            val intent = Intent(this, Activity3B::class.java)
            intent.putExtra("isChatSub",isSubscribes)
            startActivity(intent)
            finish()
        }
        img_fullScreen.setOnClickListener {
            val intent : Intent = Intent(this@QuickQualififcationActivity,FullScreenVideoActivity::class.java)
            utilities.saveString(this@QuickQualififcationActivity,"videourl",Constants.BASE_URL_IMG + "service_videos/quick_qualification_consultation.mp4")
            startActivity(intent)
        }
    }

    private fun initt() {

        next = findViewById(R.id.ivNext)
        title = findViewById(R.id.titleText)
        description = findViewById(R.id.discText)
        playerView = findViewById(R.id.video_view)
        video_rel = findViewById(R.id.video_rel)

        title.setText(intent.getStringExtra("quicktitle"))
        description.setText(intent.getStringExtra("quickdescription"))
        isSubscribes = intent.getStringExtra("quickisSubsribed").toBoolean()
        Log.d("dos",isSubscribes.toString())
        apiClient = ApiClient()
        utilities = Utilities(this)
        if (!::utilities.isInitialized) utilities = Utilities(this)

        setVideo()
    }

    fun setVideo(){

        val appNameStringRes = R.string.app_name
        val trackSelectorDef: TrackSelector = DefaultTrackSelector(this)
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelectorDef)
        val userAgent = Util.getUserAgent(this, this.getString(appNameStringRes))
        val defaultDataSourceFactory = DefaultDataSourceFactory(this, userAgent)
        val uriOfContentUrl = Uri.parse(Constants.BASE_URL_IMG + "service_videos/quick_qualification_consultation.mp4")
        val mediaSource: MediaSource = ProgressiveMediaSource.Factory(defaultDataSourceFactory)
            .createMediaSource(uriOfContentUrl)
        //MediaSource mediaSource = new ExtractorMediaSource(uriOfContentUrl, new CacheDataSourceFactory(context, 100 * 1024 * 1024, 500 * 1024 * 1024), new DefaultExtractorsFactory(), null, null);
        //MediaSource mediaSource = new ExtractorMediaSource(uriOfContentUrl, new CacheDataSourceFactory(context, 100 * 1024 * 1024, 500 * 1024 * 1024), new DefaultExtractorsFactory(), null, null);
        player!!.prepare(mediaSource)
        player!!.setPlayWhenReady(false)
        playerView!!.requestFocus()
        playerView!!.player = player
        playerView!!.setShowBuffering(PlayerView.SHOW_BUFFERING_WHEN_PLAYING)
        player!!.setVolume(1f)

    }
    override fun onDestroy() {
        super.onDestroy()

        player!!.stop()
        player!!.release()
    }
}