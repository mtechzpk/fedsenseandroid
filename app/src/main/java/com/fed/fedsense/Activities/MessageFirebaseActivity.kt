package com.fed.fedsense.Activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fed.fedsense.databinding.ActivityMessageFirebaseBinding

class MessageFirebaseActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMessageFirebaseBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMessageFirebaseBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }
}