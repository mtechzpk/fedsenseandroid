package com.fed.fedsense.Activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.fed.fedsense.util.Utilities
import com.fed.fedsense.Models.SignUp.SignupReponse
import com.fed.fedsense.R
import com.fed.fedsense.databinding.ActivitySignUpBinding
import com.google.android.material.card.MaterialCardView
import com.mtechsoft.compassapp.services.ApiClient
import kotlinx.android.synthetic.main.activity_sign_up.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SignUpActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySignUpBinding
    private lateinit var btnSignIn: TextView
    private lateinit var context: Context
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    var findus = ""
    var usajob = ""
    var disability = ""
    var employe = ""
    var mili = ""
    var referName = ""
    var referEmail = ""
    var referPhone = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        initt()
        clicks()
        radiofunction()
        btnSignIn.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun clicks() {



        binding.btnContinue.setOnClickListener {
            doYouHaveJob()
            binding.tvHaveJob.text = "Continue"
        }
        binding.btnSignup.setOnClickListener {

            val str_email: String = binding.edEmail.text.toString()
            val str_pass: String = binding.edPass.text.toString()
            val con_pass: String = binding.edConfirmPass.text.toString()

            if (findus.equals("")) {
                findus = ""
            }
            if (usajob.equals("")) {
                usajob = ""
            }
            if (mili.equals("")) {
                mili = ""
            }
            if (employe.equals("")) {
                employe = ""
            }
            if (disability.equals("")) {
                disability = ""
            }
            if (referName.equals("")) {
                referName = ""
            }
            if (referEmail.equals("")) {
                referEmail = ""
            }
            if (referPhone.equals("")) {
                referPhone = ""
            }

            if (str_email.isEmpty()) {

                utilities.customToastFailure("Email is empty", this@SignUpActivity)


            } else if (!isValidEmail(str_email)) {

                utilities.customToastFailure("Invalid Email", this@SignUpActivity)

            } else if (str_pass.isEmpty()) {

                utilities.customToastFailure("Password is empty", this@SignUpActivity)

            } else if (con_pass.isEmpty()) {

                utilities.customToastFailure("Confirm Password is empty", this@SignUpActivity)

            } else if (str_pass != con_pass) {

                utilities.customToastFailure("Password not matched", this@SignUpActivity)

            } else {

                signup(
                    str_email,
                    str_pass,
                    findus,
                    usajob,
                    mili,
                    employe,
                    disability,
                    referName,
                    referPhone,
                    referEmail
                )
            }
        }
    }

    private fun signup(
        strEmail: String, strPass: String, findus: String,
        usaprofile: String, militaryVetran: String,
        employe: String, disability: String, referName: String,
        referPhone: String, referEmail: String
    ) {

        utilities.showProgressDialog(context, "Loading ...")
        if (utilities.isConnectingToInternet(this@SignUpActivity)) {


            apiClient.getApiService().signup(
                strEmail, strPass, findus, usaprofile, militaryVetran,
                employe, disability, referName, referPhone, referEmail
            )
                .enqueue(object : Callback<SignupReponse> {

                    override fun onFailure(call: Call<SignupReponse>, t: Throwable) {

                        utilities.hideProgressDialog()
                        // Toast.makeText(applicationContext, t.toString(), Toast.LENGTH_SHORT).show()
                        utilities.customToastFailure(t.toString(), this@SignUpActivity)

                    }

                    override fun onResponse(
                        call: Call<SignupReponse>,
                        response: Response<SignupReponse>
                    ) {
                        val signupResponse = response.body()
                        utilities.hideProgressDialog()
                        if (signupResponse!!.status == true) {

                            /*utilities.customToastSuccess(
                                signupResponse.message,
                                this@SignUpActivity
                            )*/

                            startActivity(Intent(this@SignUpActivity, LoginActivity::class.java))
                            finish()

                        } else {

                            //  Toast.makeText(applicationContext, "" + signupResponse.message, Toast.LENGTH_SHORT).show()
                            utilities.customToastFailure(
                                signupResponse.message,
                                this@SignUpActivity
                            )

                        }
                    }
                })
        } else {
            Toast.makeText(
                this@SignUpActivity,
                "Check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        }

    }


    fun isValidEmail(target: CharSequence?): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

    private fun initt() {

        context = this
        apiClient = ApiClient()
        if (!::utilities.isInitialized) utilities = Utilities(this)
        btnSignIn = findViewById(R.id.tvSignIn)

        val gradientDrawable = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM,
            intArrayOf(
                Color.parseColor("#5AFF15"),
                Color.parseColor("#008000")
            )
        )
        gradientDrawable.cornerRadius = 8f;
        binding.btnSignup.setBackground(gradientDrawable)


        binding.edEmail.setOnTouchListener(View.OnTouchListener { v, event ->
            binding.rlEmail.setBackground(getDrawable(R.drawable.item_selected_border))
            binding.rlPassword.setBackground(getDrawable(R.drawable.item_selected_border_white))
            binding.rlConfirmPassword.setBackground(getDrawable(R.drawable.item_selected_border_white))
            binding.ivEmailLogin.setImageResource(R.drawable.ic_email_white)
            binding.ivPassLogin.setImageResource(R.drawable.ic_pass_grey)
            binding.ivConfirmPassSignup.setImageResource(R.drawable.ic_pass_grey)

            false
        })

        binding.edPass.setOnTouchListener(View.OnTouchListener { v, event ->
            binding.rlEmail.setBackground(getDrawable(R.drawable.item_selected_border_white))
            binding.rlPassword.setBackground(getDrawable(R.drawable.item_selected_border))
            binding.rlConfirmPassword.setBackground(getDrawable(R.drawable.item_selected_border_white))
            binding.ivEmailLogin.setImageResource(R.drawable.ic_email_grey)
            binding.ivPassLogin.setImageResource(R.drawable.ic_pass_white)
            binding.ivConfirmPassSignup.setImageResource(R.drawable.ic_pass_grey)

            false
        })

        binding.edConfirmPass.setOnTouchListener(View.OnTouchListener { v, event ->
            binding.rlEmail.setBackground(getDrawable(R.drawable.item_selected_border_white))
            binding.rlPassword.setBackground(getDrawable(R.drawable.item_selected_border_white))
            binding.rlConfirmPassword.setBackground(getDrawable(R.drawable.item_selected_border))
            binding.ivEmailLogin.setImageResource(R.drawable.ic_email_grey)
            binding.ivPassLogin.setImageResource(R.drawable.ic_pass_grey)
            binding.ivConfirmPassSignup.setImageResource(R.drawable.ic_pass_white)

            false
        })
        binding.refername.setOnTouchListener(View.OnTouchListener { v, event ->
            binding.rleferemail.setBackground(getDrawable(R.drawable.item_selected_border_white))
            binding.rlrefername.setBackground(getDrawable(R.drawable.item_selected_border))
            binding.rlrefernumber.setBackground(getDrawable(R.drawable.item_selected_border_white))

            false
        })
        binding.referemail.setOnTouchListener(View.OnTouchListener { v, event ->
            binding.rlrefername.setBackground(getDrawable(R.drawable.item_selected_border_white))
            binding.rleferemail.setBackground(getDrawable(R.drawable.item_selected_border))
            binding.rlrefernumber.setBackground(getDrawable(R.drawable.item_selected_border_white))

            false
        })
        binding.refernumber.setOnTouchListener(View.OnTouchListener { v, event ->
            binding.rlrefername.setBackground(getDrawable(R.drawable.item_selected_border_white))
            binding.rlrefernumber.setBackground(getDrawable(R.drawable.item_selected_border))
            binding.rleferemail.setBackground(getDrawable(R.drawable.item_selected_border_white))

            false
        })


    }


    private fun radiofunction() {

        binding.citizenradiogroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rb_facebook -> {
                    findus = "Facebook"
                }
                R.id.rb_google -> {
                    findus = "Google"

                }
                R.id.rb_walkin -> {
                    findus = "Walk In"

                }
                R.id.rb_employee -> {
                    findus = "Employee"

                }
                R.id.rb_website -> {
                    findus = "Website"

                }
                R.id.rb_other -> {
                    findus = "Other"
                }
            }
        })

        binding.usajob.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rb_yes -> {
                    usajob = "Yes"
                }
                R.id.rb_no -> {
                    usajob = "No"

                }

            }
        })
        binding.disability.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rb_yess -> {
                    disability = "Yes"
                }
                R.id.rb_noo -> {
                    disability = "No"

                }

            }
        })
        binding.usaemployee.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rb_yesep -> {
                    employe = "Yes"
                }
                R.id.rb_noep -> {
                    employe = "No"

                }

            }
        })

        binding.military.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rb_yesmili -> {
                    mili = "Yes"
                }
                R.id.rb_nomili -> {
                    mili = "No"

                }

            }
        })


    }

    private fun doYouHaveJob() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.popup_currentlyjob)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvYes)
        val no = dialog.findViewById<MaterialCardView>(R.id.tvNo)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {
            usajob = "Yes"
            binding.tvHaveJob.text = "Yes"
            areyouusemploye()
            dialog.dismiss()


        }
        no.setOnClickListener {
            usajob = "No"
            binding.tvHaveJob.text = "No"
            areyouusemploye()
            dialog.dismiss()

        }

        dialog.show()
    }

    private fun areyouusemploye() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.dialog_usemploye)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvYes)
        val no = dialog.findViewById<MaterialCardView>(R.id.tvNo)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {
            employe = "Yes"
            binding.tvUsEmploye.text = "Yes"
            areyoumiltary()
            dialog.dismiss()
        }
        no.setOnClickListener {
            employe = "No"
            binding.tvUsEmploye.text = "No"
            areyoumiltary()
            dialog.dismiss()

        }

        dialog.show()
    }

    private fun areyoumiltary() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.dialog_milatary)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvYes)
        val no = dialog.findViewById<MaterialCardView>(R.id.tvNo)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {
            mili = "Yes"
            binding.tvMilatary.text = "Yes"
            disability()
            dialog.dismiss()

        }
        no.setOnClickListener {
            mili = "Yes"
            binding.tvMilatary.text = "No"
            disability()
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun disability() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.dialog_disability)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvYes)
        val no = dialog.findViewById<MaterialCardView>(R.id.tvNo)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {
            disability = "Yes"
            dialog.dismiss()
            binding.tvDiability.text = "Yes"
            findus()

        }
        no.setOnClickListener {
            disability = "Yes"
            dialog.dismiss()
            binding.tvDiability.text = "No"
            findus()
        }

        dialog.show()
    }

    private fun findus() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.dialog_findus)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val tvFacebook = dialog.findViewById<MaterialCardView>(R.id.tvFacebook)
        val tvGoogle = dialog.findViewById<MaterialCardView>(R.id.tvGoogle)
        val tvWalking = dialog.findViewById<MaterialCardView>(R.id.tvWalking)
        val tvEmployee = dialog.findViewById<MaterialCardView>(R.id.tvEmployee)
        val tvWebsite = dialog.findViewById<MaterialCardView>(R.id.tvWebsite)
        val tvOther = dialog.findViewById<MaterialCardView>(R.id.tvOther)
        close.setOnClickListener { dialog.dismiss() }
        tvFacebook.setOnClickListener {
            findus = "Facebook"
            binding.tvFindUs.text = "Facebook"
            references()
            dialog.dismiss()

        }
        tvGoogle.setOnClickListener {
            findus = "Google"
            binding.tvFindUs.text = "Google"
            references()
            dialog.dismiss()

        }
        tvWalking.setOnClickListener {
            findus = "Walk In"
            binding.tvFindUs.text = "Walk In"
            references()
            dialog.dismiss()

        }
        tvEmployee.setOnClickListener {
            findus = "Employee"
            binding.tvFindUs.text = "Employee"
            references()
            dialog.dismiss()

        }
        tvWebsite.setOnClickListener {
            findus = "Website"
            binding.tvFindUs.text = "Website"
            references()
            dialog.dismiss()

        }
        tvOther.setOnClickListener {
            findus = "Other"
            binding.tvFindUs.text = "Other"
            references()
            dialog.dismiss()

        }

        dialog.show()
    }

    private fun references() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.dialog_referencedetail)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val tvOther = dialog.findViewById<MaterialCardView>(R.id.tvNo)
        val tvSkip = dialog.findViewById<MaterialCardView>(R.id.tvSkip)
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val refername = dialog.findViewById<EditText>(R.id.refername)
        val referemail = dialog.findViewById<EditText>(R.id.referemail)
        val refernumber = dialog.findViewById<EditText>(R.id.refernumber)
        close.setOnClickListener {
            binding.btnSignup.visibility = View.VISIBLE
            binding.btnContinue.visibility = View.GONE
            dialog.dismiss()
        }

        tvSkip.setOnClickListener {
            dialog.dismiss()
            referName = refername.text.toString()
            referEmail = referemail.text.toString()
            referPhone = refernumber.text.toString()
            binding.btnSignup.visibility = View.VISIBLE
            binding.btnContinue.visibility = View.GONE
        }
        tvOther.setOnClickListener {
            dialog.dismiss()
            referName = refername.text.toString()
            referEmail = referemail.text.toString()
            referPhone = refernumber.text.toString()
            binding.btnSignup.visibility = View.VISIBLE
            binding.btnContinue.visibility = View.GONE
        }
        dialog.show()
    }


}