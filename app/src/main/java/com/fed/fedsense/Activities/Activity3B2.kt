package com.fed.fedsense.Activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.room.Room
import com.fed.fedsense.Models.BaseResponse
import com.fed.fedsense.Models.Home.HomeData
import com.fed.fedsense.Models.Home.HomeResponse
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.Models.payment.Card
import com.fed.fedsense.R
import com.fed.fedsense.RoomDB.CraditCard
import com.fed.fedsense.RoomDB.MyAppDataBase
import com.google.android.material.card.MaterialCardView
import com.google.gson.Gson
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class Activity3B2 : AppCompatActivity() {

    lateinit var yes: CardView
    lateinit var no: CardView
    lateinit var tvYes: TextView
    lateinit var tvNo: TextView
    private lateinit var utilities: com.fed.fedsense.util.Utilities
    private lateinit var apiClient: ApiClient
    var amount : String = ""
    var homedata: ArrayList<HomeData> = ArrayList()
    private lateinit var myAppDatabase: MyAppDataBase
    var user_id = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val window = window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
        setContentView(R.layout.activity_activity3_b2)


        myAppDatabase = Room.databaseBuilder(this, MyAppDataBase::class.java, "FEDENSEDB").allowMainThreadQueries().build()
        yes = findViewById(R.id.btnYes)
        no = findViewById(R.id.btnNo)
        tvNo = findViewById(R.id.tvNoo)
        utilities = com.fed.fedsense.util.Utilities(this@Activity3B2)
        if (!::utilities.isInitialized) utilities = com.fed.fedsense.util.Utilities(this@Activity3B2)
        apiClient = ApiClient()
        val gsonn = Gson()
        val jsonn: String = utilities.getString(this, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        val homeData : String= utilities.getString(this,"homedata")
        val homeobj : HomeResponse = gsonn.fromJson(homeData,HomeResponse::class.java)
        user_id = java.lang.String.valueOf(obj.id)
        yes.setBackgroundResource(R.drawable.bg_button)
        no.setBackgroundResource(R.drawable.bg_border_bold)




        yes.setOnClickListener {

            yess()
            utilities.saveString(this@Activity3B2,"yes","yes")

        }


        no.setOnClickListener {
//            yess2()
//            utilities.saveString(this@Activity3B2,"yes","no")
            onBackPressed()

        }
    }

    private fun yess() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.popup_3b3)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val tv1 = dialog.findViewById<CardView>(R.id.tv1)
        val tv2 = dialog.findViewById<CardView>(R.id.tv2)
        val tv3 = dialog.findViewById<CardView>(R.id.tv3)
        close.setOnClickListener { dialog.dismiss() }

        tv1.setOnClickListener {


            var count =  myAppDatabase.cardDao().loadAll().size
            if (count > 0){

                var have = utilities.getString(this, "have")
                if (have.equals("true")){

                    val gsonn = Gson()
                    val jsonn: String = utilities.getStrings(this, "mycard")
                    if (!jsonn.isEmpty()){
                        val obj: Card = gsonn.fromJson(jsonn, Card::class.java)
                    }
                    if (jsonn.isEmpty()){

                        val intent = Intent(this, CardListActivity::class.java)
                        intent.putExtra("amount","40")
                        intent.putExtra("from","anouncments")
                        startActivity(intent)
                        finish()
                    }else{

                        val gsonn1 = Gson()
                        val jsonn1: String = utilities.getStrings(this, "mycard")
                        val card: CraditCard = gsonn1.fromJson(jsonn1, CraditCard::class.java)
                        amount = "40"
                        payAmount(card)
                        // Toast.makeText(this, "from card", Toast.LENGTH_SHORT).show()
                    }

                   }else{

                    Toast.makeText(this, "Please select a card", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this@Activity3B2,CardListActivity::class.java))

                }

            }else{

                addCardPopup()
            }

            dialog.dismiss()

        }

        tv2.setOnClickListener {

            dialog.dismiss()
            var count =  myAppDatabase.cardDao().loadAll().size
            if (count > 0){

                var have = utilities.getString(this, "have")
                if (have.equals("true")){

                    val gsonn = Gson()
                    val jsonn: String = utilities.getStrings(this, "mycard")
                    if (!jsonn.isEmpty()){
                        val obj: Card = gsonn.fromJson(jsonn, Card::class.java)
                    }
                    if (jsonn.isEmpty()){

                        val intent = Intent(this, CardListActivity::class.java)
                        intent.putExtra("amount","30")
                        intent.putExtra("from","anouncments")
                        startActivity(intent)
                        finish()

                    }else{

                        val gsonn = Gson()
                        val jsonn: String = utilities.getStrings(this, "mycard")
                        val card: CraditCard = gsonn.fromJson(jsonn, CraditCard::class.java)
                        amount = "30"
                        payAmount(card)
                        // Toast.makeText(this, "from card", Toast.LENGTH_SHORT).show()
                    }

                    }else{

                    Toast.makeText(this, "Please select a card", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this@Activity3B2,CardListActivity::class.java))

                }
            }else{

                addCardPopup()
            }





        }

        tv3.setOnClickListener {

            dialog.dismiss()
            var count =  myAppDatabase.cardDao().loadAll().size
            if (count > 0){

                var have = utilities.getString(this, "have")
                if (have.equals("true")){


                    val gsonn = Gson()
                    val jsonn: String = utilities.getStrings(this, "mycard")
                    if (!jsonn.isEmpty()){
                        val obj: Card = gsonn.fromJson(jsonn, Card::class.java)
                    }
                    if (jsonn.isEmpty()){

                        val intent = Intent(this, CardListActivity::class.java)
                        intent.putExtra("amount","20")
                        intent.putExtra("from","anouncments")
                        startActivity(intent)
                        finish()
                    }else{

                        val gsonn = Gson()
                        val jsonn: String = utilities.getStrings(this, "mycard")
                        val card: CraditCard = gsonn.fromJson(jsonn, CraditCard::class.java)
                        amount = "20"
                        payAmount(card)
                    }
                  }else{

                    Toast.makeText(this, "Please select a card", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this@Activity3B2,CardListActivity::class.java))

                }





            }else{

                addCardPopup()
            }
        }
        dialog.show()

    }


    private fun addCardPopup() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.add_card_dialog)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {

            dialog.dismiss()
            val intent  = Intent(Intent(this,AddCardActivity::class.java))
            startActivity(intent)

        }
        dialog.show()
    }




    private fun payAmount(card: CraditCard) {


        if (utilities.isConnectingToInternet(this)) {

            var expiry = card.expiryData

            val separated = expiry.split("/").toTypedArray()
            val month = separated[0]
            val year = separated[1]
            utilities.showProgressDialog(this, "Processing ...")
            apiClient.getApiService().charge_payment(
                user_id,
                card.cardNumber,
                month,
                year,
                card.cvv,
                amount

            )
                .enqueue(object : Callback<BaseResponse> {

                    override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                        val signupResponse = response.body()
                        utilities.hideProgressDialog()
                        if (signupResponse!!.status == true) {

                            startActivity(Intent(this@Activity3B2,Activity3B4::class.java))
                        } else {
                            utilities.hideProgressDialog()
                            utilities.customToastFailure(signupResponse.message, this@Activity3B2)

                        }
                    }
                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        utilities.hideProgressDialog()
                        utilities.customToastFailure(t.toString(), this@Activity3B2)
                    }
                })
        } else {
            Toast.makeText(
                this,
                "Check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        }



    }
    private fun yess2() {

        val dialog = Dialog(this)
        dialog.setContentView(R.layout.popup_3b3)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val tv1 = dialog.findViewById<CardView>(R.id.tv1)
        val tv2 = dialog.findViewById<CardView>(R.id.tv2)
        val tv3 = dialog.findViewById<CardView>(R.id.tv3)
        close.setOnClickListener { dialog.dismiss() }


        tv1.setOnClickListener {


            var count =  myAppDatabase.cardDao().loadAll().size
            if (count > 0){

                var have = utilities.getString(this, "have")
                if (have.equals("true")){

                    val gsonn = Gson()
                    val jsonn: String = utilities.getStrings(this, "mycard")
                    if (!jsonn.isEmpty()){
                        val obj: Card = gsonn.fromJson(jsonn, Card::class.java)
                    }
                    if (jsonn.isEmpty()){

                        val intent = Intent(this, CardListActivity::class.java)
                        intent.putExtra("amount","40")
                        intent.putExtra("from","anouncments")
                        startActivity(intent)
                        finish()
                    }else{

                        val gsonn1 = Gson()
                        val jsonn1: String = utilities.getStrings(this, "mycard")
                        val card: CraditCard = gsonn1.fromJson(jsonn1, CraditCard::class.java)
                        amount = "40"
                        payAmountForNo(card)
                    }

                    }else{

                    Toast.makeText(this, "Please select a card", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this@Activity3B2,CardListActivity::class.java))

                }



            }else{

                addCardPopup()
            }

            dialog.dismiss()

        }




        tv2.setOnClickListener {

            dialog.dismiss()
            var count =  myAppDatabase.cardDao().loadAll().size
            if (count > 0){


                var have = utilities.getString(this, "have")
                if (have.equals("true")){

                    val gsonn = Gson()
                    val jsonn: String = utilities.getStrings(this, "mycard")
                    if (!jsonn.isEmpty()){
                        val obj: Card = gsonn.fromJson(jsonn, Card::class.java)
                    }
                    if (jsonn.isEmpty()){

                        val intent = Intent(this, CardListActivity::class.java)
                        intent.putExtra("amount","30")
                        intent.putExtra("from","anouncments")
                        startActivity(intent)
                        finish()
                    }else{

                        val gsonn = Gson()
                        val jsonn: String = utilities.getStrings(this, "mycard")
                        val card: CraditCard = gsonn.fromJson(jsonn, CraditCard::class.java)
                        amount = "30"
                        payAmountForNo(card)
                        // Toast.makeText(this, "from card", Toast.LENGTH_SHORT).show()
                    }
                  }else{

                    Toast.makeText(this, "Please select a card", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this@Activity3B2,CardListActivity::class.java))

                }

            }else{

                addCardPopup()
            }
        }



        tv3.setOnClickListener {

            dialog.dismiss()
            var count =  myAppDatabase.cardDao().loadAll().size
            if (count > 0){

                var have = utilities.getString(this, "have")
                if (have.equals("true")){



                    val gsonn = Gson()
                    val jsonn: String = utilities.getStrings(this, "mycard")
                    if (!jsonn.isEmpty()){
                        val obj: Card = gsonn.fromJson(jsonn, Card::class.java)
                    }
                    if (jsonn.isEmpty()){

                        val intent = Intent(this, CardListActivity::class.java)
                        intent.putExtra("amount","20")
                        intent.putExtra("from","anouncments")
                        startActivity(intent)
                        finish()
                    }else{

                        val gsonn = Gson()
                        val jsonn: String = utilities.getStrings(this, "mycard")
                        val card: CraditCard = gsonn.fromJson(jsonn, CraditCard::class.java)
                        amount = "20"
                        payAmountForNo(card)
                    }
                }else{

                    Toast.makeText(this, "Please select a card", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this@Activity3B2,CardListActivity::class.java))

                }



            }else{

                addCardPopup()
            }
        }

        dialog.show()

    }

    private fun payAmountForNo(card: CraditCard) {


        if (utilities.isConnectingToInternet(this)) {

            var expiry = card.expiryData

            val separated = expiry.split("/").toTypedArray()
            val month = separated[0]
            val year = separated[1]
            utilities.showProgressDialog(this, "Processing ...")
            apiClient.getApiService().charge_payment(
                user_id,
                card.cardNumber,
                month,
                year,
                card.cvv,
                amount

            )
                .enqueue(object : Callback<BaseResponse> {

                    override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                        val signupResponse = response.body()
                        utilities.hideProgressDialog()
                        if (signupResponse!!.status == true) {
                            callQualificationApi2()
                        } else {
                            utilities.hideProgressDialog()
                            utilities.customToastFailure(signupResponse.message, this@Activity3B2)

                        }
                    }
                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        utilities.hideProgressDialog()
                        utilities.customToastFailure(t.toString(), this@Activity3B2)
                    }
                })
        } else {
            Toast.makeText(
                this,
                "Check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        }



    }
    private fun callQualificationApi2() {
        val gsonn = Gson()
        val jsonn: String = utilities.getString(this@Activity3B2, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        apiClient.getApiService().quickApplication(user_idd,
            "quick_qualification_consultation",
            "48","hours",
            "2022-10-26 11:00:00",
            "2022-11-29 11:00:00",
            amount,"$","No Annoucement",
            "12/05/2022","No Title")
            .enqueue(object : Callback<BaseResponse> {

                override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()
                    if (signupResponse!!.status == true) {
                        //Toast.makeText(this@Activity3B4,response.message(),Toast.LENGTH_SHORT).show()
                        utilities.customToastSuccess(response.message(),this@Activity3B2)
                        getHomeData()
                    } else {
                        utilities.customToastFailure(signupResponse.message,this@Activity3B2)
                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                    // Toast.makeText(this@Activity3B4, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(),this@Activity3B2)

                }


            })



    }
    private fun getHomeData() {


        if (utilities.isConnectingToInternet(this@Activity3B2)) {

            val gsonn = Gson()
            val jsonn: String = utilities.getString(this@Activity3B2, "user")
            val obj: User = gsonn.fromJson(jsonn, User::class.java)
            var user_idd: String = java.lang.String.valueOf(obj.id)

            val url = Constants.BASE_URL + "home_screen_data/" + user_idd
            utilities.showProgressDialog(this@Activity3B2, "Loading ...")
            apiClient.getApiService().getHomeData(url)
                .enqueue(object : Callback<HomeResponse> {

                    override fun onResponse(
                        call: Call<HomeResponse>, response: Response<HomeResponse>
                    ) {
                        val signupResponse = response.body()
                        val data = response.body()?.homedata
                        utilities.hideProgressDialog()
                        if (signupResponse!!.status == true) {

                            homedata = response.body()!!.homedata
                            val gson = Gson()
                            val json = gson.toJson(signupResponse)
                            utilities.saveString(this@Activity3B2,"homedata",json)
                            submit()


                        } else {
                            utilities.hideProgressDialog()
                            utilities.customToastFailure(
                                signupResponse.message,
                                this@Activity3B2
                            )

                        }
                    }

                    override fun onFailure(call: Call<HomeResponse>, t: Throwable) {

                        utilities.hideProgressDialog()
                        utilities.customToastFailure(t.toString(), this@Activity3B2)

                    }


                })
        } else {
            Toast.makeText(
                this@Activity3B2,
                "Check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        }

    }
    private fun submit() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.popup_3b5)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {
            dialog.dismiss()

                val intent = Intent(this@Activity3B2, Activity3B::class.java)
                utilities.saveString(
                    this@Activity3B2,
                    "nohere",
                    "You have successfully subscribed to quick qualification"
                )
                utilities.saveString(this@Activity3B2, "pre", "preno")
                startActivity(intent)
                finish()
            /*}else{
                val intent = Intent(this@CardListActivity,MainActivity::class.java)
                startActivity(intent)
                finish()
            }*/

        }

        dialog.show()
    }


}