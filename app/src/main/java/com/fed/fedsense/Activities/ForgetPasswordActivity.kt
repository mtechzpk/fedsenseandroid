package com.fed.fedsense.Activities

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.fed.fedsense.Models.auth.a.forgotpassword.ForgotPasswordResponseModel
import com.fed.fedsense.R
import com.fed.fedsense.databinding.ActivityForgetPasswordBinding
import com.fed.fedsense.util.Utilities
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import com.report.app.DataModel.Forgetpassword.ForgetPasswordResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgetPasswordActivity : AppCompatActivity() {

    private lateinit var binding : ActivityForgetPasswordBinding
    private lateinit var utilities : Utilities
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgetPasswordBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        utilities  = Utilities(this@ForgetPasswordActivity)
        val gradientDrawable = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM,
            intArrayOf(
                Color.parseColor("#5AFF15"),
                Color.parseColor("#008000")
            )

        )
        gradientDrawable.cornerRadius = 8f;


        binding.btnSubmit.setBackground(gradientDrawable)
        binding.edLoginEmail.setOnTouchListener(View.OnTouchListener { v, event ->
            binding.rlEmail.setBackground(getDrawable(R.drawable.item_selected_border))
            binding.ivEmailLogin.setImageResource(R.drawable.ic_email_white)

            false
        })
        binding.btnSubmit.setOnClickListener{
            val email = binding.edLoginEmail.text.toString()
            if(email.equals(""))
            {
                utilities.customToastFailure("Please Enter Your Registered Email Address",this@ForgetPasswordActivity)
            }else{
                forgetPasswordApi(email)
            }
        }

    }
    private fun forgetPasswordApi(email: String) {
        val apiClient: ApiClient = ApiClient()
        val url = Constants.BASE_URL + "forgot-password/" + email
        utilities.showProgressDialog(this@ForgetPasswordActivity,"Please wait...")
        apiClient.getApiService().forgetPassword(email).enqueue(object :
            Callback<ForgotPasswordResponseModel?> {


            override fun onFailure(call: Call<ForgotPasswordResponseModel?>, t: Throwable) {
                utilities.hideProgressDialog()
                Toast.makeText(applicationContext,"Failed", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(
                call: Call<ForgotPasswordResponseModel?>,
                response: Response<ForgotPasswordResponseModel?>
            ) {
                val responseBody=response.body()!!
                utilities.hideProgressDialog()
                if (response.isSuccessful){
                    val i =Intent(this@ForgetPasswordActivity, VerificationCodeActivity::class.java)
                    i.putExtra("code",responseBody.data.verification_code)
                    i.putExtra("email",responseBody.data.email)
                    startActivity(i)
                    Toast.makeText(applicationContext,response.body()!!.message, Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(applicationContext,response.body()?.message, Toast.LENGTH_SHORT).show()
                }
            }
        })

    }

}