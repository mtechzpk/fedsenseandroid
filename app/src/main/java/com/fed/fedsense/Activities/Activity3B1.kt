package com.fed.fedsense.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.fed.fedsense.R

class Activity3B1 : AppCompatActivity() {

    lateinit var yes: CardView
    lateinit var no: CardView
    lateinit var tvYes: TextView
    lateinit var tvNo: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val window = window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_activity3_b1)

        yes = findViewById(R.id.btnYes)
//        tvYes = findViewById(R.id.tvYess)
        no = findViewById(R.id.btnNo)
        tvNo = findViewById(R.id.tvNoo)

        no.setBackgroundResource(R.drawable.bg_border_bold)




        yes.setOnClickListener {

            val intent = Intent(this, Activity3B2::class.java)
            startActivity(intent)
            finish()

        }


        no.setOnClickListener {
            onBackPressed()
//            val intent = Intent(this, FedralActivity::class.java)
//            startActivity(intent)

        }
    }
}