package com.fed.fedsense.Activities

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.fed.fedsense.util.Utilities
import com.fed.fedsense.Adapters.MessageAdapter
import com.fed.fedsense.Models.BaseResponse
import com.fed.fedsense.Models.Home.HomeResponse
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.Models.getmessage.GetMessageDataModel
import com.fed.fedsense.Models.getmessage.GetMessageResponseModel
import com.fed.fedsense.Models.payment.Card
import com.fed.fedsense.R
import com.fed.fedsense.RoomDB.CraditCard
import com.fed.fedsense.RoomDB.MyAppDataBase
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.card.MaterialCardView
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import kotlinx.android.synthetic.main.activity_activity3_b.*
import kotlinx.android.synthetic.main.fragment_job.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.*
import kotlin.concurrent.thread

class Activity3B : AppCompatActivity() {

    lateinit var question: CardView
    lateinit var video: CardView
    lateinit var add: CardView
    lateinit var ivSend: CardView
    lateinit var back: CardView
    lateinit var rv_messages: RecyclerView
    var messageAdapter: MessageAdapter? = null
    var messageDatModel: ArrayList<GetMessageDataModel> = ArrayList<GetMessageDataModel>()

    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    private lateinit var edmessage: EditText
    var sendText: String = ""
    var user_idd: String = ""
    lateinit var uri1: Uri
    lateinit var imagefile: File
    lateinit var selected_img_bitmap: Bitmap
    private var imageUploaded: Boolean = false
    private val PICK_FILE = 101
    lateinit var fileto_uploadd: File

    var isSubscribes: Boolean = false
    var aftersubscriptionMessage = ""
    var announcemnet = ""
    var date = ""
    var position = ""
    var completeMessage = ""
    var pre = ""
    var nohere = ""
    var videoLesson = ""
    var videoSubMessage = ""
    var name = ""
    private lateinit var myAppDatabase: MyAppDataBase





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val window = window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_activity3_b)


        init()
        onClikcs()


        getMessagesApi()

    }

    private fun init() {

        myAppDatabase = Room.databaseBuilder(this, MyAppDataBase::class.java, "FEDENSEDB").allowMainThreadQueries().build()
        question = findViewById(R.id.ivQuestion)
        video = findViewById(R.id.ivVideo)
        add = findViewById(R.id.ivAdd)
        back = findViewById(R.id.icBack)
        rv_messages = findViewById(R.id.rv_messages)
        edmessage = findViewById(R.id.edmessage)
        ivSend = findViewById(R.id.ivSend)
        apiClient = ApiClient()
        utilities = Utilities(this@Activity3B)
        if (!::utilities.isInitialized) utilities = Utilities(this@Activity3B)
        val gsonn = Gson()
        val jsonn: String = utilities.getString(this, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        val homeData : String= utilities.getString(this,"homedata")
        val homeobj : HomeResponse = gsonn.fromJson(homeData,HomeResponse::class.java)
        user_idd = java.lang.String.valueOf(obj.id)
        name = obj.name.toString()
        videoLesson = utilities.getString(this@Activity3B,"videoLesson")
        nohere = utilities.getString(this@Activity3B,"nohere")
        isSubscribes = homeobj.homedata[1].is_subscribed!!
        pre = utilities.getString(this@Activity3B,"pre")
        val intent = Intent()
        val froms = intent.getStringExtra("fromsub")
        if (pre.equals("pre"))
        {
            announcemnet = utilities.getString(this@Activity3B,"announce")
            date = utilities.getString(this@Activity3B,"date")
            position = utilities.getString(this@Activity3B,"pos")
            completeMessage ="Announcement Number:"+ announcemnet+System.getProperty("line.separator") +"Closing Date:"+ date+System.getProperty("line.separator")  +"Position Status:"+ position
            sendMessageApi(completeMessage)
        }else if (pre.equals("preno")){
            sendMessageApi(nohere)
        }else if (pre.equals("lesson")){
            if(name.equals(""))
            {
                videoSubMessage = "You have successfully subscribed to the video session"

            }else{
                videoSubMessage = name+" have successfully subscribed to the video session"
            }
            sendMessageApi(videoSubMessage)
        }else{
            //do nothing on else case here
        }




       // aftersubscriptionMessage = bundle.getString("final").toString()

        if (isSubscribes.equals(false))
        {
            rlchat.visibility = View.GONE
        }else{
            rlchat.visibility = View.VISIBLE
        }

    }

    private fun onClikcs() {
        question.setOnClickListener {
            meetRepresentativeVai()

        }
        back.setOnClickListener {
            utilities.saveString(this@Activity3B,"pre","")
            finish() }
        add.setOnClickListener { add() }


        video.setOnClickListener { video() }


        ivSend.setOnClickListener {
            sendText = edmessage.text.toString()
            if (!sendText.equals("")) {
                edmessage.text = Editable.Factory.getInstance().newEditable("")
                sendMessageApi(sendText)
            }
        }
        /*ivVideo.setOnClickListener {

            val gsonn = Gson()
            val jsonn: String = utilities.getString(this, "user")
            val obj: User = gsonn.fromJson(jsonn, User::class.java)
            val user_idd = java.lang.String.valueOf(obj.id)
            if (user_idd.equals("2")){

                startActivity(Intent(this@Activity3B,VideoCallingActivity::class.java)
                    .putExtra("from","videocall"))

            }else{
                Toast.makeText(this, "You are not admin", Toast.LENGTH_SHORT).show()
            }

        }*/
    }

    private fun meetRepresentativeVai() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.dialog_meet_via)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        val tvYes = dialog.findViewById<MaterialCardView>(R.id.tvYes)
        val tvNo = dialog.findViewById<MaterialCardView>(R.id.tvNo)
        close.setOnClickListener { dialog.dismiss() }
        tvYes.setOnClickListener {
            video()
            dialog.dismiss()
        }
        tvNo.setOnClickListener {
            val intent = Intent(this, Activity3B2::class.java)
            startActivity(intent)
            finish()
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun video() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.popup_3c)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {

            dialog.dismiss()

            var count =  myAppDatabase.cardDao().loadAll().size
            if (count > 0){

                var have = utilities.getString(this, "have")
                if (have.equals("true")){

                    val gsonn = Gson()
                    val jsonn: String = utilities.getStrings(this, "mycard")
                    if (!jsonn.isEmpty()){
                        val obj: Card = gsonn.fromJson(jsonn, Card::class.java)
                    }
                    if (jsonn.isEmpty()){

                        val intent = Intent(this, CardListActivity::class.java)
                        intent.putExtra("amount","60")
                        intent.putExtra("from","videolesson")
                        startActivity(intent)
                        finish()
                    }else{

                        val gsonn = Gson()
                        val jsonn: String = utilities.getStrings(this, "mycard")
                        val card: CraditCard = gsonn.fromJson(jsonn, CraditCard::class.java)

                        payAmount(card)
                        // Toast.makeText(this, "from card", Toast.LENGTH_SHORT).show()
                    }
                }else{

                    Toast.makeText(this, "Please select a card", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this@Activity3B,CardListActivity::class.java))
                }

            }else{

                addCardPopup()
            }
        }

        dialog.show()
    }

    private fun addCardPopup() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.add_card_dialog)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {

            dialog.dismiss()
            val intent  = Intent(Intent(this,AddCardActivity::class.java))
            startActivity(intent)

           }
        dialog.show()
    }

    private fun payAmount(card: CraditCard) {


        if (utilities.isConnectingToInternet(this)) {

            var expiry = card.expiryData

            val separated = expiry.split("/").toTypedArray()
            val month = separated[0]
            val year = separated[1]
            utilities.showProgressDialog(this, "Processing ...")
            apiClient.getApiService().charge_payment(
                user_idd,
                card.cardNumber,
                month,
                year,
                card.cvv,
                "45"

            )
                .enqueue(object : Callback<BaseResponse> {

                    override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                        val signupResponse = response.body()
                        utilities.hideProgressDialog()
                        if (signupResponse!!.status == true) {

                            callAttendLessionApi()
                        } else {
                            utilities.hideProgressDialog()
                            utilities.customToastFailure(signupResponse.message, this@Activity3B)

                        }
                    }
                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        utilities.hideProgressDialog()
                        utilities.customToastFailure(t.toString(), this@Activity3B)
                    }
                })
        } else {
            Toast.makeText(
                this,
                "Check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        }



    }

    private fun callAttendLessionApi() {
        val gsonn = Gson()
        val jsonn: String = utilities.getString(this@Activity3B, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        apiClient.getApiService().attendVideoLesson(user_idd,
            "video_session","45","$")
            .enqueue(object : Callback<BaseResponse> {

                override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()
                    if (signupResponse!!.status == true) {
                        //Toast.makeText(this@Activity3B4,response.message(),Toast.LENGTH_SHORT).show()
                       // utilities.customToastSuccess(response.message(),this@Activity3B)
                        ok()
                    } else {
                        utilities.customToastFailure(signupResponse.message,this@Activity3B)
                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                    // Toast.makeText(this@Activity3B4, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(),this@Activity3B)

                }


            })



    }

    private fun ok() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.popup_4c)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {

            val intent = Intent(this, Activity3B::class.java)
            utilities.saveString(this@Activity3B,"pre","lesson")
            startActivity(intent)
            dialog.dismiss()
            finish()
        }

        dialog.show()
    }

    private fun add() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.popup_3d)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val tv1 = dialog.findViewById<CardView>(R.id.tvPdf)
        tv1.setBackgroundResource(R.drawable.bg_buttonnoo)
        val tv2 = dialog.findViewById<CardView>(R.id.tvFile)
        tv2.setBackgroundResource(R.drawable.bg_buttonnoo)
        val tv3 = dialog.findViewById<CardView>(R.id.tvImage)
        tv3.setBackgroundResource(R.drawable.bg_buttonnoo)
        close.setOnClickListener { dialog.dismiss() }

        tv1.setOnClickListener {

            tv1.setBackgroundResource(R.drawable.bg_button)
            tv2.setBackgroundResource(R.drawable.bg_buttonnoo)
            tv3.setBackgroundResource(R.drawable.bg_buttonnoo)
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.type = "application/pdf"
            startActivityForResult(intent, PICK_FILE)
            dialog.dismiss()

        }

        tv2.setOnClickListener {

            tv2.setBackgroundResource(R.drawable.bg_button)
            tv1.setBackgroundResource(R.drawable.bg_buttonnoo)
            tv3.setBackgroundResource(R.drawable.bg_buttonnoo)
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.type = "application/pdf"
            startActivityForResult(intent, PICK_FILE)
            dialog.dismiss()


        }

        tv3.setOnClickListener {

            tv3.setBackgroundResource(R.drawable.bg_button)
            tv1.setBackgroundResource(R.drawable.bg_buttonnoo)
            tv2.setBackgroundResource(R.drawable.bg_buttonnoo)
            Dexter.withActivity(this@Activity3B)
                .withPermissions(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
                )
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {

                            ImagePicker.with(this@Activity3B)
                                .crop()
                                .compress(1024)
                                .maxResultSize(1080, 1080)
                                .start()
                        }
                        if (report.isAnyPermissionPermanentlyDenied) {
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: List<PermissionRequest>,
                        token: PermissionToken
                    ) {
                        token.continuePermissionRequest()
                    }
                }).check()

            dialog.dismiss()

        }

        dialog.show()
    }

    private fun getMessagesApi() {

        val gsonn = Gson()
        val jsonn: String = utilities.getString(this@Activity3B, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)
        utilities.showProgressDialog(this@Activity3B,"Loading Chat...")
        val url = Constants.BASE_URL + "get_messages/" + user_idd + "/session"
        apiClient.getApiService().getMessages(url).enqueue(object :
            Callback<GetMessageResponseModel> {

            override fun onResponse(
                call: Call<GetMessageResponseModel>, response: Response<GetMessageResponseModel>
            ) {
                val signupResponse = response.body()
                utilities.hideProgressDialog()
                if (signupResponse!!.status == true) {
                    messageDatModel = response.body()!!.data
                    val layoutManager =
                        LinearLayoutManager(this@Activity3B, LinearLayoutManager.VERTICAL, false)
                    layoutManager.stackFromEnd = true
                    layoutManager.isSmoothScrollbarEnabled = true
                    rv_messages.setLayoutManager(layoutManager)

                    messageAdapter = MessageAdapter(this@Activity3B, messageDatModel)
                    rv_messages.setAdapter(messageAdapter)
                    rv_messages.scrollToPosition(messageDatModel.size - 1)


                } else {

                    /*Toast.makeText(
                        this@Activity3B,
                        "" + signupResponse.message,
                        Toast.LENGTH_SHORT
                    ).show()*/
                    utilities.customToastFailure(signupResponse.message,this@Activity3B)

                }
            }

            override fun onFailure(call: Call<GetMessageResponseModel>, t: Throwable) {
                utilities.hideProgressDialog()
                //Toast.makeText(this@Activity3B, t.toString(), Toast.LENGTH_SHORT).show()
                utilities.customToastFailure(t.toString(),this@Activity3B)
            }


        })

    }

    private fun sendMessageApi( message : String) {
        apiClient.getApiService().sendMessageText(user_idd, "1", message, "session")
            .enqueue(object : Callback<BaseResponse> {
                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {
                    if (response.isSuccessful) {
                        val status: Boolean = response.body()!!.status
                        if (status.equals(true)) {
                            edmessage.setText("")
                            getMessagesApi()
                        } else {
                            val message: String = response.body()!!.message
                           // Toast.makeText(this@Activity3B, message, Toast.LENGTH_SHORT).show()
                            utilities.customToastFailure(message,this@Activity3B)
                            utilities.saveString(this@Activity3B,"pre","")
                        //    utilities.saveString(this@Activity3B,"videoLesson","")
                        }
                    } else {
                        val message: String = response.body()!!.message
                        //Toast.makeText(this@Activity3B, message, Toast.LENGTH_SHORT).show()
                        utilities.customToastFailure(message,this@Activity3B)
                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                   // Toast.makeText(this@Activity3B, t.message, Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.message.toString(),this@Activity3B)

                }
            })
    }

    private fun sendMessageWithImage() {
        val user_id: RequestBody = RequestBody.create(MediaType.parse("text/plain"), user_idd)
        val fromId: RequestBody = RequestBody.create(MediaType.parse("text/plain"), "1")
        val mediaType: RequestBody = RequestBody.create(MediaType.parse("text/plain"), "image")
        val messageType: RequestBody = RequestBody.create(MediaType.parse("text/plain"), "session")
        val requestBody: RequestBody = RequestBody.create(MediaType.parse("*/*"), imagefile)
        val fileToUpload =
            MultipartBody.Part.createFormData("image", imagefile.getName(), requestBody)
        utilities.showProgressDialog(this@Activity3B, "Uploading ...")
        apiClient.getApiService()
            .sendMessageWithImage(user_id, fromId, mediaType, messageType, fileToUpload)
            .enqueue(object : Callback<BaseResponse> {
                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {

                    if (response.isSuccessful) {
                        val status: Boolean = response.body()!!.status
                        if (status.equals(true)) {
                            utilities.hideProgressDialog()
                            getMessagesApi()
                        } else {
                            utilities.hideProgressDialog()
                            val message: String = response.body()!!.message
                           // Toast.makeText(this@Activity3B, message, Toast.LENGTH_SHORT).show()
                            utilities.customToastFailure(message,this@Activity3B)
                        }
                    } else {
                        utilities.hideProgressDialog()
                        val message: String = response.body()!!.message
                       // Toast.makeText(this@Activity3B, message, Toast.LENGTH_SHORT).show()
                        utilities.customToastFailure(message,this@Activity3B)

                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    //Toast.makeText(this@Activity3B, t.message, Toast.LENGTH_SHORT).show()
                    utilities.hideProgressDialog()
                    utilities.customToastFailure(t.message.toString(),this@Activity3B)

                }
            })

    }

    private fun sendMessageWithFile(file : File) {
        utilities.showProgressDialog(this@Activity3B, "Uplaoding ...")
        val user_id: RequestBody = RequestBody.create(MediaType.parse("text/plain"), user_idd)
        val fromId: RequestBody = RequestBody.create(MediaType.parse("text/plain"), "1")
        val mediaType: RequestBody = RequestBody.create(MediaType.parse("text/plain"), "file")
        val messageType: RequestBody = RequestBody.create(MediaType.parse("text/plain"), "session")
        val requestBody: RequestBody = RequestBody.create(MediaType.parse("*/*"), file)
        val fileToUpload =
            MultipartBody.Part.createFormData("file", file.getName(), requestBody)
        apiClient.getApiService()
            .sendMessageWithImage(user_id, fromId, mediaType, messageType, fileToUpload)
            .enqueue(object : Callback<BaseResponse> {
                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {

                    if (response.isSuccessful) {
                        val status: Boolean = response.body()!!.status
                        if (status.equals(true)) {
                            getMessagesApi()
                            utilities.hideProgressDialog()
                        } else {
                            val message: String = response.body()!!.message
                            Toast.makeText(this@Activity3B, message, Toast.LENGTH_SHORT).show()
                            utilities.customToastFailure(message,this@Activity3B)
                            utilities.hideProgressDialog()


                        }
                    } else {
                        val message: String = response.body()!!.message
                       // Toast.makeText(this@Activity3B, message, Toast.LENGTH_SHORT).show()
                        utilities.customToastFailure(message,this@Activity3B)
                        utilities.hideProgressDialog()


                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                  //  Toast.makeText(this@Activity3B, t.message, Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.message.toString(),this@Activity3B)
                    utilities.hideProgressDialog()


                }
            })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == PICK_FILE )
        {
            if (resultCode == Activity.RESULT_OK && data != null) {
                val uri = data.data
                if (uri != null) {

                    val fileName = getFileNameByUri(uri)
                    copyUriToExternalFilesDir(uri, fileName)

                }
            }
        }else{
            if (resultCode == Activity.RESULT_OK) {
                uri1 = data?.data!!
                selected_img_bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri1)
                //You can get File object from intent
                val file: File = ImagePicker.getFile(data)!!
                imagefile = file
                imageUploaded = true
                sendMessageWithImage()

            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                //Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
                utilities.customToastFailure(ImagePicker.getError(data),this@Activity3B)

            } else {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
                utilities.customToastFailure("Task Cancelled",this@Activity3B)
            }
        }


    }
    private fun getFileNameByUri(uri: Uri): String {
        var fileName = System.currentTimeMillis().toString()
        val cursor = contentResolver.query(uri, null, null, null, null)
        if (cursor != null && cursor.count > 0) {
            cursor.moveToFirst()
            fileName = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME))
            cursor.close()
        }
        return fileName
    }

    private fun copyUriToExternalFilesDir(uri: Uri, fileName: String) {
        thread {
            val inputStream = contentResolver.openInputStream(uri)
            val tempDir = getExternalFilesDir("temp")
            if (inputStream != null && tempDir != null) {
                fileto_uploadd = File("$tempDir/$fileName")
                val fos = FileOutputStream(fileto_uploadd)
                val bis = BufferedInputStream(inputStream)
                val bos = BufferedOutputStream(fos)
                val byteArray = ByteArray(1024)
                var bytes = bis.read(byteArray)
                while (bytes > 0) {
                    bos.write(byteArray, 0, bytes)
                    bos.flush()
                    bytes = bis.read(byteArray)
                }
                bos.close()
                fos.close()
                runOnUiThread {
//                    Toast.makeText(this, "Copy file into $tempDir succeeded.", Toast.LENGTH_LONG).show()
                    sendMessageWithFile(fileto_uploadd)
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        utilities.saveString(this@Activity3B,"pre","")
    }
}