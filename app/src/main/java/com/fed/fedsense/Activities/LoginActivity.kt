package com.fed.fedsense.Activities

import android.app.Activity
import android.app.Dialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.view.View.OnTouchListener
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.fed.fedsense.util.Utilities
import com.fed.fedsense.Models.Login.LoginReponse
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.R
import com.fed.fedsense.databinding.ActivityLoginBinding
import com.facebook.*
import com.facebook.internal.Utility.LOG_TAG
import com.facebook.login.LoginResult
import com.fed.fedsense.Notification.Notifications
import com.fed.fedsense.util.Database
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import com.ssw.linkedinmanager.dto.LinkedInAccessToken
import com.ssw.linkedinmanager.dto.LinkedInEmailAddress
import com.ssw.linkedinmanager.dto.LinkedInUserProfile
import com.ssw.linkedinmanager.events.LinkedInManagerResponse
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*
import java.util.concurrent.Executor
import com.ssw.linkedinmanager.ui.LinkedInRequestManager
import androidx.annotation.NonNull
import org.w3c.dom.Text


class LoginActivity : AppCompatActivity(), LinkedInManagerResponse {


    private lateinit var binding: ActivityLoginBinding
    private lateinit var context: Context
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    private lateinit var user: User
    lateinit var mGoogleSignInClient: GoogleSignInClient
    var RC_SIGN_IN: Int = 100
    lateinit var callbackManager: CallbackManager
    private lateinit var executor: Executor
    private lateinit var biometricPrompt: androidx.biometric.BiometricPrompt
    private lateinit var promptInfo: androidx.biometric.BiometricPrompt.PromptInfo
    var getName = ""
    var getGoogleEmail = ""
    var googleId = ""
    var googleName = ""
    var googleToken = ""
    var getPassword = ""
    var facebookid = ""
    var facebooktoken = ""
    var linkid = ""
    var linkname = ""
    var linkemail = ""
    var linkedinedinEmail: String = ""
    var linkedinedinName: String = ""
    var linkedinedinid: String = ""
    lateinit var mDatabase: FirebaseDatabase
    var database = Database()

    var first: String = ""
    var loginStatus = ""

    /*linkdin*/
    lateinit var linkedinAuthURLFull: String
    lateinit var linkedIndialog: Dialog
    lateinit var linkedinCode: String


    var id = ""
    var firstName = ""
    var lastName = ""
    var email = ""
    var profilePicURL = ""
    var accessToken = ""
    private var linkedInRequestManager: LinkedInRequestManager? = null
    var linkeDinEmail: String = ""
    var firebaseToken: String = ""
    lateinit var reference: DatabaseReference

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_login)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        linkedInRequestManager = LinkedInRequestManager(
            this@LoginActivity,
            this,
            "78nzd0cwsj1c4p",
            "sGr7nqqwZ2XZbsID",
            "http://fedsence.app.link",
            true
        )


        val sharedPref = getSharedPreferences("TOKEN", Context.MODE_PRIVATE)
        firebaseToken = sharedPref.getString("FCM_TOKEN", "").toString()
        Log.d("token", firebaseToken)


        initt()
        onClicks()



        printHashKey(this)
        // binding.edLoginEmail.setBackgroundResource(R.drawable.bg_foucs)
        //binding.edLoginPassword.setBackgroundResource(R.drawable.bg_foucs)


    }

    private fun onClicks() {
        callbackManager = CallbackManager.Factory.create();
        binding.loginButton.setReadPermissions(Arrays.asList("email", "user_gender"));

        binding.fb.setOnClickListener {
            binding.loginButton.performClick()
        }
        binding.linkdin.setOnClickListener {
            startLinkedInSignIn()

        }

        // Callback registration
        binding.loginButton.registerCallback(
            callbackManager,
            object : FacebookCallback<LoginResult?> {


                override fun onCancel() {
                    // App code
                    Log.e("blo", "cancel")

                }

                override fun onError(exception: FacebookException) {
                    Log.e("ex", "" + exception)
                    // App code
                }

                override fun onSuccess(result: LoginResult?) {
                    Log.d("name", result?.accessToken!!.userId)
                    /* Toast.makeText(
                         this@LoginActivity,
                         "" + result?.accessToken!!.userId, Toast.LENGTH_SHORT).show()*/
                    // getFbInfo()
                    loginWithFacebook(result.accessToken.userId, firebaseToken)
                    //startActivity(Intent(this@LoginActivity, SubscriptionActivity::class.java))
                }
            })
        binding.edLoginEmail.setOnTouchListener(OnTouchListener { v, event ->
            binding.rlEmail.setBackground(getDrawable(R.drawable.item_selected_border))
            binding.rlPassword.setBackground(getDrawable(R.drawable.item_selected_border_white))
            binding.ivEmailLogin.setImageResource(R.drawable.ic_email_white)
            binding.ivPassLogin.setImageResource(R.drawable.ic_pass_grey)

            false
        })
        binding.edLoginPass.setOnTouchListener(OnTouchListener { v, event ->
            binding.rlEmail.setBackground(getDrawable(R.drawable.item_selected_border_white))
            binding.rlPassword.setBackground(getDrawable(R.drawable.item_selected_border))
            binding.ivEmailLogin.setImageResource(R.drawable.ic_email_grey)
            binding.ivPassLogin.setImageResource(R.drawable.ic_pass_white)

            false
        })


        binding.tvSignUp.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }


        binding.tvForgetPass.setOnClickListener {
            val intent = Intent(
                this,
                ForgetPasswordActivity::class.java
            )
            startActivity(intent)
        }

        binding.btnLogin.setOnClickListener {

            var email: String = binding.edLoginEmail.text.toString()
            var pass: String = binding.edLoginPass.text.toString()

            if (email.isEmpty()) {

                //  Toast.makeText(applicationContext, "Email is empty", Toast.LENGTH_SHORT).show()                            utilities.customToastFailure(message,this@FedralActivity)
                utilities.customToastFailure("Email is empty", this@LoginActivity)


            } else if (!isValidEmail(email)) {

                //  Toast.makeText(applicationContext, "Invalid Email", Toast.LENGTH_SHORT).show()
                utilities.customToastFailure("Invalid Email", this@LoginActivity)


            } else if (pass.isEmpty()) {

                //Toast.makeText(applicationContext, "Password is empty", Toast.LENGTH_SHORT).show()
                utilities.customToastFailure("Password is empty", this@LoginActivity)


            } else {

                login(email, pass, firebaseToken)
            }

        }
        binding.imgFingerPrint.setOnClickListener {
            biometricPrompt.authenticate(promptInfo)
        }


        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        binding.loginWithGoogle.setOnClickListener {
            signIn()

        }

    }

    open fun startLinkedInSignIn() {
        linkedInRequestManager!!.showAuthenticateView(LinkedInRequestManager.MODE_BOTH_OPTIONS)

        //LinkedInRequestManager.MODE_BOTH_OPTIONS - can get email and user profile data with user profile image
        //LinkedInRequestManager.MODE_EMAIL_ADDRESS_ONLY - can get only the user profile email address
        //LinkedInRequestManager.MODE_LITE_PROFILE_ONLY - can get the user profile details with profile image
    }

    private fun signIn() {
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)

            // Signed in successfully, show authenticated UI.
            //  startActivity(Intent(this@LoginActivity, SubscriptionActivity::class.java))
            val id: String? = account.id
            val name: String? = account.displayName
            val email: String? = account.email
            // Toast.makeText(this@LoginActivity, id + name + email, Toast.LENGTH_SHORT).show()
            loginWithGoogle(id.toString(), name.toString(), email.toString(), firebaseToken)
            Log.w("mio", "signInResult:failed code=")

        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("mo", "signInResult:failed code=" + e.statusCode)
            //  updateUI(null)
        }
    }

    private fun initt() {

        context = this
        apiClient = ApiClient()
        mDatabase = FirebaseDatabase.getInstance()
        if (!::utilities.isInitialized) utilities = Utilities(this)
        first = utilities.getStrings(this@LoginActivity, "login")
        Log.d("firsttt", first)
        val gradientDrawable = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM,
            intArrayOf(
                Color.parseColor("#5AFF15"),
                Color.parseColor("#008000")
            )

        )
        gradientDrawable.cornerRadius = 8f;


        executor = ContextCompat.getMainExecutor(this)
        binding.btnLogin.setBackground(gradientDrawable)
        biometricPrompt = androidx.biometric.BiometricPrompt(
            this@LoginActivity,
            executor,
            object : androidx.biometric.BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationSucceeded(result: androidx.biometric.BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    getName = utilities.getStrings(this@LoginActivity, "emails")
                    getPassword = utilities.getStrings(this@LoginActivity, "passwords")
                    getGoogleEmail = utilities.getStrings(this@LoginActivity, "goolgeemail")
                    googleName = utilities.getStrings(this@LoginActivity, "googlename")
                    googleId = utilities.getStrings(this@LoginActivity, "googleid")
                    googleToken = utilities.getStrings(this@LoginActivity, "googletoken")
                    facebookid = utilities.getStrings(this@LoginActivity, "facebookid")
                    facebooktoken = utilities.getStrings(this@LoginActivity, "facebooktoken")
                    linkid = utilities.getStrings(this@LoginActivity, "linkid")
                    linkname = utilities.getStrings(this@LoginActivity, "linkname")
                    linkemail = utilities.getStrings(this@LoginActivity, "linkemail")
                    if (!getName.equals("")) {
                        if (!getPassword.equals("")) {
                            // Toast.makeText(this@LoginActivity,"You Must Login First",Toast.LENGTH_SHORT).show()
                            login(getName, getPassword, firebaseToken)
                        }

                    }
                    if (!getGoogleEmail.equals("")) {
                        loginWithGoogle(googleId, googleName, getGoogleEmail, googleToken)
                    }
                    if (!facebookid.equals("")) {
                        loginWithFacebook(facebookid, facebooktoken)
                    }
                    if (!linkid.equals(""))
                    {
                        loginWithLinkedin(linkid,linkname,linkemail)
                    }
                }

                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    //  Toast.makeText(this@LoginActivity,"Auth Error",Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure("Auth Error", this@LoginActivity)


                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    //  Toast.makeText(this@LoginActivity,"Auth Failed",Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure("Auth Failed", this@LoginActivity)


                }
            })

        promptInfo = androidx.biometric.BiometricPrompt.PromptInfo.Builder()
            .setTitle("Biometric login for my app")
            .setSubtitle("Log in using your biometric credential")
            .setNegativeButtonText("Use account password")
            .build()


    }

    private fun login(strEmail: String, strPass: String, token: String) {
        if (utilities.isConnectingToInternet(this@LoginActivity)) {
            utilities.showProgressDialog(context, "Loading ...")
            apiClient.getApiService().login(strEmail, strPass, token)
                .enqueue(object : Callback<LoginReponse> {

                    override fun onFailure(call: Call<LoginReponse>, t: Throwable) {

                        utilities.hideProgressDialog()
                        // Toast.makeText(applicationContext, t.toString(), Toast.LENGTH_SHORT).show()
                        utilities.customToastFailure(t.toString(), this@LoginActivity)

                    }

                    override fun onResponse(
                        call: Call<LoginReponse>,
                        response: Response<LoginReponse>
                    ) {
                        val signupResponse = response.body()
                        utilities.hideProgressDialog()
                        if (signupResponse!!.status == true) {

                            user = response.body()!!.user
                            val id = response.body()!!.user.id
                            val gson = Gson()
                            val json = gson.toJson(user)
                            utilities.saveString(context, "user", json)
                            utilities.saveString(context, "userId", id.toString())
                            utilities.saveString(context, Constants.LOGIN_STATUS, "true")
                            if (first.equals("yes")) {
                                utilities.saveStrings(this@LoginActivity, "login", "")
                            } else {
                                utilities.saveStrings(this@LoginActivity, "login", "yes")
                            }
                            FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    val token = task.result
                                    val messagesNode = "device_token"
                                    val messageNode = mDatabase.reference.child(database.TOKENS)
                                        .child(messagesNode).push().key
                                    /*mDatabase.reference.child(database.TOKENS).child(id.toString())
                                        .child("device_token").setValue(token)*/
                                    mDatabase.reference.child(database.TOKENS).child(id.toString())
                                        .child("device_token").setValue(token)
//                                FirebaseDatabase.getInstance().getReference("tokens")
//                                    .child(id.toString()).setValue(token)
                                    Notifications.createNotificationChannel(this@LoginActivity)
                                }
                            }
                            /* Toast.makeText(
                                 applicationContext,
                                 "" + signupResponse.message,
                                 Toast.LENGTH_SHORT
                             ).show()*/
                            // utilities.customToastFailure(signupResponse.message,this@LoginActivity)
                            utilities.saveStrings(this@LoginActivity, "emails", "")
                            utilities.saveStrings(this@LoginActivity, "googlename", "")
                            utilities.saveStrings(this@LoginActivity, "googleid", "")
                            utilities.saveStrings(this@LoginActivity, "goolgeemail", "")
                            utilities.saveStrings(this@LoginActivity, "googletoken", "")
                            utilities.saveStrings(this@LoginActivity, "facebookid", "")
                            utilities.saveStrings(this@LoginActivity, "facebooktoken", "")
                            utilities.saveStrings(this@LoginActivity, "linkid", "")
                            utilities.saveStrings(this@LoginActivity, "linkname", "")
                            utilities.saveStrings(this@LoginActivity, "linkemail", "")
                            val email: String = binding.edLoginEmail.text.toString()
                            val pass: String = binding.edLoginPass.text.toString()
                            if (getName.equals("")) {
                                if (getPassword.equals("")) {
                                    utilities.saveStrings(this@LoginActivity, "emails", email)
                                    utilities.saveStrings(this@LoginActivity, "passwords", pass)
                                }
                            } else if (!email.equals("")) {
                                if (!pass.equals("")) {
                                    utilities.saveStrings(this@LoginActivity, "emails", email)
                                    utilities.saveStrings(this@LoginActivity, "passwords", pass)
                                }

                            } else {
                                utilities.saveStrings(this@LoginActivity, "emails", getName)
                                utilities.saveStrings(this@LoginActivity, "passwords", getPassword)
                            }


                            val name: String = user.name!!
                            if (name.equals("")) {
                                utilities.saveString(this@LoginActivity, "firstTime", "firstTime")
                                startActivity(
                                    Intent(
                                        this@LoginActivity,
                                        ProfileActivity::class.java
                                    ).addFlags(
                                        Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    )
                                )
                                finish()

                            } else {
                                startActivity(
                                    Intent(this@LoginActivity, MainActivity::class.java).addFlags(
                                        Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    )
                                )
                                finish()

                            }


                        } else {

                            /* Toast.makeText(
                                 applicationContext,
                                 "" + signupResponse.message,
                                 Toast.LENGTH_SHORT
                             ).show()*/
                            utilities.customToastFailure(signupResponse.message, this@LoginActivity)


                        }
                    }
                })
        } else {
            Toast.makeText(
                this@LoginActivity,
                "Check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        }

    }

    private fun loginWithFacebook(userId: String, token: String) {

        utilities.showProgressDialog(context, "Loading ...")
        apiClient.getApiService().loginWithFacebook(userId, token)
            .enqueue(object : Callback<LoginReponse> {

                override fun onFailure(call: Call<LoginReponse>, t: Throwable) {

                    utilities.hideProgressDialog()
                    Toast.makeText(applicationContext, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(), this@LoginActivity)

                }

                override fun onResponse(
                    call: Call<LoginReponse>,
                    response: Response<LoginReponse>
                ) {
                    val signupResponse = response.body()
                    utilities.hideProgressDialog()
                    if (signupResponse!!.status == true) {

                        user = response.body()!!.user
                        val id = response.body()!!.user.id
                        val gson = Gson()
                        val json = gson.toJson(user)
                        utilities.saveString(context, "user", json)
                        utilities.saveString(context, "userId", id.toString())
                        utilities.saveString(context, Constants.LOGIN_STATUS, "true")
                        utilities.saveString(context, "loginfacebook", "facebook")
                        if (first.equals("yes")) {
                            utilities.saveStrings(this@LoginActivity, "login", "no")
                        } else {
                            utilities.saveStrings(this@LoginActivity, "login", "yes")
                        }

                        utilities.saveStrings(this@LoginActivity, "facebookid", userId)
                        utilities.saveStrings(this@LoginActivity, "facebooktoken", token)
                        utilities.saveStrings(this@LoginActivity, "goolgeemail", "")
                        utilities.saveStrings(this@LoginActivity, "googlename", "")
                        utilities.saveStrings(this@LoginActivity, "googleid", "")
                        utilities.saveStrings(this@LoginActivity, "googletoken", "")
                        utilities.saveStrings(this@LoginActivity, "emails", "")
                        utilities.saveStrings(this@LoginActivity, "passwords", "")
                        utilities.saveStrings(this@LoginActivity, "linkid", "")
                        utilities.saveStrings(this@LoginActivity, "linkname", "")
                        utilities.saveStrings(this@LoginActivity, "linkemail", "")
                        /*Toast.makeText(
                            applicationContext,
                            "" + signupResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()*/

                        val name: String = user.name!!
                        if (name.equals("")) {
                            utilities.saveString(this@LoginActivity, "firstTime", "firstTime")
                            startActivity(
                                Intent(this@LoginActivity, ProfileActivity::class.java).addFlags(
                                    Intent.FLAG_ACTIVITY_CLEAR_TOP
                                )
                            )
                            finish()

                        } else {
                            startActivity(
                                Intent(this@LoginActivity, MainActivity::class.java).addFlags(
                                    Intent.FLAG_ACTIVITY_CLEAR_TOP
                                )
                            )
                            finish()

                        }


                    } else {

                        /* Toast.makeText(
                             applicationContext,
                             "" + signupResponse.message,
                             Toast.LENGTH_SHORT
                         ).show()*/
                        utilities.customToastFailure(signupResponse.message, this@LoginActivity)


                    }
                }
            })

    }

    private fun loginWithGoogle(googleId: String, name: String, email: String, token: String) {

        utilities.showProgressDialog(context, "Loading ...")
        apiClient.getApiService().loginWithGoogle(googleId, name, email, token)
            .enqueue(object : Callback<LoginReponse> {

                override fun onFailure(call: Call<LoginReponse>, t: Throwable) {

                    utilities.hideProgressDialog()
                    Toast.makeText(applicationContext, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(), this@LoginActivity)

                }

                override fun onResponse(
                    call: Call<LoginReponse>,
                    response: Response<LoginReponse>
                ) {
                    val signupResponse = response.body()
                    utilities.hideProgressDialog()
                    if (signupResponse!!.status == true) {

                        user = response.body()!!.user
                        val id = response.body()!!.user.id
                        val gson = Gson()
                        val json = gson.toJson(user)
                        utilities.saveString(context, "user", json)
                        utilities.saveString(context, "userId", id.toString())
                        utilities.saveString(context, Constants.LOGIN_STATUS, "true")
                        utilities.saveString(context, "logingoogle", "google")
                        if (first.equals("yes")) {
                            utilities.saveStrings(this@LoginActivity, "login", "no")
                        } else {
                            utilities.saveStrings(this@LoginActivity, "login", "yes")
                        }

                        /*Toast.makeText(
                            applicationContext,
                            "" + signupResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()*/
                        val name: String = user.name!!
                        utilities.saveStrings(this@LoginActivity, "goolgeemail", email)
                        utilities.saveStrings(this@LoginActivity, "googlename", name)
                        utilities.saveStrings(this@LoginActivity, "googleid", googleId)
                        utilities.saveStrings(this@LoginActivity, "googletoken", token)
                        utilities.saveStrings(this@LoginActivity, "emails", "")
                        utilities.saveStrings(this@LoginActivity, "passwords", "")
                        utilities.saveStrings(this@LoginActivity, "facebookid", "")
                        utilities.saveStrings(this@LoginActivity, "facebooktoken", "")
                        utilities.saveStrings(this@LoginActivity, "linkid", "")
                        utilities.saveStrings(this@LoginActivity, "linkname", "")
                        utilities.saveStrings(this@LoginActivity, "linkemail", "")
                        if (name.equals("")) {
                            utilities.saveString(this@LoginActivity, "firstTime", "firstTime")
                            startActivity(
                                Intent(this@LoginActivity, ProfileActivity::class.java).addFlags(
                                    Intent.FLAG_ACTIVITY_CLEAR_TOP
                                )
                            )
                            finish()

                        } else {
                            startActivity(
                                Intent(this@LoginActivity, MainActivity::class.java).addFlags(
                                    Intent.FLAG_ACTIVITY_CLEAR_TOP
                                )
                            )
                            finish()

                        }
                    } else {

                        /*Toast.makeText(
                            applicationContext,
                            "" + signupResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()*/
                        utilities.customToastFailure(signupResponse.message, this@LoginActivity)


                    }
                }
            })

    }

    private fun loginWithLinkedin(linkedinId: String, name: String, email: String) {

        utilities.showProgressDialog(context, "Loading ...")
        apiClient.getApiService().loginWithLinkedin(linkedinId, name, email)
            .enqueue(object : Callback<LoginReponse> {

                override fun onFailure(call: Call<LoginReponse>, t: Throwable) {

                    utilities.hideProgressDialog()
                    // Toast.makeText(applicationContext, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(), this@LoginActivity)

                }

                override fun onResponse(
                    call: Call<LoginReponse>,
                    response: Response<LoginReponse>
                ) {
                    val signupResponse = response.body()
                    utilities.hideProgressDialog()
                    if (signupResponse!!.status == true) {

                        user = response.body()!!.user
                        val id = response.body()!!.user.id
                        val gson = Gson()
                        val json = gson.toJson(user)
                        utilities.saveString(context, "user", json)
                        utilities.saveString(context, "userId", id.toString())
                        utilities.saveString(context, Constants.LOGIN_STATUS, "true")
                        utilities.saveString(context, "loginLinkedin", "linkedin")

                        /*Toast.makeText(
                            applicationContext,
                            "" + signupResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()*/
                        utilities.saveStrings(this@LoginActivity, "facebookid", "")
                        utilities.saveStrings(this@LoginActivity, "facebooktoken", "")
                        utilities.saveStrings(this@LoginActivity, "goolgeemail", "")
                        utilities.saveStrings(this@LoginActivity, "googlename", "")
                        utilities.saveStrings(this@LoginActivity, "googleid", "")
                        utilities.saveStrings(this@LoginActivity, "googletoken", "")
                        utilities.saveStrings(this@LoginActivity, "emails", "")
                        utilities.saveStrings(this@LoginActivity, "passwords", "")
                        utilities.saveStrings(this@LoginActivity, "linkid", linkedinId)
                        utilities.saveStrings(this@LoginActivity, "linkname", name)
                        utilities.saveStrings(this@LoginActivity, "linkemail", email)
                        val name: String = user.name!!
                        if (name.equals("")) {
                            utilities.saveString(this@LoginActivity, "firstTime", "firstTime")
                            startActivity(
                                Intent(this@LoginActivity, ProfileActivity::class.java).addFlags(
                                    Intent.FLAG_ACTIVITY_CLEAR_TOP
                                )
                            )
                            finish()

                        } else {
                            startActivity(
                                Intent(this@LoginActivity, MainActivity::class.java).addFlags(
                                    Intent.FLAG_ACTIVITY_CLEAR_TOP
                                )
                            )
                            finish()

                        }

                    } else {

                        /* Toast.makeText(
                             applicationContext,
                             "" + signupResponse.message,
                             Toast.LENGTH_SHORT
                         ).show()*/
                        utilities.customToastFailure(signupResponse.message, this@LoginActivity)


                    }
                }
            })

    }

    fun isValidEmail(target: CharSequence?): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

    fun printHashKey(Context: Context) {
        try {
            val info = Context.packageManager.getPackageInfo(
                Context.packageName,
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey: String = String(Base64.encode(md.digest(), 0))
                Log.i("ContentValuesTAG", "printHashKey() Hash Key: $hashKey")
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e("ContentValues.TAG", "printHashKey()", e)
        } catch (e: Exception) {
            Log.e(ContentValues.TAG, "printHashKey()", e)
        }
    }

    private fun getFbInfo() {
        val request = GraphRequest.newMeRequest(
            AccessToken.getCurrentAccessToken(),
            object : GraphRequest.GraphJSONObjectCallback {
                override fun onCompleted(
                    obj: JSONObject?,
                    response: GraphResponse?
                ) {
                    try {
                        Log.d(LOG_TAG, "fb json object: $obj")
                        Log.d(LOG_TAG, "fb graph response: $response")
                        val id = obj!!.getString("id")
                        val first_name = obj.getString("first_name")
                        val last_name = obj.getString("last_name")
                        val email: String
                        if (obj.has("email")) {
                            email = obj.getString("email")
                            Toast.makeText(this@LoginActivity, email, Toast.LENGTH_SHORT).show()

                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            })
        val parameters = Bundle()
        parameters.putString(
            "fields",
            "id,first_name,last_name,email,gender,birthday"
        ) // id,first_name,last_name,email,gender,birthday,cover,picture.type(large)
        request.parameters = parameters
        request.executeAsync()
    }

    override fun onGetAccessTokenFailed() {
    }

    override fun onGetAccessTokenSuccess(linkedInAccessToken: LinkedInAccessToken?) {
    }

    override fun onGetCodeFailed() {
    }

    override fun onGetCodeSuccess(code: String?) {
    }

    override fun onGetProfileDataFailed() {
    }

    override fun onGetProfileDataSuccess(linkedInUserProfile: LinkedInUserProfile?) {
        /*tvLastName.setText("Last Name : " + linkedInUserProfile!!.userName.lastName.localized.en_US)


        Glide
            .with(context)
            .load(linkedInUserProfile!!.imageURL)
            .into(ivImage)*/
        linkedInUserProfile?.userName?.id
        var userNam = linkedInUserProfile?.userName?.firstName?.localized?.en_US
        var userId: String = linkedInUserProfile?.userName?.id.toString()

        linkedinedinName = linkedInUserProfile?.userName?.firstName?.localized?.en_US.toString()
        linkedinedinid = linkedInUserProfile?.userName?.id.toString()
        if (linkedinedinName.equals("")) {
            //nothing should be one
        } else if (linkedinedinid.equals("")) {
            //something to right the code here
        } else {
            loginWithLinkedin(linkedinedinid, linkedinedinName, linkeDinEmail)
        }


        /*Toast.makeText(this@LoginActivity,userNam,Toast.LENGTH_SHORT).show()
        Toast.makeText(this@LoginActivity,userId,Toast.LENGTH_SHORT).show()
        Toast.makeText(this@LoginActivity,linkeDinEmail,Toast.LENGTH_SHORT).show()*/
        linkedInRequestManager!!.dismissAuthenticateView()
    }

    override fun onGetEmailAddressFailed() {
    }

    override fun onGetEmailAddressSuccess(linkedInEmailAddress: LinkedInEmailAddress?) {
        linkeDinEmail = linkedInEmailAddress?.emailAddress.toString()
        //  Toast.makeText(this@LoginActivity,linkedInEmailAddress?.emailAddress,Toast.LENGTH_SHORT).show()
        linkedInRequestManager!!.dismissAuthenticateView()
    }


}
//adding comment
