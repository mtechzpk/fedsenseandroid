package com.fed.fedsense.Activities

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.VideoView
import com.fed.fedsense.util.Utilities
import com.fed.fedsense.R
import com.mtechsoft.compassapp.networking.Constants

class SplashScreenActivity : AppCompatActivity() {

    private val SPLASH_TIME_OUT = 2000L
    private lateinit var utilities: Utilities
    var videoView: VideoView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)


        if (!::utilities.isInitialized) utilities = Utilities(this)

        videoView = findViewById(R.id.videoview)
        val videoUri = Uri.parse("android.resource://" + packageName + "/" + R.raw.fedsence)
        videoView!!.setVideoURI(videoUri)
         videoView!!.setOnCompletionListener {
             Handler().postDelayed(
                 {
                     val logged_in = utilities.getString(this, Constants.LOGIN_STATUS)
                     if (logged_in.equals("true")){

                         val i = Intent(this, MainActivity::class.java)
                         startActivity(i)
                         finish()

                     }else{

                         val i = Intent(this, LoginActivity::class.java)
                         startActivity(i)
                         finish()
                     }

                 }, SPLASH_TIME_OUT)
         }
         videoView!!.start()
        /*Handler().postDelayed(
            {
                val logged_in = utilities.getString(this, Constants.LOGIN_STATUS)
                if (logged_in.equals("true")){

                    val i = Intent(this, MainActivity::class.java)
                    startActivity(i)
                    finish()

                }else{

                    val i = Intent(this, LoginActivity::class.java)
                    startActivity(i)
                    finish()
                }

            }, SPLASH_TIME_OUT)*/

    }
}