package com.fed.fedsense.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.fed.fedsense.R
import com.fed.fedsense.databinding.ActivityVerificationCodeBinding
import com.fed.fedsense.util.Utilities

class VerificationCodeActivity : AppCompatActivity() {
    private lateinit var binding : ActivityVerificationCodeBinding
    var code : String?= null
    var email : String? = null
    var userCode : String? = null
    private lateinit var utilities: Utilities
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityVerificationCodeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        utilities = Utilities(this@VerificationCodeActivity)

        val intent = intent
        code = intent.getStringExtra("code")
        email= intent.getStringExtra("email")
        binding.btnSubmit.setOnClickListener {
            userCode = binding.pinView.text.toString()
            if (userCode.isNullOrEmpty()){
                Toast.makeText(applicationContext, "Please enter the code", Toast.LENGTH_SHORT).show()
            }else if (!userCode.equals(code)){
                Toast.makeText(applicationContext, "Invalid Code", Toast.LENGTH_SHORT).show()

            }else{
                val i = Intent(this, CreatePasswordActivity::class.java)
                i.putExtra("email",email)
                startActivity(i)

            }

        }

    }
}