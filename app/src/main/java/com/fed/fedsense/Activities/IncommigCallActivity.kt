package com.fed.fedsense.Activities

import android.app.NotificationManager
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fed.fedsense.R
import android.media.RingtoneManager

import android.media.Ringtone
import android.net.Uri
import com.fed.fedsense.util.RingtonePlayingService
import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.mtechsoft.compassapp.networking.Constants


class IncommigCallActivity : AppCompatActivity() {

    var token = ""
    var channel = ""
    var action = ""
    lateinit var r: Ringtone

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_incommig_call)

         action = intent.getStringExtra("action").toString()
         token = intent.getStringExtra("token").toString()
         channel = intent.getStringExtra("channel").toString()

        token = Constants.token
        channel = Constants.channel
        Log.d("creds", action+"\n"+Constants.token+"\n"+Constants.channel)

         startService(Intent(this, RingtonePlayingService::class.java))




//        val notification: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
//        r = RingtoneManager.getRingtone(applicationContext, notification)
//        r.play()

    }

    fun joinCall(view: android.view.View) {

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
        stopService(Intent(this, RingtonePlayingService::class.java))

//        r.stop()


        startActivity(
            Intent(this, VideoCallingActivity::class.java)
            .putExtra("from","inCommingCall")
            .putExtra("token",token)
            .putExtra("channel",channel))

        finish()
    }

    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {

            stopService(Intent(this@IncommigCallActivity, RingtonePlayingService::class.java))
            finish()

//            awaitingDriverPhone.setText(intent.extras!!.getString("phone")) //setting values to the TextViews
//            awaitingDriverLat.setText(intent.extras!!.getDouble("lat"))
//            awaitingDriverLng.setText(intent.extras!!.getDouble("lng"))
        }
    }

    override fun onStart() {
        super.onStart()

        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver),
            IntentFilter("MissedCall"))
    }

    override fun onStop() {
        super.onStop()

        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(mMessageReceiver)
    }
}