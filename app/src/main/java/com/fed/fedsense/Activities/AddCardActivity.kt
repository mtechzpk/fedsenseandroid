package com.fed.fedsense.Activities

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View.OnFocusChangeListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.fed.fedsense.Models.BaseResponse
import com.fed.fedsense.R
import com.fed.fedsense.RoomDB.CraditCard
import com.fed.fedsense.RoomDB.MyAppDataBase
import com.fed.fedsense.util.Utilities
import com.mtechsoft.compassapp.services.ApiClient
import kotlinx.android.synthetic.main.activity_add_card.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddCardActivity : AppCompatActivity() {

    private var number = ""
    private var name = ""
    private var expiry = ""
    private var cvv = ""
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    private lateinit var myAppDatabase: MyAppDataBase


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_card)

        initt()




        etCardHolderName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                name = s.toString()
                crediCardView.setCardData(name, number, cvv, expiry)
            }

            override fun afterTextChanged(s: Editable) {}
        })

        etMaskedCardNumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.length > 0) {
                    etTemp.setHint("")
                } else {
                    etTemp.setHint("Card Number")
                }
                number = etMaskedCardNumber.getRawText()
                crediCardView.setCardData(name, number, cvv, expiry)
            }

            override fun afterTextChanged(s: Editable) {}
        })
        etExpiryDate.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.length > 0) {
                    etTemp2.hint = ""
                } else {
                    etTemp2.setHint("Expiry Date")
                }
                expiry = etExpiryDate.text.toString()
                crediCardView.setCardData(name, number, cvv, etExpiryDate.rawText)
            }

            override fun afterTextChanged(s: Editable) {}
        })

        etSecurityCode.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                cvv = s.toString()
                crediCardView.setCardData(name, number, cvv, expiry)
            }

            override fun afterTextChanged(s: Editable) {}
        })


        etSecurityCode.onFocusChangeListener = OnFocusChangeListener { v, hasFocus ->
            crediCardView.flip()
        }


        icBack.setOnClickListener{
            finish()
        }

        tvOk.setOnClickListener {
            if (number.isEmpty())
            {
                Toast.makeText(this@AddCardActivity,"Please Enter Card Number",Toast.LENGTH_SHORT).show()
            }else if (number.length<16)
            {
                Toast.makeText(this@AddCardActivity,"Please Enter A Valid Card Number",Toast.LENGTH_SHORT).show()
            }else if (name.isEmpty())
            {
                Toast.makeText(this@AddCardActivity, "Please Enter Name On Card", Toast.LENGTH_SHORT).show()
            }else if (expiry.isEmpty())
            {
                Toast.makeText(this@AddCardActivity, "Please Enter Card Expiry", Toast.LENGTH_SHORT).show()
            }else if (cvv.isEmpty())
            {
                Toast.makeText(this@AddCardActivity, "Please Enter Card CVV", Toast.LENGTH_SHORT).show()
            }else if (cvv.length<3)
            {
                Toast.makeText(this@AddCardActivity, "Please Enter Valid CVV", Toast.LENGTH_SHORT).show()
            }else{
                val id = (0..1000000).random()
                val card = CraditCard(id,number,name,expiry,cvv,"false")
                saveCard(card)
            }
//          addCardOnServer()
        }
    }

    private fun saveCard(card: CraditCard) {
        myAppDatabase.cardDao().insertStudent(card)
        Toast.makeText(this, "Card Added", Toast.LENGTH_SHORT).show()
        finish()
    }

    private fun initt() {

        apiClient = ApiClient()
        utilities = Utilities(this)
        if (!::utilities.isInitialized) utilities = Utilities(this)
        myAppDatabase = Room.databaseBuilder(this, MyAppDataBase::class.java, "FEDENSEDB").allowMainThreadQueries().build()

    }

    private fun addCardOnServer() {

        if (number.isEmpty()) {
            Toast.makeText(this@AddCardActivity, "Enter Card Number", Toast.LENGTH_SHORT)
                .show()
        } else if (name.isEmpty()) {
            Toast.makeText(
                this@AddCardActivity,
                "Enter Card Holder Name",
                Toast.LENGTH_SHORT
            ).show()
        } else if (expiry.isEmpty()) {
            Toast.makeText(this@AddCardActivity, "Enter Expiry Date", Toast.LENGTH_SHORT)
                .show()
        } else if (cvv.isEmpty()) {
            Toast.makeText(this@AddCardActivity, "Enter CVV", Toast.LENGTH_SHORT)
                .show()
        }else{

            callApi()
        }
    }

    private fun callApi() {

//        val gsonn = Gson()
//
//        val jsonn: String = utilities.getString(this@VideoCallingActivity, "user")
//        val obj: User = gsonn.fromJson(jsonn, User::class.java)
//        var user_idd: String = java.lang.String.valueOf(obj.id)

        utilities.showProgressDialog(this,"Saving")
        apiClient.getApiService().save_card(
            "2",
            name,
            number,
            expiry,
            cvv
        )
            .enqueue(object : Callback<BaseResponse> {
                override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                    utilities.hideProgressDialog()

                    val signupResponse = response.body()
                    val data = response.body()
                    if (signupResponse!!.status == true) {

                        Toast.makeText(this@AddCardActivity, signupResponse.message, Toast.LENGTH_SHORT).show()
                        finish()

                    } else {

                        utilities.customToastFailure(signupResponse.message,this@AddCardActivity)

                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    utilities.hideProgressDialog()
                    utilities.customToastFailure(t.toString(),this@AddCardActivity)
                }


            })
    }

}