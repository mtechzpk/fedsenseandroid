package com.fed.fedsense.Activities

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ViewGroup
import android.view.WindowManager
import android.view.WindowManager.LayoutParams.FLAG_SECURE
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import com.fed.fedsense.util.Utilities
import com.fed.fedsense.Models.BaseResponse
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.R
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.gson.Gson
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import kotlinx.android.synthetic.main.activity_video_detail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VideoDetailActivity : AppCompatActivity() {
    lateinit var next: CardView
    lateinit var videoTitle: TextView
    lateinit var likecounter: TextView
    lateinit var dislikecounter: TextView
    lateinit var desc: TextView
    lateinit var video_rel: RelativeLayout
    lateinit var like: ImageView
    lateinit var dislike: ImageView
    lateinit var ivBack: ImageView
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    private var player: SimpleExoPlayer? = null
    private var playerView: PlayerView? = null

    var videoUrl: String = ""
    var isLike: Boolean = false
    var isDislike: Boolean = false
    var videoName: String = ""
    var totalLike: String = ""
    var totalDislike: String = ""
    var videoDesc: String = ""
    var videoId: String = ""
    var oneIncre: Int = 1
    var getNoLike: String = ""
    var getNoDisLike: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFlags(
            FLAG_SECURE,
            FLAG_SECURE
        )
        setContentView(R.layout.activity_video_detail)


        val bundle = intent.extras
        videoUrl = bundle?.getString("videourl").toString()
        isLike = bundle?.getBoolean("islike")!!
        isDislike = bundle.getBoolean("isdislike")
        videoName = bundle.getString("videoname").toString()
        totalLike = bundle.getString("totallike").toString()
        totalDislike = bundle.getString("totaldislike").toString()
        videoDesc = bundle.getString("desc").toString()
        videoId = bundle.getString("id").toString()
        initt()

    }

    private fun initt() {

        videoTitle = findViewById(R.id.videoTitle)
        playerView = findViewById(R.id.video_view)
        video_rel = findViewById(R.id.video_rel)
        like = findViewById(R.id.like)
        dislike = findViewById(R.id.dislike)
        likecounter = findViewById(R.id.likecounter)
        dislikecounter = findViewById(R.id.dislikecounter)
        desc = findViewById(R.id.tvDesc)
        ivBack = findViewById(R.id.ivBack)
        /*imgPause.setOnClickListener {
            player?.setPlayWhenReady(false);
            player?.getPlaybackState();
        }*/

        // videoTitle.setText(intent.getStringExtra("title"))
        //  description.setText(intent.getStringExtra("description"))
        apiClient = ApiClient()
        utilities = Utilities(this)
        if (!::utilities.isInitialized) utilities = Utilities(this)


        videoTitle.text = videoName
        desc.text = videoDesc
        likecounter.text = totalLike
        dislikecounter.text = totalDislike
        if (isLike.equals(true)) {
            like.setImageDrawable(resources.getDrawable(R.drawable.likegreen))

        } else {
            like.setImageDrawable(resources.getDrawable(R.drawable.like))

        }
        if (isDislike.equals(true)) {
            dislike.setImageDrawable(resources.getDrawable(R.drawable.dislikenew))

        } else {
            dislike.setImageDrawable(resources.getDrawable(R.drawable.dislike))

        }
        if (!videoUrl.equals("")) {
            setVideo()
        }
        ivBack.setOnClickListener {
            onBackPressed()
        }
        like.setOnClickListener {
            getNoLike = likecounter.text.toString()
            getNoDisLike = dislikecounter.text.toString()

            if (isLike.equals(true)) {
                //do nothing
            } else {


                likeVideoApi()
                like.setImageDrawable(resources.getDrawable(R.drawable.likegreen))
                dislike.setImageDrawable(resources.getDrawable(R.drawable.dislike))
                getNoLike = (getNoLike.toInt() + 1).toString()
                likecounter.text = getNoLike
                isLike = true
                isDislike = false
                if (!getNoDisLike.equals("0")) {
                    getNoDisLike = (getNoDisLike.toInt() - 1).toString()
                    dislikecounter.text = getNoDisLike
                }

            }
        }
        dislike.setOnClickListener {
            getNoLike = likecounter.text.toString()
            getNoDisLike = dislikecounter.text.toString()
            if (isDislike.equals(true)) {
                //do nothing
            } else {
                dislikeVideoApi()
                like.setImageDrawable(resources.getDrawable(R.drawable.like))
                dislike.setImageDrawable(resources.getDrawable(R.drawable.dislikenew))
                getNoDisLike = (getNoDisLike.toInt() + 1).toString()
                dislikecounter.text = getNoDisLike
                isDislike = true
                isLike = false
                if (!getNoLike.equals("0")) {
                    getNoLike = (getNoLike.toInt() - 1).toString()
                    likecounter.text = getNoLike
                }

            }
        }
        img_fullScreen.setOnClickListener {
            val intent: Intent =
                Intent(this@VideoDetailActivity, FullScreenVideoActivity::class.java)
            utilities.saveString(
                this@VideoDetailActivity,
                "videourl",
                Constants.BASE_URL_IMG + videoUrl
            )
            startActivity(intent)
        }
    }

    fun setVideo() {

        val appNameStringRes = R.string.app_name
        val trackSelectorDef: TrackSelector = DefaultTrackSelector(this)
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelectorDef)
        val userAgent = Util.getUserAgent(this, this.getString(appNameStringRes))
        val defaultDataSourceFactory = DefaultDataSourceFactory(this, userAgent)
        val uriOfContentUrl = Uri.parse(Constants.BASE_URL_IMG + videoUrl)
        val mediaSource: MediaSource = ProgressiveMediaSource.Factory(defaultDataSourceFactory)
            .createMediaSource(uriOfContentUrl)
        //MediaSource mediaSource = new ExtractorMediaSource(uriOfContentUrl, new CacheDataSourceFactory(context, 100 * 1024 * 1024, 500 * 1024 * 1024), new DefaultExtractorsFactory(), null, null);
        //MediaSource mediaSource = new ExtractorMediaSource(uriOfContentUrl, new CacheDataSourceFactory(context, 100 * 1024 * 1024, 500 * 1024 * 1024), new DefaultExtractorsFactory(), null, null);
        player!!.prepare(mediaSource)
        player!!.setPlayWhenReady(false)
        playerView!!.requestFocus()
        playerView!!.player = player
        playerView!!.setShowBuffering(PlayerView.SHOW_BUFFERING_WHEN_PLAYING)
        player!!.setVolume(1f)

    }

    override fun onDestroy() {
        super.onDestroy()

        player!!.stop()
        player!!.release()
    }

    private fun likeVideoApi() {

        val gsonn = Gson()
        val jsonn: String = utilities.getString(this@VideoDetailActivity, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        val user_idd: String = java.lang.String.valueOf(obj.id)
        utilities.showProgressDialog(this@VideoDetailActivity, "Loading ...")

        apiClient.getApiService().likeVideo(user_idd, videoId)
            .enqueue(object : Callback<BaseResponse> {

                override fun onResponse(
                    call: Call<BaseResponse>, response: Response<BaseResponse>
                ) {
                    val signupResponse = response.body()
                    utilities.hideProgressDialog()
                    if (signupResponse!!.status == true) {
                        /*Toast.makeText(
                            this@VideoDetailActivity,
                            signupResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()*/
                        //   utilities.customToastSuccess(signupResponse.message,this@VideoDetailActivity)


                    } else {

                        /* Toast.makeText(
                             this@VideoDetailActivity,
                             "" + signupResponse.message,
                             Toast.LENGTH_SHORT
                         ).show()*/
                        utilities.customToastFailure(
                            signupResponse.message,
                            this@VideoDetailActivity
                        )


                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    utilities.hideProgressDialog()
                    //Toast.makeText(this@VideoDetailActivity, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(), this@VideoDetailActivity)

                }


            })

    }

    private fun dislikeVideoApi() {

        val gsonn = Gson()
        val jsonn: String = utilities.getString(this@VideoDetailActivity, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        val user_idd: String = java.lang.String.valueOf(obj.id)
        utilities.showProgressDialog(this@VideoDetailActivity, "Loading ...")
        apiClient.getApiService().dislikeVideo(user_idd, videoId)
            .enqueue(object : Callback<BaseResponse> {

                override fun onResponse(
                    call: Call<BaseResponse>, response: Response<BaseResponse>
                ) {
                    val signupResponse = response.body()
                    utilities.hideProgressDialog()
                    if (signupResponse!!.status == true) {
                        /*Toast.makeText(
                            this@VideoDetailActivity,
                            signupResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()*/

                           utilities.customToastSuccess(signupResponse.message,this@VideoDetailActivity)


                    } else {

                        /*Toast.makeText(
                            this@VideoDetailActivity,
                            "" + signupResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()*/
                        utilities.customToastFailure(
                            signupResponse.message,
                            this@VideoDetailActivity
                        )


                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    utilities.hideProgressDialog()
                    // Toast.makeText(this@VideoDetailActivity, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(), this@VideoDetailActivity)

                }


            })

    }


}