package com.fed.fedsense.Activities

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.Gravity
import android.view.WindowManager
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.room.Room
import com.fed.fedsense.util.Utilities
import com.fed.fedsense.Models.BaseResponse
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.Models.payment.Card
import com.fed.fedsense.R
import com.fed.fedsense.RoomDB.CraditCard
import com.fed.fedsense.RoomDB.MyAppDataBase
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.android.material.card.MaterialCardView
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import kotlinx.android.synthetic.main.activity_resume.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import kotlin.concurrent.thread


class ResumeActivity : AppCompatActivity() {

    lateinit var next: CardView
    lateinit var title: TextView
    lateinit var description: TextView
    lateinit var video_rel: RelativeLayout
    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    private var player: SimpleExoPlayer? = null
    private var playerView: PlayerView? = null
    private val PICK_FILE = 101
    lateinit var fileto_uploadd: File
    private lateinit var myAppDatabase: MyAppDataBase
    var user_id = ""




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_SECURE,
            WindowManager.LayoutParams.FLAG_SECURE
        )
        setContentView(R.layout.activity_resume)


        initt()
        if (!::utilities.isInitialized) utilities = Utilities(this)
        apiClient = ApiClient()
        val gsonn = Gson()
        val jsonn: String = utilities.getString(this, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        user_id = java.lang.String.valueOf(obj.id)
        next.setOnClickListener {

            resume()
        }
        img_fullScreen.setOnClickListener {
            val intent : Intent = Intent(this@ResumeActivity,FullScreenVideoActivity::class.java)
            utilities.saveString(this@ResumeActivity,"videourl",Constants.BASE_URL_IMG + "service_videos/resume_revamp_camp.mp4")
            startActivity(intent)
        }


    }

    private fun initt() {

        myAppDatabase = Room.databaseBuilder(this, MyAppDataBase::class.java, "FEDENSEDB").allowMainThreadQueries().build()
        next = findViewById(R.id.ivNext)
        title = findViewById(R.id.titleText)
        description = findViewById(R.id.discText)
        playerView = findViewById(R.id.video_view)
        video_rel = findViewById(R.id.video_rel)

        title.setText(intent.getStringExtra("title"))
        description.setText(intent.getStringExtra("description"))
        apiClient = ApiClient()
        utilities = Utilities(this)
        if (!::utilities.isInitialized) utilities = Utilities(this)
        Dexter.withActivity(this@ResumeActivity)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                    }
                    if (report.isAnyPermissionPermanentlyDenied) {
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).check()
        setVideo()
    }

    fun setVideo(){

        val appNameStringRes = R.string.app_name
        val trackSelectorDef: TrackSelector = DefaultTrackSelector(this)
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelectorDef)
        val userAgent = Util.getUserAgent(this, this.getString(appNameStringRes))
        val defaultDataSourceFactory = DefaultDataSourceFactory(this, userAgent)
        val uriOfContentUrl = Uri.parse(Constants.BASE_URL_IMG + "service_videos/resume_revamp_camp.mp4")
        val mediaSource: MediaSource = ProgressiveMediaSource.Factory(defaultDataSourceFactory)
            .createMediaSource(uriOfContentUrl)
        //MediaSource mediaSource = new ExtractorMediaSource(uriOfContentUrl, new CacheDataSourceFactory(context, 100 * 1024 * 1024, 500 * 1024 * 1024), new DefaultExtractorsFactory(), null, null);
        //MediaSource mediaSource = new ExtractorMediaSource(uriOfContentUrl, new CacheDataSourceFactory(context, 100 * 1024 * 1024, 500 * 1024 * 1024), new DefaultExtractorsFactory(), null, null);
        player!!.prepare(mediaSource)
        player!!.setPlayWhenReady(false)
        playerView!!.requestFocus()
        playerView!!.player = player
        playerView!!.setShowBuffering(PlayerView.SHOW_BUFFERING_WHEN_PLAYING)
        player!!.setVolume(1f)

    }


    private fun resume() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.resume_dialog)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvYes)
        val no = dialog.findViewById<MaterialCardView>(R.id.tvNo)
        no.setBackgroundResource(R.drawable.bg_buttonnoo)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {
            resume_yes()
            dialog.dismiss()
        }
        no.setOnClickListener {
            noClick()
            dialog.dismiss() }

        dialog.show()
    }
    private fun noClick() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.resume_revamp_noclick)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvYes)
        val no = dialog.findViewById<MaterialCardView>(R.id.tvNo)
        no.setBackgroundResource(R.drawable.bg_buttonnoo)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {
            dialog.dismiss()

            var count =  myAppDatabase.cardDao().loadAll().size
            if (count > 0){

                var have = utilities.getString(this, "have")
                if (have.equals("true")){


                    val gsonn = Gson()
                    val jsonn: String = utilities.getStrings(this, "mycard")
                    if (!jsonn.isEmpty()){
                        val obj: Card = gsonn.fromJson(jsonn, Card::class.java)
                    }
                    if (jsonn.isEmpty()){

                        val intent = Intent(this@ResumeActivity,CardListActivity::class.java)
                        val bundle = Bundle()
                        // bundle.putString("filePath", fileto_uploadd.toString());
                        bundle.putString("amount","165")
                        bundle.putString("from","resumeno")
                        intent.putExtras(bundle)
                        startActivity(intent)
                        dialog.dismiss()

                    }else{

                        val gsonn = Gson()
                        val jsonn: String = utilities.getStrings(this, "mycard")
                        val card: CraditCard = gsonn.fromJson(jsonn, CraditCard::class.java)
                        payAmountForNo(card)
                        // Toast.makeText(this, "from card", Toast.LENGTH_SHORT).show()
                    }
                }else{

                    Toast.makeText(this, "Please select a card", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this@ResumeActivity,CardListActivity::class.java))

                }







            }else{

                addCardPopup()
            }
        }


        no.setOnClickListener {
            dialog.dismiss() }

        dialog.show()
    }


    private fun resume_yes() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.resume_popup_yes)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvUplaod)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {
//            pay_now()
            dialog.dismiss()
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            intent.type = "application/*"
            startActivityForResult(intent, PICK_FILE)
        }

        dialog.show()
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {


            PICK_FILE -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val uri = data.data
                    if (uri != null) {

                        val fileName = getFileNameByUri(uri)
                        copyUriToExternalFilesDir(uri, fileName)

                    }
                }
            }
        }

    }
    private fun getFileNameByUri(uri: Uri): String {
        var fileName = System.currentTimeMillis().toString()
        val cursor = contentResolver.query(uri, null, null, null, null)
        if (cursor != null && cursor.count > 0) {
            cursor.moveToFirst()
            fileName = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME))
            cursor.close()
        }
        return fileName
    }

    private fun copyUriToExternalFilesDir(uri: Uri, fileName: String) {
        thread {
            val inputStream = contentResolver.openInputStream(uri)
            val tempDir = getExternalFilesDir("temp")
            if (inputStream != null && tempDir != null) {
                fileto_uploadd = File("$tempDir/$fileName")
                val fos = FileOutputStream(fileto_uploadd)
                val bis = BufferedInputStream(inputStream)
                val bos = BufferedOutputStream(fos)
                val byteArray = ByteArray(1024)
                var bytes = bis.read(byteArray)
                while (bytes > 0) {
                    bos.write(byteArray, 0, bytes)
                    bos.flush()
                    bytes = bis.read(byteArray)
                }
                bos.close()
                fos.close()
                runOnUiThread {
                    afteruplaodDialouge()
//                    Toast.makeText(this, "Copy file into $tempDir succeeded.", Toast.LENGTH_LONG).show()
                   // uploadResume(fileto_uploadd)

                    //afteruplaodDialouge()

                }
            }
        }
    }
    private fun uploadResume(file: File) {

        utilities.showProgressDialog(this, "Uploading ...")

        val gsonn = Gson()
        val jsonn: String = utilities.getString(this, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        val user_id: RequestBody = RequestBody.create(MediaType.parse("text/plain"), user_idd)
        val service_id: RequestBody = RequestBody.create(MediaType.parse("text/plain"), "1")
        val requestBody: RequestBody = RequestBody.create(MediaType.parse("*/*"), file)
        val fileToUpload = MultipartBody.Part.createFormData("resume", file.getName(), requestBody)

        apiClient.getApiService().upload_resume(
            user_id,
            service_id,
            fileToUpload
        )
            .enqueue(object : retrofit2.Callback<BaseResponse> {
                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {

                    if (response.isSuccessful) {

                        utilities.hideProgressDialog()
                        val status: Boolean = response.body()!!.status
                        if (status.equals(true)) {

                            val message: String = response.body()!!.message
                            //Toast.makeText(this@ResumeActivity, message, Toast.LENGTH_SHORT).show()
                            utilities.customToastSuccess(message,this@ResumeActivity)
                            afteruplaodYesResume()
                            //resumeRevampApplicationApi()


                        } else {
                            val message: String = response.body()!!.message
                            Toast.makeText(this@ResumeActivity, message, Toast.LENGTH_SHORT).show()
                            utilities.customToastFailure(message,this@ResumeActivity)

                        }
                    } else {

                        utilities.hideProgressDialog()
                        val message: String = response.body()!!.message
                        //Toast.makeText(this@ResumeActivity, message, Toast.LENGTH_SHORT).show()
                        utilities.customToastFailure(message,this@ResumeActivity)

                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    utilities.hideProgressDialog()
                    /*Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                        .show()*/
                    utilities.customToastFailure(t.message.toString(),this@ResumeActivity)

                }
            })

    }
    private fun afteruplaodYesResume() {

        val dialog = Dialog(this)

        dialog.setContentView(R.layout.resume_revamp_dialog)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {
            //here payment method will be integrated and than we will call the api
            // uploadResume(fileto_uploadd)
            val intent = Intent(this@ResumeActivity,MainActivity::class.java)
            startActivity(intent)
            dialog.dismiss()

        }

        dialog.show()
    }

    private fun afteruplaodDialouge() {

        val dialog = Dialog(this)

        dialog.setContentView(R.layout.popup5c)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {
            dialog.dismiss()


            var count =  myAppDatabase.cardDao().loadAll().size
            if (count > 0){

                var have = utilities.getString(this, "have")
                if (have.equals("true")){


                    val gsonn = Gson()
                    val jsonn: String = utilities.getStrings(this, "mycard")
                    if (!jsonn.isEmpty()){
                        val obj: Card = gsonn.fromJson(jsonn, Card::class.java)
                    }
                    if (jsonn.isEmpty()){

                        val intent = Intent(this@ResumeActivity,CardListActivity::class.java)
                        val bundle = Bundle()
                        bundle.putString("filePath", fileto_uploadd.toString());
                        bundle.putString("amount","45")
                        bundle.putString("from","resumeRevamp")
                        intent.putExtras(bundle)
                        startActivity(intent)
                        dialog.dismiss()
                    }else{

                        val gsonn = Gson()
                        val jsonn: String = utilities.getStrings(this, "mycard")
                        val card: CraditCard = gsonn.fromJson(jsonn, CraditCard::class.java)

                        payAmount(card)
                        // Toast.makeText(this, "from card", Toast.LENGTH_SHORT).show()
                    }

                }else{

                    Toast.makeText(this, "Please select a card", Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this@ResumeActivity,CardListActivity::class.java))

                }











            }else{

                addCardPopup()
            }

        }

        dialog.show()
    }


    private fun addCardPopup() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.add_card_dialog)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val ok = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        ok.setOnClickListener {

            dialog.dismiss()
            val intent  = Intent(Intent(this,AddCardActivity::class.java))
            startActivity(intent)

        }
        dialog.show()
    }


    private fun payAmount(card: CraditCard) {


        if (utilities.isConnectingToInternet(this)) {

            var expiry = card.expiryData

            val separated = expiry.split("/").toTypedArray()
            val month = separated[0]
            val year = separated[1]
            utilities.showProgressDialog(this, "Processing ...")
            apiClient.getApiService().charge_payment(
                user_id,
                card.cardNumber,
                month,
                year,
                card.cvv,
                "45"

            )
                .enqueue(object : Callback<BaseResponse> {

                    override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                        val signupResponse = response.body()
                        utilities.hideProgressDialog()
                        if (signupResponse!!.status == true) {

                            resumeRevampApplicationApi()
                        } else {
                            utilities.hideProgressDialog()
                            utilities.customToastFailure(signupResponse.message, this@ResumeActivity)

                        }
                    }
                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        utilities.hideProgressDialog()
                        utilities.customToastFailure(t.toString(), this@ResumeActivity)
                    }
                })
        } else {
            Toast.makeText(
                this,
                "Check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        }



    }
    private fun payAmountForNo(card: CraditCard) {


        if (utilities.isConnectingToInternet(this)) {

            var expiry = card.expiryData

            val separated = expiry.split("/").toTypedArray()
            val month = separated[0]
            val year = separated[1]
            utilities.showProgressDialog(this, "Processing ...")
            apiClient.getApiService().charge_payment(
                user_id,
                card.cardNumber,
                month,
                year,
                card.cvv,
                "165"

            )
                .enqueue(object : Callback<BaseResponse> {

                    override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                        val signupResponse = response.body()
                        utilities.hideProgressDialog()
                        if (signupResponse!!.status == true) {

                            resumeRevampApplicationApiNo()
                        } else {
                            utilities.hideProgressDialog()
                            utilities.customToastFailure(signupResponse.message, this@ResumeActivity)

                        }
                    }
                    override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                        utilities.hideProgressDialog()
                        utilities.customToastFailure(t.toString(), this@ResumeActivity)
                    }
                })
        } else {
            Toast.makeText(
                this,
                "Check your internet connection",
                Toast.LENGTH_SHORT
            ).show()
        }



    }
    private fun resumeRevampApplicationApi() {

        val gsonn = Gson()

        val jsonn: String = utilities.getString(this@ResumeActivity, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        apiClient.getApiService().resumeRevampApplication(user_idd,
            "resume_revamp_camp",
            "45","$")
            .enqueue(object : Callback<BaseResponse> {

                override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()
                    if (signupResponse!!.status == true) {
                        //  ook()
                        uploadResume(fileto_uploadd)

                    } else {

                        /*Toast.makeText(
                            this@ResumeActivity,
                            "" + signupResponse.message,
                            Toast.LENGTH_SHORT).show()*/
                        utilities.customToastFailure(signupResponse.message,this@ResumeActivity)

                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                    // Toast.makeText(this@ResumeActivity, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(),this@ResumeActivity)

                }


            })


    }
    private fun resumeRevampApplicationApiNo() {

        val gsonn = Gson()

        val jsonn: String = utilities.getString(this@ResumeActivity, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        apiClient.getApiService().resumeRevampApplication(user_idd,
            "resume_revamp_camp",
            "165","$")
            .enqueue(object : Callback<BaseResponse> {

                override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()
                    if (signupResponse!!.status == true) {
                        //  ook()
                        afterResumeNo()

                    } else {

                        /*Toast.makeText(
                            this@ResumeActivity,
                            "" + signupResponse.message,
                            Toast.LENGTH_SHORT).show()*/
                        utilities.customToastFailure(signupResponse.message,this@ResumeActivity)

                    }
                }

                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {

                    // Toast.makeText(this@ResumeActivity, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(),this@ResumeActivity)

                }


            })


    }
    private fun pay_now() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.popup5c)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {
            ook()
        }

        dialog.show()
    }

    private fun ook() {
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.popup_5d)


        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            dialog.dismiss()
            finish()

        }

        dialog.show()
    }


    override fun onDestroy() {
        super.onDestroy()

        player!!.stop()
        player!!.release()
    }

    private fun afterResumeNo() {

        val dialog = Dialog(this)

        dialog.setContentView(R.layout.resume_revamp_dialog)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.attributes = lp
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        val yes = dialog.findViewById<MaterialCardView>(R.id.tvOk)
        close.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {
            //here payment method will be integrated and than we will call the api
            // uploadResume(fileto_uploadd)
            val intent = Intent(this@ResumeActivity,MainActivity::class.java)
            startActivity(intent)
            dialog.dismiss()

        }

        dialog.show()
    }


}