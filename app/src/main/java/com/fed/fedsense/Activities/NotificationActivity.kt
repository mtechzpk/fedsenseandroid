package com.fed.fedsense.Activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fed.fedsense.Adapters.NotificationAdapter
import com.fed.fedsense.Models.Home.HomeData
import com.fed.fedsense.Models.Home.HomeResponse
import com.fed.fedsense.Models.NotificationDataModel
import com.fed.fedsense.Models.SignUp.User
import com.fed.fedsense.Models.notification.NotificationResponseModel
import com.fed.fedsense.R
import com.fed.fedsense.util.Utilities
import com.google.gson.Gson
import com.mtechsoft.compassapp.networking.Constants
import com.mtechsoft.compassapp.services.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationActivity : AppCompatActivity() {

    private lateinit var apiClient: ApiClient
    private lateinit var utilities: Utilities
    private lateinit var icBack: RelativeLayout
    var notificationList: java.util.ArrayList<com.fed.fedsense.Models.notification.NotificationDataModel> = java.util.ArrayList<com.fed.fedsense.Models.notification.NotificationDataModel>()
    var adapter: NotificationAdapter? = null
    lateinit var rvNotification : RecyclerView
    var i: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val window = window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_notification)

        apiClient = ApiClient()
        utilities = Utilities(this@NotificationActivity)
        if (!::utilities.isInitialized) utilities = Utilities(this@NotificationActivity)

         rvNotification = findViewById(R.id.rvNotification)
        icBack = findViewById(R.id.icBack)
      //  initRecyclerView()
        icBack.setOnClickListener {
            onBackPressed()
        }
        notificationListApi()



    }
/*

    private fun initRecyclerView() {

        list = ArrayList<NotificationDataModel>()
        for (i in 0..9){
            val item = NotificationDataModel()
            item.desc = "The payment has been made and you \nhave subscribed Federal Application \nManager Service for one/two/three"
            item.date = "Nov 1 at 22:14"
            item.image= R.drawable.ic_notifi_img

            list.add(item)
        }


    }
*/

    private fun notificationListApi() {

        val gsonn = Gson()
        val jsonn: String = utilities.getString(this@NotificationActivity, "user")
        val obj: User = gsonn.fromJson(jsonn, User::class.java)
        var user_idd: String = java.lang.String.valueOf(obj.id)

        val url = Constants.BASE_URL+"notification_list/"+user_idd
        utilities.showProgressDialog(this@NotificationActivity, "Loading ...")
        apiClient.getApiService().getNotificationList(url)
            .enqueue(object : Callback<NotificationResponseModel> {

                override fun onResponse(call: Call<NotificationResponseModel>, response: Response<NotificationResponseModel>
                ) {
                    val signupResponse = response.body()
                    val data = response.body()?.data
                    utilities.hideProgressDialog()
                    if (signupResponse!!.status == true) {
                        notificationList = response.body()!!.data

                        rvNotification.setLayoutManager(LinearLayoutManager(this@NotificationActivity, LinearLayoutManager.VERTICAL, false))
                        adapter = NotificationAdapter(this@NotificationActivity, notificationList)
                        rvNotification.setAdapter(adapter)

                    } else {

                       /* Toast.makeText(
                            this@NotificationActivity,
                            "" + signupResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()*/
                        utilities.customToastFailure(signupResponse.message,this@NotificationActivity)


                    }
                }

                override fun onFailure(call: Call<NotificationResponseModel>, t: Throwable) {

                    utilities.hideProgressDialog()
                    //Toast.makeText(this@NotificationActivity, t.toString(), Toast.LENGTH_SHORT).show()
                    utilities.customToastFailure(t.toString(),this@NotificationActivity)

                }


            })

    }
}