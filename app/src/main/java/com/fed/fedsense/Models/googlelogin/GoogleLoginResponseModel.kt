package com.fed.fedsense.Models.googlelogin

import com.google.gson.annotations.SerializedName

data class GoogleLoginResponseModel(
    @SerializedName("status")
    val  status : Boolean,

    @SerializedName("message")
    var message : String,

    @SerializedName("data")
    var data : GoogleLoginDataModel
)
