package com.fed.fedsense.Models.scheduling

import com.google.gson.annotations.SerializedName

data class AdminSchedulesResponseModel(
    @SerializedName("status")
    var status : Boolean,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var data : ArrayList<AdminScheduleDateDataModel>
)
