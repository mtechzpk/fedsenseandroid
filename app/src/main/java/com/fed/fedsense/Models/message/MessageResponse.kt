package com.fed.fedsense.Models.message

import com.google.gson.annotations.SerializedName

data class MessageResponse(

    @SerializedName("status")
    var status: Boolean,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var messages: ArrayList<MessageModel>
)
