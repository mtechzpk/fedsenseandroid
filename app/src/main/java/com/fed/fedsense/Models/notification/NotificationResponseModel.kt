package com.fed.fedsense.Models.notification

import com.fed.fedsense.Models.getmessage.GetMessageDataModel
import com.google.gson.annotations.SerializedName

data class NotificationResponseModel(
    @SerializedName("status")
    var status : Boolean,

    @SerializedName("message")
    var message : String,

    @SerializedName("data")
    var data : ArrayList<NotificationDataModel>
)
