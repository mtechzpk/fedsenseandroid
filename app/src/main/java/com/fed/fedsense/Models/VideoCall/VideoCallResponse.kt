package com.fed.fedsense.Models.VideoCall

import com.fed.fedsense.Models.SignUp.User
import com.google.gson.annotations.SerializedName

data class VideoCallResponse(

    @SerializedName("status")
    var status: Boolean,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var data: VideoCallData

)
