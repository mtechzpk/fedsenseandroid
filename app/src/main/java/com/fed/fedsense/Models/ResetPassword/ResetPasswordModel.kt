package com.report.app.DataModel.ResetPassword

data class ResetPasswordModel(
    var id: Int,
    var username: String? = "",
    var email: String? = "",
    var national_id: String? = "",
    var phone: String? =""
)
