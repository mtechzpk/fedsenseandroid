package com.fed.fedsense.Models.Home

data class HomeData(

    val id: Int,
    var service_type : String = "",
    var service_title : String? = "",
    var service_video : String? = "",
    var service_description : String? = "",
    var is_subscribed : Boolean? = false
)
