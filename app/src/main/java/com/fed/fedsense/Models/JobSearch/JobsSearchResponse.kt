package com.fed.fedsense.Models.JobSearch

data class JobsSearchResponse(
    var SearchResult: SearchResult
)
