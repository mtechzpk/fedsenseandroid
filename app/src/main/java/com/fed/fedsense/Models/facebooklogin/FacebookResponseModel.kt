package com.fed.fedsense.Models.facebooklogin

import com.google.gson.annotations.SerializedName

data class FacebookResponseModel(
    @SerializedName("status")
    var status : Boolean,

    @SerializedName("message")
    var message : String,

    @SerializedName("data")
    var data : FacebookDataModel
)
