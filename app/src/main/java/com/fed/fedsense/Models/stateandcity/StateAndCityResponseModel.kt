package com.fed.fedsense.Models.stateandcity

import com.google.gson.annotations.SerializedName

data class StateAndCityResponseModel(
    @SerializedName("status")
    var status : Boolean,

    @SerializedName("message")
    var message : String,

    @SerializedName("data")
    var data : ArrayList<StateAndCityDataModel>

)
