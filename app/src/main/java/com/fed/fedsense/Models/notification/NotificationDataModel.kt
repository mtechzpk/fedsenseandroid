package com.fed.fedsense.Models.notification

import com.google.gson.annotations.SerializedName

data class NotificationDataModel(
    @SerializedName("id")
    var id : String,

    @SerializedName("notification_to")
    var notification_to : String,

    @SerializedName("notification")
    var notification : String,


    @SerializedName("notification_time")
    var notification_time : String,

    @SerializedName("profile_image")
    var profile_image : String

    )
