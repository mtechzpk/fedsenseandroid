package com.fed.fedsense.Models.faqmodel

import com.google.gson.annotations.SerializedName

data class FAQDataListModel(
    @SerializedName("id")
    var id : Int,

    @SerializedName("question")
    var question : String,

    @SerializedName("answer")
    var answer : String,

    var expanded : Boolean = false
)
