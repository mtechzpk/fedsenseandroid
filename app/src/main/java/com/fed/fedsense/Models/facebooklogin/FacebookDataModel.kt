package com.fed.fedsense.Models.facebooklogin

import com.google.gson.annotations.SerializedName

data class FacebookDataModel(
    @SerializedName("id")
    var id : String,

    @SerializedName("facebook_id")
    var facebook_id : String,

    @SerializedName("google_id")
    var google_id : String,

    @SerializedName("face_id")
    var face_id : String,

    @SerializedName("name")
    var name : String,

    @SerializedName("email")
    var email : String,

    @SerializedName("profile_image")
    var profile_image : String,

    @SerializedName("position_of_interest")
    var position_of_interest : String,

    @SerializedName("chat_session_name")
    var chat_session_name : String)
