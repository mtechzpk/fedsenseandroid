package com.fed.fedsense.Models.Home

import com.google.gson.annotations.SerializedName

data class HomeResponse(

    @SerializedName("status")
    var status: Boolean,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var homedata: ArrayList<HomeData>

)
