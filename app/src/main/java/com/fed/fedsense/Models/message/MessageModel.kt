package com.fed.fedsense.Models.message

data class MessageModel(

    var id: Int,
    var from_id: String,
    var to_id: String,
    var text: String,
    var media: String,
    var media_type: String,
    var time: String,
    var seen: String,
    var sender_id: String,
    var sender_name: String,
    var sender_avatar: String,
    var receiver_id: String,
    var receiver_name: String,
    var receiver_avatar: String,

)

