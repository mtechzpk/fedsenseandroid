package com.fed.fedsense.Models.videolesson

import com.google.gson.annotations.SerializedName

data class VideoLessonFreeDataModel(
    @SerializedName("id")
    var id : String,

    @SerializedName("video_title")
    var video_title : String,

    @SerializedName("video_description")
    var video_description : String,

    @SerializedName("video_type")
    var video_type : String,

    @SerializedName("video")
    var video : String,

    @SerializedName("video_price")
    var video_price : String,

    @SerializedName("video_price_currency")
    var video_price_currency : String,

    @SerializedName("is_subscribed")
    var is_subscribed : Boolean,

    @SerializedName("total_likes")
    var total_likes : String,

    @SerializedName("total_dislikes")
    var total_dislikes : String,

    @SerializedName("is_liked")
    var is_liked : Boolean,

    @SerializedName("is_disliked")
    var is_disliked : Boolean
)
