package com.fed.fedsense.Models.scheduling

import com.google.gson.annotations.SerializedName

data class AdminSchduleTimeDataModel(
    @SerializedName("id")
    var id : String,

    @SerializedName("schedule_time")
    var schedule_time : String,

    @SerializedName("time_status")
    var time_status : String
)
