package com.report.app.DataModel.ResetPassword

import com.fed.fedsense.Models.SignUp.User
import com.google.gson.annotations.SerializedName

data class ResetPasswordResponse(

    @SerializedName("status")
    var status: Boolean,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var user: User
)
