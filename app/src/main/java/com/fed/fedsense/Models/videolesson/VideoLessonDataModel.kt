package com.fed.fedsense.Models.videolesson

import com.google.gson.annotations.SerializedName

data class VideoLessonDataModel(
    @SerializedName("free")
    var videofreelist: ArrayList<VideoLessonFreeDataModel>,

    @SerializedName("paid")
    var videopremiumlist: ArrayList<VideoLessonPremiumDataModel>
)