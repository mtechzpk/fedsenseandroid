package com.fed.fedsense.Models.jobs

import com.google.gson.annotations.SerializedName

data class GetJobsResponseModel(
    @SerializedName("status")
    var status : Boolean,

    @SerializedName("message")
    var message : String,

    @SerializedName("data")
    var data : ArrayList<JobsDataModel>
)