package com.fed.fedsense.Models.auth.a.forgotpassword

import com.google.gson.annotations.SerializedName

data class VerifyCodeResponseModel(
    @SerializedName("status")
    var status : Boolean,

    @SerializedName("message")
    var message : String,

    @SerializedName("data")
    var data : VerifPasswordDataModel
)
