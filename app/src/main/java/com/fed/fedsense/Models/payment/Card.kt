package com.fed.fedsense.Models.payment

import com.google.gson.annotations.SerializedName

data class Card(

    @SerializedName("id")
    var id: Int,

    @SerializedName("user_id")
    var user_id: String,

    @SerializedName("card_holder_name")
    var card_holder_name: String,

    @SerializedName("card_number")
    var card_number: String,

    @SerializedName("expiry_date")
    var expiry_date: String,

    @SerializedName("cvv")
    var cvv: String,

    @SerializedName("status")
    var status: Boolean = false

    )
