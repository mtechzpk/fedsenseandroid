package com.fed.fedsense.Models

data class BaseResponse(

    var status: Boolean,
    var message: String
)
