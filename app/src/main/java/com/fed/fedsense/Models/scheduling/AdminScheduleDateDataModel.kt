package com.fed.fedsense.Models.scheduling

import com.google.gson.annotations.SerializedName

data class AdminScheduleDateDataModel(
    @SerializedName("id")
    var id : String,

    @SerializedName("schedule_date")
    var schedule_date : String,

    @SerializedName("day_status")
    var day_status : String,

    @SerializedName("time_slots")
    var time_slots: ArrayList<AdminSchduleTimeDataModel>
)
