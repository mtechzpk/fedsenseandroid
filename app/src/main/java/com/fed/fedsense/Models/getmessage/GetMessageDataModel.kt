package com.fed.fedsense.Models.getmessage

import com.google.gson.annotations.SerializedName

data class GetMessageDataModel(
    @SerializedName("id")
    var id : String,

    @SerializedName("from_id")
    var from_id : String,

    @SerializedName("to_id")
    var to_id : String,


    @SerializedName("message_type")
    var message_type : String,

    @SerializedName("session_name")
    var session_name : String,

    @SerializedName("text")
    var text : String,

    @SerializedName("media")
    var media : String,

    @SerializedName("media_type")
    var media_type : String,

    @SerializedName("time")
    var time : String,

    @SerializedName("seen")
    var seen : String,

    @SerializedName("sender_id")
    var sender_id : String,

    @SerializedName("sender_name")
    var sender_name : String,

    @SerializedName("sender_avatar")
    var sender_avatar : String,

    @SerializedName("receiver_id")
    var receiver_id : String,

    @SerializedName("receiver_name")
    var receiver_name : String,

    @SerializedName("receiver_avatar")
    var receiver_avatar : String
)
