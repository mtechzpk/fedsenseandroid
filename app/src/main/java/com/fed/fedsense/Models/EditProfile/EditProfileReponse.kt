package com.fed.fedsense.Models.EditProfile

import com.fed.fedsense.Models.SignUp.User
import com.google.gson.annotations.SerializedName

data class EditProfileReponse(

    @SerializedName("status")
    var status: Boolean,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var user: User

)
