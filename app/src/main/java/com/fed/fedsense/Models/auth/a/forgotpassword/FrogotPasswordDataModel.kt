package com.fed.fedsense.Models.auth.a.forgotpassword

import com.google.gson.annotations.SerializedName

data class FrogotPasswordDataModel(
    @SerializedName("email")
    var email : String,

    @SerializedName("Verification_code")
    var verification_code : String
)
