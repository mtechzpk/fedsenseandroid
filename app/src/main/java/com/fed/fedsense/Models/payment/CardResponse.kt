package com.fed.fedsense.Models.payment

import com.fed.fedsense.Models.SignUp.User
import com.google.gson.annotations.SerializedName

data class CardResponse(

    @SerializedName("status")
    var status: Boolean,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var card: ArrayList<Card>

)
