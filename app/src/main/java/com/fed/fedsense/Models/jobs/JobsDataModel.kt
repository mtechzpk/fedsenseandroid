package com.fed.fedsense.Models.jobs

import com.google.gson.annotations.SerializedName

data class JobsDataModel(
    @SerializedName("id")
    var id : Int,

    @SerializedName("name")
    var name : String,

    @SerializedName("image")
    var image : String
){
    var isChecked : Boolean= false
}
