package com.fed.fedsense.Models.JobSearch

data class SearchResult(
    var SearchResultItems: ArrayList<SearchResultItems>,
    var UserArea : UserArea
)
