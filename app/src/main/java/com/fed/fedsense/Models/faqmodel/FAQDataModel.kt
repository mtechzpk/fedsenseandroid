package com.fed.fedsense.Models.faqmodel

import com.google.gson.annotations.SerializedName

data class FAQDataModel(
    @SerializedName("current_page")
    var current_page : Int,

    @SerializedName("data")
    var data : ArrayList<FAQDataListModel>,

    @SerializedName("first_page_url")
    var first_page_url : String,

    @SerializedName("from")
    var from : Int,

    @SerializedName("last_page")
    var last_page : Int,

    @SerializedName("last_page_url")
    var last_page_url : String,


)
