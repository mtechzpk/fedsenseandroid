package com.report.app.DataModel.Forgetpassword

import com.google.gson.annotations.SerializedName

data class ForgetPasswordResponseModel(
    @SerializedName("status")
    var status : Boolean,

    @SerializedName("message")
    var message : String,

    @SerializedName("data")
    var data : ForgetDataModel

)
