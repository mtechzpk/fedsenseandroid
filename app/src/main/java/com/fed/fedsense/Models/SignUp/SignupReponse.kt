package com.fed.fedsense.Models.SignUp

import com.google.gson.annotations.SerializedName

data class SignupReponse(

    @SerializedName("status")
    var status: Boolean,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var user: User

)
