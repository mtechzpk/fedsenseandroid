package com.fed.fedsense.Models.JobSearch

data class MatchedObjectDescriptor(

    var ApplyURI: List<String>,
    var PositionTitle: String,
    var PositionURI: String,
    var PositionLocationDisplay: String,
    var OrganizationName: String,

)
