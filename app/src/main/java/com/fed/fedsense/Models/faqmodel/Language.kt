package com.fed.fedsense.Models.faqmodel

data class Language(
    val name : String ="",
    val description : String= "",
    var expand : Boolean = false
)
