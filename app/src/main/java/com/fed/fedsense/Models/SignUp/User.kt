package com.fed.fedsense.Models.SignUp

data class User(

    val id: Int,
    val google_id : String = "",
    val facebook_id : String? = "",
    val linkedin_id : String? = "",
    val thumbnail_id : String? = "",
    val face_id : String? = "",
    val name : String? = "",
    val email : String? = "",
    val profile_image : String? = "",
    val position_of_interest : String? = "",
    val state : String? = "",
    val city : String? = "",
    val country_code : String? = "",
    val phone : String? = "",
    val like_to_upload : String? = "",
    val are_you_in_US : String? = "",
    val chat_session_name : String? = "",
    var allow_notifications : String? = ""

)
