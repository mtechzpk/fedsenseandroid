package com.fed.fedsense.Models.auth.a.forgotpassword

import com.google.gson.annotations.SerializedName

data class VerifPasswordDataModel(
    @SerializedName("data")
    var data : String
)
