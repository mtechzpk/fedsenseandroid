package com.fed.fedsense.Models.stateandcity

import com.google.gson.annotations.SerializedName

data class StateAndCityDataModel(
    @SerializedName("id")
    var id : String,

    @SerializedName("state")
    var state : String,

    @SerializedName("cities")
    var cities : ArrayList<CityDataModel>
)
