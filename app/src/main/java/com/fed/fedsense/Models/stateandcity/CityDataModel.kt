package com.fed.fedsense.Models.stateandcity

import com.google.gson.annotations.SerializedName

data class CityDataModel(
    @SerializedName("id")
    var id : String,

    @SerializedName("state_id")
    var state_id : String,

    @SerializedName("city")
    var city : String
)
