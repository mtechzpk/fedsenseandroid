package com.report.app.DataModel.Forgetpassword

import com.google.gson.annotations.SerializedName

data class ForgetDataModel(
    @SerializedName("email")
    var email : String,

    @SerializedName("verification_code")
    var code : String
)
