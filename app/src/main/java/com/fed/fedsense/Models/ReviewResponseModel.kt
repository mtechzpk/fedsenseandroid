package com.fed.fedsense.Models

import com.google.gson.annotations.SerializedName

data class ReviewResponseModel(
    @SerializedName("status")
    var status : Boolean,

    @SerializedName("message")
    var message : String


)
