package com.fed.fedsense.Models.videolesson

import com.google.gson.annotations.SerializedName

data class VideoLessonResponseModel(
    @SerializedName("status")
    var status : Boolean,

    @SerializedName("message")
    var message : String,

    @SerializedName("data")
    var data : VideoLessonDataModel
)
