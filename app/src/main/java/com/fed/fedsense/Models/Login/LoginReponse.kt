package com.fed.fedsense.Models.Login

import com.fed.fedsense.Models.SignUp.User
import com.google.gson.annotations.SerializedName

data class LoginReponse(

    @SerializedName("status")
    var status: Boolean,

    @SerializedName("message")
    var message: String,

    @SerializedName("data")
    var user: User

)
