package com.fed.fedsense.Models.VideoCall

import com.google.gson.annotations.SerializedName

data class VideoCallData(

    @SerializedName("channel_name")
    var channel_name: String,

    @SerializedName("call_token")
    var call_token: String

)
