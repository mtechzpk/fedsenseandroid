package com.fed.fedsense.Models.getmessage

import com.google.gson.annotations.SerializedName

data class GetMessageResponseModel(
    @SerializedName("status")
    var status : Boolean,

    @SerializedName("message")
    var message : String,

    @SerializedName("data")
    var data : ArrayList<GetMessageDataModel>
)
