package com.mtechsoft.compassapp.services

import com.fed.fedsense.Models.BaseResponse
import com.fed.fedsense.Models.EditProfile.EditProfileReponse
import com.fed.fedsense.Models.Home.HomeResponse
import com.fed.fedsense.Models.JobSearch.JobsSearchResponse
import com.fed.fedsense.Models.Login.LoginReponse
import com.fed.fedsense.Models.SignUp.SignupReponse
import com.fed.fedsense.Models.VideoCall.VideoCallResponse
import com.fed.fedsense.Models.auth.a.forgotpassword.ForgotPasswordResponseModel
import com.fed.fedsense.Models.faqmodel.FAQResponseModel
import com.fed.fedsense.Models.getmessage.GetMessageResponseModel
import com.fed.fedsense.Models.jobs.GetJobsResponseModel
import com.fed.fedsense.Models.message.MessageResponse
import com.fed.fedsense.Models.notification.NotificationResponseModel
import com.fed.fedsense.Models.payment.CardResponse
import com.fed.fedsense.Models.scheduling.AdminSchedulesResponseModel
import com.fed.fedsense.Models.stateandcity.StateAndCityResponseModel
import com.fed.fedsense.Models.videolesson.VideoLessonResponseModel
import com.report.app.DataModel.Forgetpassword.ForgetPasswordResponseModel
import com.report.app.DataModel.ResetPassword.ResetPasswordResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @Headers("Accept: application/json")
    @POST("login")
    @FormUrlEncoded
    fun login(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("token") token: String,
    ):
            Call<LoginReponse>


    @Headers("Accept: application/json")
    @POST("login_with_facebook")
    @FormUrlEncoded
    fun loginWithFacebook(
        @Field("facebook_id") face_id: String,
        @Field("token") token: String
    ):
            Call<LoginReponse>

    @Headers("Accept: application/json")
    @POST("login_with_google")
    @FormUrlEncoded
    fun loginWithGoogle(
        @Field("google_id") google_id: String,
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("token") token: String,
    ):
            Call<LoginReponse>

    @Headers("Accept: application/json")
    @POST("login_with_linkedin")
    @FormUrlEncoded
    fun loginWithLinkedin(
        @Field("linkedin_id") linkedin_id: String,
        @Field("name") name: String,
        @Field("email") email: String
    ):
            Call<LoginReponse>

    @Headers("Accept: application/json")
    @POST("signup")
    @FormUrlEncoded
    fun signup(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("how_did_you_find_us") howdidyoufind: String,
        @Field("do_you_have_usa_job_profile") doyouhaveprofile: String,
        @Field("are_you_a_military_veteran") militaryVetran: String,
        @Field("are_you_us_employee") employee: String,
        @Field("have_you_documented_disability") disability: String,
        @Field("refer_name") name: String,
        @Field("refer_phone") phone: String,
        @Field("refer_email") referemail: String
    ):
            Call<SignupReponse>

    @Headers("Accept: application/json")
    @GET
    fun getmessages(@Url url: String): Call<MessageResponse>


    @Headers("Accept: application/json")
    @POST("send_message")
    @FormUrlEncoded
    fun sendMessage(
        @Field("from_id") from_id: String,
        @Field("to_id") to_id: String,
        @Field("text") text: String,
    ):
            Call<BaseResponse>


    @Headers("Accept: application/json")
    @POST("like_video")
    @FormUrlEncoded
    fun likeVideo(
        @Field("user_id") user_id: String,
        @Field("video_id") video_id: String
    ):
            Call<BaseResponse>


    @Headers("Accept: application/json")
    @POST("dislike_video")
    @FormUrlEncoded
    fun dislikeVideo(
        @Field("user_id") user_id: String,
        @Field("video_id") video_id: String
    ):
            Call<BaseResponse>


    @Headers("Accept: application/json")
    @GET
    fun getHomeData(@Url url: String): Call<HomeResponse>


    @Headers("Accept: application/json")
    @GET
    fun getNotificationList(@Url url: String): Call<NotificationResponseModel>


    @Headers("Accept: application/json")
    @GET
    fun getMessages(@Url url: String): Call<GetMessageResponseModel>


    @Headers("Accept: application/json")
    @GET
    fun getvideoLesson(@Url url: String): Call<VideoLessonResponseModel>


    @Multipart
    @POST("upload_resume")
    fun upload_resume(
        @Part("user_id") id: RequestBody,
        @Part("service_id") service_id: RequestBody,
        @Part file: MultipartBody.Part?,

        ):
            Call<BaseResponse>


    @Headers("Accept: application/json")
    @POST("edit_profile")
    @FormUrlEncoded
    fun eidtprofileWithoutImage(
        @Field("user_id") user_id: String,
        @Field("name") name: String,
        @Field("state") state: String,
        @Field("city") city: String,
        @Field("country_code") countryCode: String,
        @Field("phone") phone: String,
        @Field("like_to_upload") like_to_upload: String,
        @Field("are_you_in_US") are_you_in_US: String,

    ):
            Call<EditProfileReponse>


    @Headers("Accept: Application/json")
    @GET("admin_schedules")
    fun adminSchdules():
            Call<AdminSchedulesResponseModel>

    @Headers("Accept: Application/json")
    @GET("states_and_cities")
    fun stateAndCities():
            Call<StateAndCityResponseModel>


    @Multipart
    @POST("edit_profile")
    fun eidtprofileWithImage(
        @Part("user_id") user_id: RequestBody,
        @Part("name") name: RequestBody,
        @Part("state") state: RequestBody,
        @Part("city") city: RequestBody,
        @Part("like_to_upload") like_to_upload: RequestBody,
        @Part("are_you_in_US") are_you_in_US: RequestBody,
        @Part("country_code") countryCode : RequestBody,
        @Part("phone") phone : RequestBody,
        @Part file: MultipartBody.Part?,
        @Part file2: MultipartBody.Part?,

        ):
            Call<EditProfileReponse>


    @Headers("Accept: application/json")
    @POST("send_message")
    @FormUrlEncoded
    fun sendMessageText(
        @Field("from_id") from_id: String,
        @Field("to_id") to_id: String,
        @Field("text") text: String,
        @Field("message_type") message_type: String
    ):
            Call<BaseResponse>


    @Multipart
    @POST("edit_profile")
    fun sendMessageWithFile(
        @Part("from_id") from_id: RequestBody,
        @Part("to_id") to_id: RequestBody,
        @Part("media_type") media_type: RequestBody,
        @Part("message_type") message_type: RequestBody,
        @Part file: MultipartBody.Part?,

        ):
            Call<BaseResponse>

    @Multipart
    @POST("send_message")
    fun sendMessageWithImage(
        @Part("from_id") from_id: RequestBody,
        @Part("to_id") to_id: RequestBody,
        @Part("media_type") media_type: RequestBody,
        @Part("message_type") message_type: RequestBody,
        @Part image: MultipartBody.Part?,

        ):
            Call<BaseResponse>


    @Headers("Authorization-key: a0COsXNU7dTZ5hPX1jde3ZCVfPFW5cxYkBP6F5/P5Sw=")
    @GET
    fun jobsSearch(@Url url: String): Call<JobsSearchResponse>

    @Headers("Accept: application/json")
    @POST("subscribe")
    @FormUrlEncoded
    fun federalApplication(
        @Field("user_id") user_id: String,
        @Field("subscription_type") subscription_type: String,
        @Field("subscription_duration") subscription_duration: String,
        @Field("subscription_duration_type") subscription_duration_type: String,
        @Field("start_date_and_time") start_date_and_time: String,
        @Field("end_date_and_time") end_date_and_time: String,
        @Field("paid_amount") paid_amount: String,
        @Field("paid_amount_currency") paid_amount_currency: String,
    ):
            Call<BaseResponse>

    @Headers("Accept: application/json")
    @POST("subscribe")
    @FormUrlEncoded
    fun quickApplication(
        @Field("user_id") user_id: String,
        @Field("subscription_type") subscription_type: String,
        @Field("subscription_duration") subscription_duration: String,
        @Field("subscription_duration_type") subscription_duration_type: String,
        @Field("start_date_and_time") start_date_and_time: String,
        @Field("end_date_and_time") end_date_and_time: String,
        @Field("paid_amount") paid_amount: String,
        @Field("paid_amount_currency") paid_amount_currency: String,
        @Field("announcement_number") announcement_number: String,
        @Field("closing_date") closing_date: String,
        @Field("position_title") position_title: String,
    ):
            Call<BaseResponse>


    @Headers("Accept: application/json")
    @POST("subscribe")
    @FormUrlEncoded
    fun paidVideoLesson(
        @Field("user_id") user_id: String,
        @Field("subscription_type") subscription_type: String,
        @Field("video_id") videoId: String,
        @Field("paid_amount") paid_amount: String,
        @Field("paid_amount_currency") paid_amount_currency: String
    ):
            Call<BaseResponse>


    @Headers("Accept: application/json")
    @POST("subscribe")
    @FormUrlEncoded
    fun enrollInFedApplication(
        @Field("user_id") user_id: String,
        @Field("subscription_type") subscription_type: String,
        @Field("paid_amount") subscription_duration: String,
        @Field("paid_amount_currency") subscription_duration_type: String,
        @Field("schedule_day") start_date_and_time: String,
        @Field("schedule_time") end_date_and_time: String,

        ):
            Call<BaseResponse>

    @Headers("Accept: application/json")
    @POST("subscribe")
    @FormUrlEncoded
    fun resumeRevampApplication(
        @Field("user_id") user_id: String,
        @Field("subscription_type") subscription_type: String,
        @Field("paid_amount") subscription_duration: String,
        @Field("paid_amount_currency") subscription_duration_type: String
    ):
            Call<BaseResponse>

    @Headers("Accept: application/json")
    @POST("subscribe")
    @FormUrlEncoded
    fun attendVideoLesson(
        @Field("user_id") user_id: String,
        @Field("subscription_type") subscription_type: String,
        @Field("paid_amount") paid_amount: String,
        @Field("paid_amount_currency") paid_amount_currency: String
    ):
            Call<BaseResponse>


    @Headers("Accept: application/json")
    @POST("video_call")
    @FormUrlEncoded
    fun video_call(
        @Field("dialer_id") dialer_id: String,
        @Field("receiver_id") receiver_id: String,
    ):
            Call<VideoCallResponse>


    @Headers("Accept: application/json")
    @POST("save_card")
    @FormUrlEncoded
    fun save_card(
        @Field("user_id") user_id: String,
        @Field("card_holder_name") card_holder_name: String,
        @Field("card_number") card_number: String,
        @Field("expiry_date") expiry_date: String,
        @Field("cvv") cvv: String,

        ):
            Call<BaseResponse>

    @Headers("Accept: application/json")
    @POST("charge_payment")
    @FormUrlEncoded
    fun charge_payment(
        @Field("user_id") user_id :String,
        @Field("card_number") card_number: String,
        @Field("exp_month") exp_month: String,
        @Field("exp_year") exp_year: String,
        @Field("cvv") cvv: String,
        @Field("amount") amount: String,

        ):
            Call<BaseResponse>


    @Headers("Accept: application/json")
    @GET
    fun get_cards(@Url url: String): Call<CardResponse>

    @Headers("Accept: application/json")
    @POST("user_review")
    @FormUrlEncoded
    fun reviewAndSuggestions(
        @Field("user_id") user_id: String,
        @Field("review") review: String
    ):
            Call<BaseResponse>


    @Headers("Accept: application/json")
    @GET
    fun muteNotification(@Url url: String): Call<SignupReponse>

    @Headers("Accept: application/json")
    @GET
    fun getFaq(@Url url: String): Call<FAQResponseModel>

    @Headers("Accept: Application/json")
    @GET("job_groups")
    fun jobsGroups():
            Call<GetJobsResponseModel>


    @Headers("Accept: application/json")
    @POST("forgot_password")
    @FormUrlEncoded
    fun forgetPassword(
        @Field("email") email: String
    ):
            Call<ForgotPasswordResponseModel>


    @Headers("Accept: application/json")
    @POST("reset_password")
    @FormUrlEncoded
    fun restpass(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("confirm_password") confirm_password: String,
    ): Call<ResetPasswordResponse>

}