package com.fed.fedsense.Notification;

public class MessageNotificationData {
    private String receiverID, receiverName, receiverImage, message, notificationType;

    public MessageNotificationData() {
        notificationType = "chat";
    }

    public MessageNotificationData(String receiverID, String receiverName, String receiverImage, String message, String notificationType) {
        this.receiverID = receiverID;
        this.receiverName = receiverName;
        this.receiverImage = receiverImage;
        this.message = message;
        this.notificationType = notificationType;
    }

    public MessageNotificationData(String receiverID, String receiverName, String receiverImage, String message) {
        this.receiverID = receiverID;
        this.receiverName = receiverName;
        this.message = message;
        notificationType = "chat";
    }

    public String getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(String receiverID) {
        this.receiverID = receiverID;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverImage() {
        return receiverImage;
    }

    public void setReceiverImage(String receiverImage) {
        this.receiverImage = receiverImage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }
}