package com.fed.fedsense.Notification;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAsLxcyzc:APA91bEmZwgDcHxcJjRr0SO0V2OI2T0Hi6TGSKsUL9p2f8Ds5wuR3lf_nSq-hz8PMd8UPZ6od2hsFa3ybTSvHGNn9RZe7LwPu8vZ3zQdbIsRwMaImh-pI7E30h04uJJ-XdedT0r8gtPd"
            }
    )
    @POST("fcm/send")
    Call<MessageResponseModel> sendNotifcation(@Body NotificationSenderModel body);
}