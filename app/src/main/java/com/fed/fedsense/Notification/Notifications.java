package com.fed.fedsense.Notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.fed.fedsense.Activities.IncommigCallActivity;
import com.fed.fedsense.Activities.MainActivity;
import com.fed.fedsense.R;
import com.fed.fedsense.util.Database;
import com.fed.fedsense.util.Utilities;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.RemoteMessage;


import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Notifications {

    private Context context;

    public Notifications(Context context){
        this.context = context;
    }

    private static final String CHANNEL_ID = "scNotifications";
    private static final String CHANNEL_NAME = "SC Notifications";
    private static final String CHANNEL_DESC = "Sending and Receiving Notifications";


    //This Method will Display Notification and set the Pending Intents
    public static void displayNotifications(Context context, RemoteMessage remoteMessage) {


        String title = "", text = "";
        Intent intent = new Intent(context, MainActivity.class);

//        if (remoteMessage.getData().get("notificationType").equals("chat")) {

        String receiverID = remoteMessage.getData().get("receiverID");
//        String receiverName = remoteMessage.getData().get("receiverName");
        String receiverName = "Admin";

        String receiverImage = remoteMessage.getData().get("receiverImage");
        String message = remoteMessage.getData().get("message");
        title = receiverName;
        text = message;
        Utilities utilities = new Utilities(context);
        String token = "";
        String channel = "";
        if (remoteMessage.getData().containsKey("call_token"))
        {
            token = remoteMessage.getData().get("call_token");
            channel = remoteMessage.getData().get("channel_name");
            SharedPreferences sharedPreferences = context.getSharedPreferences("TOKEN",Context.MODE_PRIVATE);
            String incall = sharedPreferences.getString("IN_CALL","").toString();
            if (!incall.equals("true"))
            {
                intent = new Intent(context, IncommigCallActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("aciton","manual");
                intent.putExtra("token",token);
                intent.putExtra("channel",channel).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Log.d("callparts","callpartexcuted");
            }
        }else {
            intent = new Intent(context, MainActivity.class);
            Log.d("callparts","notexecuted");
        }


        /*utilities.saveString(context, "artist_id", receiverID);
        utilities.saveString(context, "artist_name", receiverName);
        utilities.saveString(context, "artist_image", receiverImage);*/
//        }


        PendingIntent pendingIntent = PendingIntent.getActivity(
                context,
                100,
                intent,
                PendingIntent.FLAG_IMMUTABLE);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context, CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_logo)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                        .setPriority(NotificationCompat.PRIORITY_MAX);

        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(context);
        managerCompat.notify(1, builder.build());

    }

    public static void displayNotifications1(Context context, RemoteMessage remoteMessage) {


        String title = "", text = "";
        Intent intent = new Intent(context, MainActivity.class);

//        if (remoteMessage.getData().get("notificationType").equals("chat")) {

        String receiverID = remoteMessage.getData().get("receiverID");
//        String receiverName = remoteMessage.getData().get("receiverName");
        String receiverName = "Admin";

        String title1 = remoteMessage.getData().get("title");
        String message = remoteMessage.getData().get("message");
        intent = new Intent(context, MainActivity.class);
        title = title1;
        text = message;
        Utilities utilities = new Utilities(context);

        /*utilities.saveString(context, "artist_id", receiverID);
        utilities.saveString(context, "artist_name", receiverName);
        utilities.saveString(context, "artist_image", receiverImage);*/
//        }


        PendingIntent pendingIntent = PendingIntent.getActivity(
                context,
                100,
                intent,
                PendingIntent.FLAG_IMMUTABLE);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context, CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_logo)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                        .setPriority(NotificationCompat.PRIORITY_MAX);

        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(context);
        managerCompat.notify(1, builder.build());

    }

    //This Will create a Channel for the Notification. Called after the Successful Login
    public static void createNotificationChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(CHANNEL_DESC);
            NotificationManager manager = context.getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }
    }

    // This Overloaded Method will send Notification to Single Target User
    public static void sendNotification(final MessageNotificationData data,
                                        final String user_id, final Context context) {

        Log.d("YAM", "Sending...");

        final ProgressDialog progressDialog = new ProgressDialog(context);
        final APIService apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);
        Database database = new Database();
        FirebaseDatabase.getInstance().getReference("tokens").child(user_id).child("device_token")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {

                            String fcmToken = dataSnapshot.getValue().toString();
                            Log.d("YAM", "FCM : " + fcmToken);

                            NotificationSenderModel reciever = new NotificationSenderModel(data, fcmToken);
                            apiService.sendNotifcation(reciever).enqueue(new Callback<MessageResponseModel>() {
                                @Override
                                public void onResponse(Call<MessageResponseModel> call, Response<MessageResponseModel> response) {
                                    if (response.code() == 200) {
                                        Log.d("YAM", "Response : " + response.code());
//                                        Toast.makeText(context, "success", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<MessageResponseModel> call, Throwable t) {
                                    Toast.makeText(context, "failed", Toast.LENGTH_SHORT).show();

                                }
                            });

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(context, databaseError.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
    }

}
